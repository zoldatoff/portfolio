﻿CREATE OR REPLACE package UTIL.pkg$status_business as

c$lag   number := 2/24; -- Сдвиг ожидания техдаты с 00:00 на 22:00

function f$get(
   in$owner in varchar2                     -- схема, в которой расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$b_date_name in varchar2 default null  -- опционально: название бизнес-даты. Если параметр не указан, то возвращается наименьшая из дат для указанной таблицы
) return date deterministic;

procedure p$set(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$b_date_name in varchar2               -- название бизнес-даты
 , in$b_date in date default null           -- опционально: значение бизнес-даты, которое требуется установить. По умолчанию устанавливается значение с лагом по умолчанию: trunc(sysdate) - lag
);

procedure p$wait_until(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$hour_to_wait in integer default null  -- час отсечки (integer в диапазоне от 0 до 23), если не задан - бесконечное ожидание
 , in$b_date_name in varchar2 default null  -- опционально: название ожидаемой бизнес-даты
 , in$b_date in date default null           -- опционально: ожидаемое значение бизнес-даты. По умолчанию применяется стандартный лаг: trunc(sysdate) - lag
);

function f$wait_until(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$hour_to_wait in integer default null  -- час отсечки (integer в диапазоне от 0 до 23), если не задан - бесконечное ожидание
 , in$b_date_name in varchar2 default null  -- опционально: название ожидаемой бизнес-даты
 , in$b_date in date default null           -- опционально: ожидаемое значение бизнес-даты. По умолчанию применяется стандартный лаг: trunc(sysdate) - lag
)
return varchar2;

end;
/

CREATE OR REPLACE package body UTIL.pkg$status_business as

--####################### Private ##############################################
function f$get$name(
    in$owner in varchar2
  , in$table_name in varchar2
  , in$b_date_name in varchar2
) return varchar2
is
begin
    return nvl(upper(in$owner),'NULL')||'.'||nvl(upper(in$table_name),'NULL')||case when in$b_date_name not like '@%' then '.' else '' end||upper(in$b_date_name);
exception when others then
    return '';
end;

function f$get$rb(
    in$owner in varchar2        -- Схема таблицы, для которой необходимо извлечь бизнес-дату
  , in$table_name in varchar2   -- Таблица, для которой необходимо извлечь бизнес-дату
)
return date
is
    v$maxdate           date;
    v$search_table      util.status_business$external.search_table%type;
    v$search_string     util.status_business$external.search_string%type;

    ex$no_method        exception;
    ex$no_date          exception;
begin
    select upper(search_table), search_string
    into  v$search_table, v$search_string
    from util.status_business$external e
    where upper(e.owner) = upper(in$owner)
      and upper(e.table_name) = upper(in$table_name)
      and upper(database_name) = '@RB';

    if (upper(v$search_table) = 'BACKDB.CLOSEDAYLOG') then
        select max(doperday)
        into v$maxdate
        from backdb.closedaylog@rb
        where stext like '%'||v$search_string||'%';

        if (v$maxdate is null) then
            raise ex$no_date;
        end if;

        return v$maxdate;
    elsif (upper(v$search_table) = 'RM.CLOSEDAYLOG') then
        select max(doperday-1)
        into v$maxdate
        from rm.closedaylog@rb
        where stext like '%'||v$search_string||'%';

        if (v$maxdate is null) then
            raise ex$no_date;
        end if;

        return v$maxdate;
    end if;

    raise ex$no_method;
exception
    when ex$no_date then
        util.pkg$log.p$add_log(
            null
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,'@RB')||'</b> не найдена в таблице UTIL.STATUS_BUSINESS$EXTERNAL'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$RB'
        );
        return c$notappldate;
    when ex$no_method then
        util.pkg$log.p$add_log(
            null
          , 'Для бизнес-даты <b>'||f$get$name(in$owner,in$table_name,'@RB')||'</b> не найден обработчик процедуре UTIL.PKG$STATUS_BUSINESS.F$GET$RB'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$RB'
        );
        return c$notappldate;
    when no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,'@RB')||'</b> не найдена в таблице UTIL.STATUS_BUSINESS$EXTERNAL'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$RB'
        );
        return c$notappldate;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'Функция UTIL.PKG$STATUS_BUSINESS.F$GET$RB вызвала исключение '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          ||'. Бизнес-дата : <b>'||f$get$name(in$owner,in$table_name,'@RB')||'</b>'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$RB'
        );
        return c$notappldate;
end;

function f$get$b2(
    in$owner in varchar2        -- Схема таблицы, для которой необходимо извлечь бизнес-дату
  , in$table_name in varchar2   -- Таблица, для которой необходимо извлечь бизнес-дату
)
return date
is
    v$maxdate           date;
    v$search_table      util.status_business$external.search_table%type;
    v$search_string     util.status_business$external.search_string%type;

    ex$no_method        exception;
    ex$no_date          exception;
begin
    select upper(search_table), search_string
    into  v$search_table, v$search_string
    from util.status_business$external e
    where upper(e.owner) = upper(in$owner)
      and upper(e.table_name) = upper(in$table_name)
      and upper(database_name) = '@B2';

    if (upper(v$search_table) = 'CREATOR.SYSLOG') then
        select max(systemday-1)
        into v$maxdate
        from creator.syslog@b2
        where systemeventtypeid = 6
          and message = 'Операционный день открыт';

        if (v$maxdate is null) then
            raise ex$no_date;
        end if;

        return v$maxdate;
    end if;

    raise ex$no_method;
exception
    when ex$no_date then
        util.pkg$log.p$add_log(
            null
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,'@B2')||'</b> не найдена в таблице UTIL.STATUS_BUSINESS$EXTERNAL'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$B2'
        );
        return c$notappldate;
    when ex$no_method then
        util.pkg$log.p$add_log(
            null
          , 'Для бизнес-даты <b>'||f$get$name(in$owner,in$table_name,'@B2')||'</b> не найден обработчик процедуре UTIL.PKG$STATUS_BUSINESS.F$GET$B2'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$B2'
        );
        return c$notappldate;
    when no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,'@B2')||'</b> не найдена в таблице UTIL.STATUS_BUSINESS$EXTERNAL'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$B2'
        );
        return c$notappldate;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'Функция UTIL.PKG$STATUS_BUSINESS.F$GET$B2 вызвала исключение '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          ||'. Бизнес-дата : <b>'||f$get$name(in$owner,in$table_name,'@B2')||'</b>'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$B2'
        );
        return c$notappldate;
end;

function f$get$dwh(
    in$owner in varchar2        -- Схема таблицы, для которой необходимо извлечь бизнес-дату
  , in$table_name in varchar2   -- Таблица, для которой необходимо извлечь бизнес-дату
)
return date
is
    v$maxdate           date;
    v$search_table      util.status_business$external.search_table%type;
    v$search_string     util.status_business$external.search_string%type;

    ex$no_date          exception;
    ex$no_data_found    exception;
begin
    begin
        select trunc(sysdate) - lag_dd
        into v$maxdate
        from util.status_business
        where upper(owner) = upper(in$owner)
          and upper(table_name) = upper(in$table_name)
          and upper(b_date_name) = '@DWH';
    exception when no_data_found then
        raise ex$no_data_found;
    end;

    if (v$maxdate is null) then
        raise ex$no_date;
    end if;

    return v$maxdate;
exception
    when ex$no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,'@DWH')||'</b> не найдена в таблице UTIL.STATUS_BUSINESS'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$DWH'
        );
        return c$notappldate;
    when ex$no_date then
        util.pkg$log.p$add_log(
            null
          , 'Для бизнес-даты <b>'||f$get$name(in$owner,in$table_name,'@DWH')||'</b> не задан LAG_DD в таблице UTIL.STATUS_BUSINESS'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$DWH'
        );
        return c$notappldate;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'Функция UTIL.PKG$STATUS_BUSINESS.F$GET$DWH вызвала исключение '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          ||'. Бизнес-дата : <b>'||f$get$name(in$owner,in$table_name,'@DWH')||'</b>'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$GET$DWH'
        );
        return c$notappldate;
end;

--####################### Public ###############################################

function f$get(
   in$owner in varchar2                     -- схема, в которой расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$b_date_name in varchar2 default null  -- опционально: название бизнес-даты. Если параметр не указан, то возвращается наименьшая из дат для указанной таблицы
) return date deterministic
is
    var$ret date;
begin
    if (upper(in$b_date_name) = '@RB') then
        return pkg$status_business.f$get$rb(in$owner, in$table_name);
    end if;

    if (upper(in$b_date_name) = '@B2') then
        return pkg$status_business.f$get$b2(in$owner, in$table_name);
    end if;

    if (upper(in$b_date_name) = '@DWH') then
        return pkg$status_business.f$get$dwh(in$owner, in$table_name);
    end if;

    select min(nvl(b_date,c$mindate))
    into var$ret
    from util.status_business
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and b_date_name = nvl(upper(in$b_date_name),b_date_name);

    return nvl(var$ret,c$mindate);
exception when others then
    return c$mindate;
end;

procedure p$set(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$b_date_name in varchar2               -- название бизнес-даты
 , in$b_date in date default null           -- опционально: значение бизнес-даты, которое требуется установить. По умолчанию устанавливается значение с лагом по умолчанию: trunc(sysdate) - lag
)
is
    pragma autonomous_transaction;
    v$b_date_prev date;
    var$dt date;
    var$lag number;
    var$desc varchar2(1000);
    v$date_ins_upd date;
begin
    if (in$b_date_name like '@%') then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Невозможно установить бизнес-дату  <b>'
          ||f$get$name(in$owner,in$table_name,in$b_date_name)
          ||'</b>: бизнес-дата для внешних серверов доступна только для чтения.'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.P$SET'
        );

        return;
    end if;

    begin
        select nvl(b_date,c$mindate)
        into v$b_date_prev
        from util.status_business
        where owner = upper(in$owner)
          and table_name = upper(in$table_name)
          and b_date_name = upper(in$b_date_name);
    exception when others then
        v$b_date_prev := c$mindate;
    end;

    update util.status_business
    set b_date = coalesce(trunc(in$b_date,'dd'),trunc(sysdate) - lag_dd,c$mindate)
      , date_ins_upd = sysdate
      , p_action = upper(in$p_action)
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and b_date_name = upper(in$b_date_name)
    returning   b_date, lag_dd , description, date_ins_upd
    into        var$dt, var$lag, var$desc   , v$date_ins_upd;

    if (sql%rowcount > 0) then
        insert into util.status_business_hist(
            id, owner, table_name, b_date_name, b_date, p_action
          , lag_dd, description, date_ins_upd, b_date_prev
        )
        values (
            util.seq$status_business_hist$id.nextval
          , upper(in$owner)
          , upper(in$table_name)
          , upper(in$b_date_name)
          , var$dt
          , upper(in$p_action)
          , var$lag
          , var$desc
          , v$date_ins_upd
          , v$b_date_prev
        );

        if (var$lag is null) then
            util.pkg$log.p$add_log(
                upper(in$p_action)
              , 'В таблице UTIL.STATUS_BUSINESS не проставлена задержка по умолчанию LAG_DD для бизнес-даты <b>'
              ||f$get$name(in$owner,in$table_name,in$b_date_name)
              ||'</b>'
              , 'warning'
              , 'date','eol'
              , 'UTIL.PKG$STATUS_BUSINESS.P$SET'
            );
        end if;

        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Установлена бизнес-дата <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
          ||case when var$desc is not null then ' ['||var$desc||']' end
          ||' = <b>'||nvl(to_char(var$dt,'dd.mm.yyyy'),'NULL')||'</b>'
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.P$SET'
        );
    else
        v$date_ins_upd := sysdate;

        insert into util.status_business(
            owner, table_name, b_date_name, b_date, p_action, lag_dd, description, date_ins_upd
        )
        values (
            upper(in$owner), upper(in$table_name), upper(in$b_date_name), in$b_date, upper(in$p_action), null, null, v$date_ins_upd
        );

        insert into util.status_business_hist(
            id, owner, table_name, b_date_name, b_date, p_action
          , lag_dd, description, date_ins_upd, b_date_prev
        )
        values (
            util.seq$status_business_hist$id.nextval
          , upper(in$owner)
          , upper(in$table_name)
          , upper(in$b_date_name)
          , nvl(in$b_date,c$mindate)
          , upper(in$p_action)
          , null
          , null
          , v$date_ins_upd
          , c$mindate
        );

        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Добавлена новая бизнес-дата <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)
          ||'</b> = '||to_char(nvl(in$b_date,c$mindate),'dd.mm.yyyy')||' </span><span class="red_btext">Проставьте задержку по умолчанию LAG_DD в таблице UTIL.STATUS_BUSINESS</span>'
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.P$SET'
        );
    end if;

    commit;
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , '[UTIL.PKG$STATUS_BUSINESS.P$SET] Процедура P$SET вызвала исключение: '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      ||' Бинес-дата : <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
      , 'error'
    );
    rollback;
end;

function f$wait_until(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$hour_to_wait in integer default null  -- час отсечки (integer в диапазоне от 0 до 23), если не задан - бесконечное ожидание
 , in$b_date_name in varchar2 default null  -- опционально: название ожидаемой бизнес-даты
 , in$b_date in date default null           -- опционально: ожидаемое значение бизнес-даты. По умолчанию применяется стандартный лаг: trunc(sysdate) - lag
)
return varchar2
is
    var$lag number;
    var$desc varchar2(1000);
begin
    begin
        select description
        into var$desc
        from util.status_business
        where owner = upper(in$owner)
          and table_name = upper(in$table_name)
          and b_date_name = upper(in$b_date_name);
    exception when others then
        var$desc := '';
    end;

    if (in$hour_to_wait not between 0 and 23) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Час отсечки должен быть в диапазоне от 0 до 23 часов'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
        );
        return 'ERROR';
    end if;

    if (pkg$status_business.f$get(in$owner,in$table_name,in$b_date_name)  = c$mindate) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Бизнес-дата <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b> не найдена в таблице UTIL.STATUS_BUSINESS'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
        );
        return 'ERROR';
    end if;

    begin
        select max(lag_dd)
        into var$lag
        from util.status_business
        where owner = upper(in$owner)
          and table_name = upper(in$table_name)
          and b_date_name = nvl(upper(in$b_date_name),b_date_name);

        if (var$lag is null) then
            raise no_data_found;
        end if;
    exception when no_data_found then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'В таблице UTIL.STATUS_BUSINESS не задан параметр LAG_DD для бизнес-даты <b>'
           ||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
        );
        return 'ERROR';
    end;

    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'Ждем появления бизнес-даты <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
      ||case when in$hour_to_wait is not null then ' до '||in$hour_to_wait||':00' end
      , 'notice'
      , 'date','eol'
      , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
    );

    loop
        if (nvl(in$b_date,trunc(sysdate) - var$lag) <= pkg$status_business.f$get(in$owner,in$table_name,in$b_date_name)) then
            util.pkg$log.p$add_log(
                upper(in$p_action)
              , 'Дождались бизнес-даты <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
              ||' = '||to_char(pkg$status_business.f$get(in$owner,in$table_name,in$b_date_name),'dd.mm.yyyy')
              , 'notice'
              , 'date','eol'
              , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
            );

            return 'BUSINESS_DATE';
        elsif(case when to_char(sysdate+c$lag,'hh24') < in$hour_to_wait then 0
                   when to_char(sysdate,'hh24') < in$hour_to_wait then 0
                   when in$hour_to_wait is null then 0
                   else 1
              end = 1) then
            util.pkg$log.p$add_log(
                upper(in$p_action)
              , 'Истекло время ожидания бизнес-даты <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
              ||' до '||in$hour_to_wait||':00'
              , 'warning'
              , 'date','eol'
              , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
            );

            return 'TIME_OF_DAY';
        end if;

        if (upper(in$b_date_name) in ('@B2','@RB','@DWH')) then
            peo.pkg$ddl.p$sleep_for(null,1);
        else
            peo.pkg$ddl.p$sleep_for(null,1);
        end if;
    end loop;
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'Функция F$WAIT_UNTIL вызвала исключение : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      ||'Бизнес-дата : <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
      , 'warning'
      , 'date','eol'
      , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
    );
    return 'ERROR';
end;

procedure p$wait_until(
   in$p_action in varchar2                  -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица
 , in$table_name in varchar2                -- название таблицы
 , in$hour_to_wait in integer default null  -- час отсечки (integer в диапазоне от 0 до 23), если не задан - бесконечное ожидание
 , in$b_date_name in varchar2 default null  -- опционально: название ожидаемой бизнес-даты
 , in$b_date in date default null           -- опционально: ожидаемое значение бизнес-даты. По умолчанию применяется стандартный лаг: trunc(sysdate) - lag
)
is
    var$result varchar2(100);
begin
    var$result := f$wait_until(in$p_action, in$owner, in$table_name, in$hour_to_wait, in$b_date_name, in$b_date);
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'Процедура P$WAIT_UNTIL вызвала исключение : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      ||' Бизнес-дата : <b>'||f$get$name(in$owner,in$table_name,in$b_date_name)||'</b>'
      , 'warning'
      , 'date','eol'
      , 'UTIL.PKG$STATUS_BUSINESS.F$WAIT_UNTIL'
    );
end;

end;
/

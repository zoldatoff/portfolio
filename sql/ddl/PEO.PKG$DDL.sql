﻿CREATE OR REPLACE package PEO.pkg$ddl  as
 procedure p$sleep_until_rback_idle(
        in$jobname varchar2 -- Имя вызывающей процедуры
  );

  procedure p$sleep_until(
        in$jobname      varchar2, -- Имя вызывающей процедуры
        in$date_to_sleep    date  -- Время до которого спать
  );

  procedure p$sleep_for(
        in$jobname      varchar2,  -- Имя вызывающей процедуры
        in$minutes_to_sleep number -- Сколько минут спать
  );



  procedure p$mv_refresh(
        in$jobname      varchar2, -- Имя вызывающей процедуры
        in$ownname      varchar2, -- Схема, в которой расположено обнвляемое материализованное представление
        in$mvname       varchar2, -- Обновляемое материализованное представление
        in$refresh_type varchar2  -- Тип пересборки представления (возможные значения: 'f', 'fast' для пересборки по логам и 'c', 'complete' для полной пересборки)
  );

  procedure p$set_sort_area(
        in$jobname varchar2,         -- Имя вызывающей процедуры
        in$sizemb number default 100  -- Величина выделяемой памяти в МБ, максимум 200
  );

  --procedure set_sort_area(in$sizeMB number default 10);

 -- procedure index_off(in$jobname varchar2,in$index_name varchar2);
 -- procedure index_off(in$index_name varchar2);

 -- procedure index_on(in$jobname varchar2,in$index_name varchar2);
 -- procedure index_on(in$index_name varchar2);

  procedure p$trunc(
        in$jobname varchar2,                      -- Имя вызывающей процедуры
        in$ownname varchar2,                      -- Схема, в которой расположена очищаемая таблица
        in$tabname varchar2,                      -- Очищаемая таблица
        in$partname date,                         -- Очищаемая партиция
        in$rebuild varchar2 default 'rebuild'   -- Надо ли перестраивать индексы
  );

  procedure p$trunc(
        in$jobname varchar2, -- Имя вызывающей процедуры
        in$ownname varchar2, -- Схема, в которой расположена очищаемая таблица
        in$tabname varchar2  -- Очищаемая таблица
  );
  --procedure trunc(in$table_name varchar2);

  procedure p$stat(
        in$jobname varchar2, -- Имя вызывающей процедуры
        in$ownname varchar2, -- Схема, в которой расположена таблица, по которой собирается статистика
        in$tabname varchar2, -- Таблица, по которой собирается статистика
        in$partname date -- Партиция, по которой собирается статистика
  );

  procedure p$stat(
        in$jobname varchar2, -- Имя вызывающей процедуры
        in$ownname varchar2, -- Схема, в которой расположена таблица, по которой собирается статистика
        in$tabname varchar2  -- Таблица, по которой собирается статистика
  );

  procedure p$compress(
        in$jobname varchar2, -- Имя вызывающей процедуры
        in$ownname varchar2, -- Схема, в которой расположена сжимаемая таблица
        in$tabname varchar2, -- Таблица, которую собираемся сжать
        in$partname date     -- Партиция, которую собираемся сжать
  );

  procedure p$exchange(
        in$jobname        varchar2, -- Имя вызывающей процедуры
        in$ownname        varchar2, -- Схема, в которой расположена таблица, в которой заменяется партиция
        in$tabname        varchar2, -- Таблица, в которой заменяется партиция
        in$partname       date      -- Заменяемая партиция (задаётся датой)
  );

  procedure p$exchange(
        in$jobname        varchar2, -- Имя вызывающей процедуры
        in$ownname        varchar2, -- Схема, в которой расположена таблица, в которой заменяется партиция
        in$tabname        varchar2  -- Таблица, в которой заменяется партиция
  );

 procedure p$defragmentation(
        in$program_action varchar2,
        in$ownname        varchar2,
        in$tabname        varchar2
  );

end pkg$ddl;
/


CREATE OR REPLACE package body PEO.pkg$ddl  as

  c$jobname constant varchar2(300) := 'PEO.PKG$DDL';
  v$strline varchar2(3000);

  -- PRIVATE --

   function f$table_exist(in$ownname varchar2, in$tabname varchar2)
  return number
  is
     v$table_exist number(10);
  begin

     select least(count(*),1) into v$table_exist
     from
     (
         select *
         from sys.all_tables
         where owner      = upper(trim(in$ownname))
           and table_name = upper(trim(in$tabname))
     );

     return nvl(v$table_exist,-1);

  exception when others then return -1;
  end f$table_exist;

  function f$partition_exist(in$ownname varchar2, in$tabname varchar2, in$partname varchar2)
  return number
  is
     v$partition_exist number(10);
  begin

     select least(count(*),1) into v$partition_exist
     from
     (
         select *
         from sys.all_tab_partitions
         where table_owner    = trim(upper(in$ownname))
           and table_name     = trim(upper(in$tabname))
           and partition_name = trim(upper(in$partname))
     );

     return nvl(v$partition_exist,-1);

  exception when others then return -1;
  end f$partition_exist;

  function f$global_index_exist(in$ownname varchar2, in$tabname varchar2)
  return number
  is
     v$global_index_exist number(10);
  begin

     select least(count(*),1) into v$global_index_exist
     from
     (
          select * from sys.all_indexes
            where table_owner = trim(upper(in$ownname))
              and table_name  = trim(upper(in$tabname))
              and partitioned = 'NO'
     );

     return nvl(v$global_index_exist,-1);

  exception when others then return -1;
  end f$global_index_exist;

  /*PRIVATE*/
--    procedure index_off(in$index_name varchar2)
--  is
--  begin
--  index_off('UTIL.COMMON_LOGS',in$index_name);
--  exception when others then raise;
--  end;
--
--  procedure index_off(in$jobname varchar2,in$index_name varchar2)
--  is
--  v$index_name       varchar2(100); -- Полное имя индекса OWNER.MY_INDEX
--
--  v$index            varchar2(100); -- Краткое имя индекса MY_INDEX
--  v$index_owner      varchar2(100); -- Схема, где живёт индекс
--
--  v$table            varchar2(100);
--  v$table_owner      varchar2(100);
--
--  v$constraint       varchar2(100);
--  v$constraint_owner varchar2(100);
--
--  v$cnt              number(2);
--
--  UNCORRECT_FORMAT          exception;
--  UNEXIST_INDEX             exception;
--  INVALID_INDEX             exception;
--  UNABLE_TO_OFF_INDEX       exception;
--  UNABLE_TO_OFF_CONSTRAINT  exception;
--  UNABLE_TO_ALTER_SESSION   exception;
--
--  begin
--
--  --util.pkg$job_control.start_log(in$jobname);
--  util.pkg$job_control.add_nodate_log(in$jobname, '<br>Начинаем выключение индекса '||in$index_name);
--
--  --Убираем лишние пробелы и переводим в верхний регистр ' Слива садовая ' --> 'СЛИВА САДОВАЯ'
--  v$index_name := upper(trim(in$index_name));
--  util.pkg$job_control.add_log(in$jobname,'Имя изменено '||in$index_name||'-->'||v$index_name, var$log_level=>1);
--
--  --Проверяем, что формат введённого индекса правильный: начало строки + произвольное количество букв/цифр/_/$ + одна точка + произвольное количество букв/цифр/_/$ + конец строки
--  select count(*) into v$cnt
--  from
--  (
--      select v$index_name from dual
--      where regexp_like (v$index_name,'^[A-Za-z0-9_\$]+\.{1}[A-Za-z0-9_\$]+$')
--  );
--  if v$cnt != 1 then raise UNCORRECT_FORMAT; end if;
--
--  -- Получаем схему и имя индекса 'SRC.INDEX_PK' --> 'SRC' + 'INDEX_PK'
--  v$index_owner := substr(v$index_name,1,instr(v$index_name,'.')-1);
--  v$index       := substr(v$index_name,instr(v$index_name,'.')+1);
--  util.pkg$job_control.add_log(in$jobname,'OWNER:'||v$index_owner, var$log_level=>1);
--  util.pkg$job_control.add_log(in$jobname,'INDEX:'||v$index, var$log_level=>1);
--
--  -- Проверяем, существует ли заданный индекс
--  select count(*) into v$cnt
--  from dba_indexes
--  where OWNER      = v$index_owner
--    and INDEX_NAME = v$index;
--  if v$cnt != 1 then raise UNEXIST_INDEX; end if;
--
--  -- Проверяем, не отключен ли уже индекс
--  select count(*) into  v$cnt
--  from dba_indexes
--  where OWNER      = v$index_owner
--    and INDEX_NAME = v$index
--    and status     = 'VALID';
--  if v$cnt != 1 then raise INVALID_INDEX; end if;
--
--  begin --Проверяем, нет ли на этом индексе constraint
--      select constraint_name, owner, table_name into v$constraint, v$constraint_owner, v$table
--      from dba_constraints
--        where INDEX_OWNER = v$index_owner
--        and   INDEX_NAME  = v$index
--        and   constraint_type in ('P', 'U'); -- P = Primary key; U = Unique
--        util.pkg$job_control.add_log(in$jobname,'Найден CONSTRAINT', var$log_level=>1);
--        util.pkg$job_control.add_log(in$jobname,'CONSTRAINT_OWNER:'||v$constraint_owner, var$log_level=>1);
--        util.pkg$job_control.add_log(in$jobname,'CONSTRAINT:'||v$constraint, var$log_level=>1);
--      exception when NO_DATA_FOUND then null;
--  end;
--
--  -- Если есть constraint на индексе, то отключаем его
--  if v$constraint is not null then
--    begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER TABLE '||v$constraint_owner||'.'||v$table||' MODIFY CONSTRAINT '||v$constraint||' DISABLE KEEP INDEX', var$log_level=>1);
--    peo.exe_sql(('ALTER TABLE '||v$constraint_owner||'.'||v$table||' MODIFY CONSTRAINT '||v$constraint||' DISABLE KEEP INDEX'));
--    exception when others then raise UNABLE_TO_OFF_CONSTRAINT;
--    end;
--  end if;
--
--  -- Отключаем индекс
--  begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER INDEX '||v$index_owner||'.'||v$index||' UNUSABLE', var$log_level=>1);
--    peo.exe_sql('ALTER INDEX '||v$index_owner||'.'||v$index||' UNUSABLE');
--    util.pkg$job_control.add_log(in$jobname,'Готово!', var$log_level=>1);
--    exception
--    when others then raise UNABLE_TO_OFF_INDEX;
--  end;
--
--  --Изменяем параметры сессии, чтобы можно было совершать действия над таблицами с unusable index:
--  begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER SESSION SET SKIP_UNUSABLE_INDEXES  = TRUE', var$log_level=>1);
--    peo.exe_sql('ALTER SESSION SET SKIP_UNUSABLE_INDEXES = TRUE');
--    util.pkg$job_control.add_log(in$jobname,'ALTER SESSION SET SKIP_INDEX_MAINTENANCE = TRUE', var$log_level=>1);
--    peo.exe_sql('ALTER SESSION SET SKIP_INDEX_MAINTENANCE = TRUE');
--    util.pkg$job_control.add_log(in$jobname,'Готово!', var$log_level=>1);
--    exception
--    when others then raise UNABLE_TO_ALTER_SESSION;
--  end;
--
--
--  --util.pkg$job_control.end_log(in$jobname, sysdate);
--
--
--  -- Заготовки для сложных constraint
--
----  -- Получаем таблицу, к которой привязан индекс:
----  select table_name, table_owner into v$table, v$table_owner
----  from dba_indexes
----    where OWNER      = v$index_owner
----    and   INDEX_NAME = v$index;
----    util.pkg$job_control.add_log(in$jobname,'TABLE_OWNER:'||v$table_owner);
----    util.pkg$job_control.add_log(in$jobname,'TABLE:'||v$table);
----
--
--  --Если есть, то отключаем
--    -- Получаем все колонки, на которые ссылается данный индекс. Часть 1 — обычный индекс
----  for my_cursor in
----  (
----      select column_name
----      from all_ind_columns
----      where index_owner = v$index_owner
----        and index_name  = v$index
----        and column_name not like 'SYS%$'
----  )
----  loop
----    --v$cloumn_list(1) := my_cursor.column_name;
----    util.pkg$job_control.add_log(in$jobname,'COLUMN INDEXED1:'||my_cursor.column_name);
----  end loop;
----
----  -- Получаем все колонки, на которые ссылается данный индекс. Часть 2 — сложный или многосоставной индекс
----
----  for my_cursor in
----  (
----      select c.column_name
----      from all_ind_expressions  e
----      left join all_tab_columns c on  e.table_name = c.table_name
----                                  and e.table_owner = c.owner
----                                  and e.column_position = c.column_id
----      where e.index_owner = 'EVSEEV'
----  )
----  loop
----    --v$cloumn_list(1) := my_cursor.column_name;
----    util.pkg$job_control.add_log(in$jobname,'COLUMN INDEXED2:'||my_cursor.column_name);
----  end loop;
--
--
--
--  exception
--      when UNCORRECT_FORMAT          then util.pkg$job_control.send_warning(in$jobname,'Неверный формат индекса');
--      when UNEXIST_INDEX             then util.pkg$job_control.send_warning(in$jobname,'Индекс не существует');
--      when INVALID_INDEX             then util.pkg$job_control.send_warning(in$jobname,'Индекс уже отключен');
--      when UNABLE_TO_OFF_INDEX       then util.pkg$job_control.send_warning(in$jobname,'Не удалось отключить индекс');
--      when UNABLE_TO_OFF_CONSTRAINT  then util.pkg$job_control.send_warning(in$jobname,'Не удалось отключить constraint');
--      when UNABLE_TO_ALTER_SESSION   then util.pkg$job_control.send_warning(in$jobname,'Не удалось изменить параметры сессии');
--      when others                    then util.pkg$job_control.send_error(in$jobname,'Неизвестная ошибка:'||SQLERRM); RAISE;
--  end index_off;
--
--
--  procedure index_on(in$index_name varchar2)
--  is
--  begin
--  index_on('UTIL.COMMON_LOGS',in$index_name);
--  exception when others then raise;
--  end;
--  procedure index_on(in$jobname varchar2,in$index_name varchar2)
--  is
--  v$index_name       varchar2(100);
--
--  v$index            varchar2(100);
--  v$index_owner      varchar2(100);
--
--  v$table            varchar2(100);
--  v$table_owner      varchar2(100);
--
--  v$constraint       varchar2(100);
--  v$constraint_owner varchar2(100);
--
--  v$cnt              number(2);
--
--  UNCORRECT_FORMAT         exception;
--  UNEXIST_INDEX            exception;
--  ALREDY_VALID_INDEX       exception;
--  UNABLE_TO_ON_INDEX       exception;
--  UNABLE_TO_ON_CONSTRAINT  exception;
--  UNABLE_TO_ALTER_SESSION  exception;
--
--  begin
--
--  --util.pkg$job_control.start_log(in$jobname);
--  util.pkg$job_control.add_nodate_log(in$jobname, '<br>Начинаем включение индекса '||in$index_name);
--
--  --аналогично index_off
--  v$index_name := upper(trim(in$index_name));
--  util.pkg$job_control.add_log(in$jobname,'Имя изменено '||in$index_name||'-->'||v$index_name, var$log_level=>1);
--
--  --Проверяем, что формат введённого индекса правильный: начало строки + произвольное количество букв/цифр/_/$ + одна точка + произвольное количество букв/цифр/_/$ + конец строки
--  select count(*) into v$cnt
--  from
--  (
--      select v$index_name from dual
--      where regexp_like (v$index_name,'^[A-Za-z0-9_\$]+\.{1}[A-Za-z0-9_\$]+$')
--  );
--  if v$cnt != 1 then raise UNCORRECT_FORMAT; end if;
--
--  -- Получаем схему и имя индекса 'SRC.INDEX_PK' --> 'SRC' + 'INDEX_PK'
--  v$index_owner := substr(v$index_name,1,instr(v$index_name,'.')-1);
--  v$index       := substr(v$index_name,instr(v$index_name,'.')+1);
--  util.pkg$job_control.add_log(in$jobname,'OWNER:'||v$index_owner, var$log_level=>1);
--  util.pkg$job_control.add_log(in$jobname,'INDEX:'||v$index, var$log_level=>1);
--
--  -- Проверяем, существует ли заданный индекс
--  select count(*) into v$cnt
--  from dba_indexes
--  where OWNER      = v$index_owner
--    and INDEX_NAME = v$index;
--  if v$cnt != 1 then raise UNEXIST_INDEX; end if;
--
--  -- Проверяем, не включен ли уже индекс
--  select count(*) into  v$cnt
--  from dba_indexes
--  where OWNER      = v$index_owner
--    and INDEX_NAME = v$index
--    and status     = 'VALID';
--  if v$cnt  = 1 then raise ALREDY_VALID_INDEX; end if;
--
--  begin --Проверяем, нет ли на этом индексе constraint
--        --Поскольку при отключении constraint падает его связка с индексом, если он был создан при создании constraint
--        --То искать можно только по совпадению имени индекса и constraint, что адекватно
--        --т.к. вспомогательные индексы при создании constraint создаются с именем constraint
--      select constraint_name, owner, table_name into v$constraint, v$constraint_owner, v$table
--      from dba_constraints
--        where OWNER = v$index_owner
--        and   CONSTRAINT_NAME  = v$index
--        and   constraint_type in ('P', 'U'); -- P = Primary key; U = Unique
--        util.pkg$job_control.add_log(in$jobname,'Найден CONSTRAINT', var$log_level=>1);
--        util.pkg$job_control.add_log(in$jobname,'CONSTRAINT_OWNER:'||v$constraint_owner, var$log_level=>1);
--        util.pkg$job_control.add_log(in$jobname,'CONSTRAINT:'||v$constraint, var$log_level=>1);
--      exception when NO_DATA_FOUND then null;
--  end;
--
--  -- Включаем индекс
--  begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER INDEX '||v$index_owner||'.'||v$index||' REBUILD', var$log_level=>1);
--    peo.exe_sql('ALTER INDEX '||v$index_owner||'.'||v$index||' REBUILD');
--    exception
--    when others then raise UNABLE_TO_ON_INDEX;
--  end;
--
--    -- Если есть constraint на индексе, то включаем его
--  if v$constraint is not null then
--    begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER TABLE '||v$constraint_owner||'.'||v$table||' MODIFY CONSTRAINT '||v$constraint||' ENABLE VALIDATE', var$log_level=>1);
--    peo.exe_sql(('ALTER TABLE '||v$constraint_owner||'.'||v$table||' MODIFY CONSTRAINT '||v$constraint||' ENABLE VALIDATE'));
--    exception when others then raise UNABLE_TO_ON_CONSTRAINT;
--    end;
--  end if;
--
--  --Изменяем параметры сессии — возвращаем запрет на работу с unusable индексами
--  begin
--    util.pkg$job_control.add_log(in$jobname,'ALTER SESSION SET SKIP_UNUSABLE_INDEXES = FALSE', var$log_level=>1);
--    peo.exe_sql('ALTER SESSION SET SKIP_UNUSABLE_INDEXES = FALSE');
--    util.pkg$job_control.add_log(in$jobname,'ALTER SESSION SET SKIP_INDEX_MAINTENANCE  = FALSE', var$log_level=>1);
--    peo.exe_sql('ALTER SESSION SET SKIP_INDEX_MAINTENANCE = FALSE');
--    util.pkg$job_control.add_log(in$jobname,'Готово!', var$log_level=>1);
--    exception
--    when others then raise UNABLE_TO_ALTER_SESSION;
--  end;
--
--  util.pkg$job_control.add_log(in$jobname,'Готово!', var$log_level=>1);
--
--  --util.pkg$job_control.end_log(in$jobname,sysdate);
--
--  exception
--  when UNCORRECT_FORMAT         then util.pkg$job_control.send_warning(in$jobname,'Неверный формат индекса');
--  when UNEXIST_INDEX            then util.pkg$job_control.send_warning(in$jobname,'Индекс не существует');
--  when ALREDY_VALID_INDEX       then util.pkg$job_control.send_warning(in$jobname,'Индекс уже включен');
--  when UNABLE_TO_ON_INDEX       then util.pkg$job_control.send_warning(in$jobname,'Не удалось отключить индекс');
--  when UNABLE_TO_ON_CONSTRAINT  then util.pkg$job_control.send_warning(in$jobname,'Не удалось отключить constraint');
--  when UNABLE_TO_ALTER_SESSION   then util.pkg$job_control.send_warning(in$jobname,'Не удалось изменить параметры сессии');
--  when others then util.pkg$job_control.send_error(in$jobname,'Неизвестная ошибка:'||SQLERRM); RAISE;
--  end index_on;

  /*PUBLIC*/
    procedure p$sleep_until_rback_idle (in$jobname varchar2)
    is
       c$hour_window_open  constant number(2) := 20;
       c$hour_window_close constant number(2) :=  9;
       v$sec_to_wait                number(6) :=  0;
       v$current_hour               number(2) := to_number(to_char(sysdate,'hh24'));
   begin

       if (v$current_hour between c$hour_window_close + 1 and c$hour_window_open - 1) then
           begin
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидаем часа '||to_char(c$hour_window_open)||'...', in$level => 'notice'); end if;
              v$sec_to_wait := round(24*60*60*( (trunc(sysdate,'dd') + c$hour_window_open/24 - sysdate)));
              dbms_lock.sleep(v$sec_to_wait);
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидание завершено', in$level => 'info'); end if;
           end;
           else
           begin
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Процедура запущена внутри допустимого для выгрузок с RBACK окна '
                                                                                ||to_char(c$hour_window_open)||' – '||to_char(c$hour_window_close)||' часов', in$level => 'info');
              end if;
           end;
       end if;
   exception when others then
       util.pkg$log.p$add_trace(in$jobname);
       raise;
   end p$sleep_until_rback_idle;


  procedure p$sleep_until (in$jobname varchar2, in$date_to_sleep in date)
  is
       v$sec integer;
   begin
       v$sec := round(24*60*60*(in$date_to_sleep - sysdate));
  if (v$sec > 0) then
           begin
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидаем до '||to_char(in$date_to_sleep,'dd.mm.yyyy hh24:mi:ss')||'...', in$level => 'notice'); end if;
              dbms_lock.sleep(v$sec);
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидание завершено', in$level => 'info'); end if;
           end;
           else
           begin
              if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Дата ожидания '||to_char(in$date_to_sleep,'dd.mm.yyyy hh24:mi:ss')||' уже прошла.', in$level => 'info'); end if;
           end;
       end if;
   exception when others then
       util.pkg$log.p$add_trace(in$jobname);
       raise;
   end;

  procedure p$sleep_for (in$jobname varchar2, in$minutes_to_sleep in number)
  is
   begin
       if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидаем '||to_char(round(in$minutes_to_sleep,2))||' мин. ...', in$level => 'notice'); end if;
       dbms_lock.sleep(in$minutes_to_sleep*60);
       if (in$jobname is not null) then util.pkg$log.p$add_log(in$jobname,'Ожидание завершено', in$level => 'info'); end if;
   exception when others then
       util.pkg$log.p$add_trace(in$jobname);
       raise;
   end;

  procedure p$mv_refresh(in$jobname varchar2, in$ownname varchar2, in$mvname varchar2, in$refresh_type varchar2)
  is
    v$cnt number(2);
    v$refresh_type varchar2(50);
    MVIEW_DOESNT_EXIST exception;
    REFRESH_TYPE_ERROR exception;
  begin

      select decode(lower(in$refresh_type), 'fast', 'f', 'complete', 'c', 'pct', 'p', lower(in$refresh_type)) into v$refresh_type from dual;

      if nvl(v$refresh_type,'x') not in ('f','c','p') then raise REFRESH_TYPE_ERROR; end if;

      util.pkg$log.p$add_log(in$jobname,'Обновляем materialized view <b>'||upper(in$ownname)||'.'||upper(in$mvname)||'</b>', in$level => 'notice');

      select count(*) into v$cnt
      from sys.all_mviews
      where owner = upper(in$ownname)
        and mview_name = upper(in$mvname);
      if (v$cnt < 1) then raise MVIEW_DOESNT_EXIST; end if;

      execute immediate 'alter session set "_mv_refresh_use_stats" = true';

      if (v$refresh_type = 'f') then
          util.pkg$log.p$add_log(in$jobname,'Метод fast refresh', in$level => 'info');
          dbms_snapshot.refresh(list => upper(in$ownname)||'.'||upper(in$mvname), method => 'f');
      elsif (v$refresh_type = 'c') then
          util.pkg$log.p$add_log(in$jobname,'Метод full refresh', in$level => 'info');
          dbms_snapshot.refresh(list => upper(in$ownname)||'.'||upper(in$mvname), method => 'c');
      elsif (v$refresh_type = 'p') then
          util.pkg$log.p$add_log(in$jobname,'Метод pct refresh', in$level => 'info');
          begin
             dbms_snapshot.refresh(list => upper(in$ownname)||'.'||upper(in$mvname), method => 'p');
          exception when others then
             util.pkg$log.p$add_log(in$jobname, 'Обновление matview <b>'||upper(in$ownname)||'.'||upper(in$mvname)||'</b> по технологии PCT завершилось с ошибкой. Пробуем полное обновление.', in$level => 'warning');
             peo.pkg$ddl.p$mv_refresh(in$jobname, in$ownname, in$mvname, 'complete');
          end;
      end if;

      util.pkg$log.p$add_log(in$jobname,'Обновление materialized view <b>'||upper(in$ownname)||'.'||upper(in$mvname)||'</b> успешно завершено!', in$level => 'info');

      exception
      when MVIEW_DOESNT_EXIST then util.pkg$log.p$add_log(in$jobname,'Указанный Meterialized View не существует!', in$level => 'warning'); util.pkg$log.p$add_trace(in$jobname); raise;
      when REFRESH_TYPE_ERROR then util.pkg$log.p$add_log(in$jobname,'Указанный refresh_type '||in$refresh_type||' недопустим.
           Возможные значения: ''f'' или ''fast'' для пересборки по логам, ''c'' или ''complete'' для полной пересборки, ''p'' или ''pct'' для пересборки по партициям.', in$level => 'warning'); util.pkg$log.p$add_trace(in$jobname); raise;
      when others             then util.pkg$log.p$add_trace(in$jobname); raise;
  end p$mv_refresh;

  procedure p$set_sort_area(in$jobname varchar2, in$sizeMB number default 100)
  is
  v$hash_area_size number;
  v$sort_area_size number;
  TOO_LARGE        exception;
  begin

    if in$sizeMB > 200 then raise TOO_LARGE;
    end if;

    select value into v$hash_area_size from v$parameter where name = 'hash_area_size';
    select value into v$sort_area_size from v$parameter where name = 'sort_area_size';

    util.pkg$log.p$add_log(in$jobname,'Увеличиваем выделенную под хэш-соединение и сортировку память до '||to_char(in$sizeMB)||' МБ', 'notice', 'nodate');

    util.pkg$log.p$add_log(in$jobname,'<br>Параметры сессии до изменения:', in$level => 'info');
    util.pkg$log.p$add_log(in$jobname,'hash_area_size = '||util.pkg$f.f$to_char(v$hash_area_size,0)||' байт', 'info', 'nodate');
    util.pkg$log.p$add_log(in$jobname,'sort_area_size = '||util.pkg$f.f$to_char(v$sort_area_size,0)||' байт', 'info', 'nodate');

    execute immediate 'alter session set WORKAREA_SIZE_POLICY = MANUAL';
    execute immediate 'alter session set hash_area_size = '||to_char(in$sizeMB*power(2,20));
    execute immediate 'alter session set sort_area_size = '||to_char(in$sizeMB*power(2,20));

    select value into v$hash_area_size from v$parameter where name = 'hash_area_size';
    select value into v$sort_area_size from v$parameter where name = 'sort_area_size';

    util.pkg$log.p$add_log(in$jobname,'Параметры сессии после изменения (устанавливаемое значение доступной памяти = '||in$sizeMB||' МБ):', 'info');
    util.pkg$log.p$add_log(in$jobname,'hash_area_size = '||util.pkg$f.f$to_char(v$hash_area_size,0)||' байт', 'info', 'nodate');
    util.pkg$log.p$add_log(in$jobname,'sort_area_size = '||util.pkg$f.f$to_char(v$sort_area_size,0)||' байт', 'info', 'nodate');

  exception
    when TOO_LARGE then util.pkg$log.p$add_log(in$jobname,'Объём занимаемой памяти не может превышать 200 МБ!', in$level => 'warning');
    when others then util.pkg$log.p$add_trace(in$jobname);
  end p$set_sort_area;


  procedure p$trunc(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2, in$partname date, in$rebuild varchar2 default 'rebuild')
  is
  v$cnt          number(2);
  v$current_user varchar2(30);
  v$ownname      varchar2(30);
  v$tabname      varchar2(40);
  v$partname     varchar2(40);
  v$update       varchar2(40);

  v$locking_user    varchar2(30);
  v$locking_session number(10);

  INCORRECT_SCHEME_FOR_PARTITION   exception;
  INCORRECT_SCHEME_FOR_TABLE       exception;
  INCORRECT_PARTITION              exception;
  INCORRECT_TABLE                  exception;
  INCORRECT_DATE                   exception;
  LOCK_DETECTED                    exception;

  PRAGMA EXCEPTION_INIT(LOCK_DETECTED, -54); /* Ошибка залочивания объекта, который пытаемся очистить*/

  begin

  v$ownname := upper(in$ownname);
  v$tabname := upper(in$tabname);
  if (in$partname = c$maxdate) then v$partname := upper(v$tabname||'_XX_XX');
                             else v$partname := upper(v$tabname||'_'||to_char(in$partname,'yy_mm'));
  end if;

  -- Проверка на существование таблицы
  if (f$table_exist(v$ownname, v$tabname) != 1) then raise INCORRECT_TABLE; end if;

   if (in$partname is not null) then

      util.pkg$log.p$add_log(in$jobname, 'Truncate таблицы <b>'||v$ownname||'.'||v$tabname||' партиция '||v$partname||'</b>', in$level => 'notice');

      if in$partname != last_day(in$partname) then raise INCORRECT_DATE; end if;

       -- Получаем текущего пользователя
      select username
      into v$current_user
      from user_users;

      --Проверяем, что таблица в домашней схеме
      if (v$ownname != v$current_user and v$current_user != 'PEO') then raise INCORRECT_SCHEME_FOR_PARTITION; end if;

      --Проверяем существование выбраной партиции
      if (f$partition_exist(v$ownname, v$tabname, v$partname) != 1) then raise INCORRECT_PARTITION; end if;

      if (lower(trim(in$rebuild)) = 'rebuild') then v$update := ' UPDATE GLOBAL INDEXES'; else v$update := ''; end if;

      util.pkg$log.p$add_log (in$jobname,'ALTER TABLE '||v$ownname||'.'||v$tabname||' TRUNCATE PARTITION '||v$partname||v$update, in$level => 'info');
      execute immediate 'ALTER TABLE '||v$ownname||'.'||v$tabname||' TRUNCATE PARTITION '||v$partname||v$update ;
   else
       --Проверяем, что таблица в схеме TMP
       util.pkg$log.p$add_log(in$jobname, 'Truncate таблицы <b>'||v$ownname||'.'||v$tabname||'</b>', in$level => 'notice');
       if (v$ownname not in ('TMP', 'ETL')) then raise INCORRECT_SCHEME_FOR_TABLE; end if;

      util.pkg$log.p$add_log(in$jobname,'TRUNCATE TABLE '||v$ownname||'.'||v$tabname, in$level => 'info');
       execute immediate 'TRUNCATE TABLE '||v$ownname||'.'||v$tabname;
   end if;

   util.pkg$log.p$add_log(in$jobname,'Truncate завершен!', in$level => 'info');

  exception
      when LOCK_DETECTED                  then
         util.pkg$log.p$add_table(in$jobname,
        'select session_id, oracle_username, os_user_name, decode (
            locked_mode,
            0, ''None'',
            1, ''Null'',
            2, ''Row-S (SS)'',
            3, ''Row-X (SX)'',
            4, ''Share'',
            5, ''S/Row-X (SSX)'',
            6, ''Exclusive'',
            to_char (locked_mode)
         ) lock_mode
         , a.sql_text
         from v$locked_object l
         inner join sys.all_objects o on l.object_id = o.object_id
         inner join v$session s on l.session_id = s.sid
         left join v$sqlarea a on s.sql_id = a.sql_id
         where upper(owner) = '''||v$ownname||'''
           and upper(object_name) = '''||v$tabname||'''',
         '<tr><th>Идентификатор сессии</th><th>Имя пользователя Oracle</th><th>Имя пользователя ОС</th><th>Тип блокировки</th><th>Текст запроса</th></tr>'
       , 'Блокировки таблицы '||v$ownname||'.'||v$tabname, 'warning');
       raise;

      when INCORRECT_SCHEME_FOR_TABLE     then util.pkg$log.p$add_log(in$jobname,'Truncate таблиц можно выполнять только во временных (TMP/ETL) схемах', in$level => 'warning'); raise;
      when INCORRECT_SCHEME_FOR_PARTITION then util.pkg$log.p$add_log(in$jobname,'Truncate партиций можно выполнять только в домашней ('||v$current_user||') схеме', in$level => 'warning'); raise;
      when INCORRECT_PARTITION            then util.pkg$log.p$add_log(in$jobname,'Партиция '||v$partname||' не существует', in$level => 'warning'); raise;
      when INCORRECT_TABLE                then util.pkg$log.p$add_log(in$jobname,'Таблица '||v$ownname||'.'||v$tabname||' не существует. Truncate невозможен', in$level => 'warning'); raise;
      when INCORRECT_DATE                 then util.pkg$log.p$add_log(in$jobname,'Входная дата ('||to_char(in$partname,'dd.mm.yyyy')||')
                                               должна быть последним днём очищаемого месяца ('||to_char(last_day(in$partname),'dd.mm.yyyy')||')', in$level => 'warning'); raise;
      when others                         then util.pkg$log.p$add_trace(in$jobname); raise;
  end p$trunc;

procedure p$trunc(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2)
is
begin
    p$trunc(in$jobname => in$jobname, in$ownname => in$ownname, in$tabname => in$tabname, in$partname => null);
    exception when others then util.pkg$log.p$add_trace(in$jobname); raise;
end p$trunc;

  procedure p$stat(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2, in$partname date)
  is
  v$ownname      varchar2(30);
  v$tabname      varchar2(40);
  v$partname     varchar2(40);
  v$cnt number(10);

  INCORRECT_DATE      exception;
  INCORRECT_FORMAT    exception;
  INCORRECT_PARTITION exception;
  begin

  v$ownname := upper(in$ownname);
  v$tabname := upper(in$tabname);
  if (in$partname = c$maxdate) then v$partname := upper(v$tabname||'_XX_XX');
                               else v$partname := upper(v$tabname||'_'||to_char(in$partname,'yy_mm'));
  end if;


  --Проверяем, что такая таблица есть
  if (f$table_exist(v$ownname, v$tabname) != 1) then raise INCORRECT_FORMAT; end if;

  if (in$partname is not null) then

      util.pkg$log.p$add_log(in$jobname, 'Собираем статистику по таблице <b>'||v$ownname||'.'||v$tabname||'</b>, партиция: <b>'||v$partname||'</b>', in$level => 'notice');

      -- Проверяем, что дата введена правильно
      if (in$partname != last_day(in$partname)) then raise INCORRECT_DATE; end if;

      -- Проверяем существование выбраной партиции
      --Проверяем существование выбраной партиции
      if (f$partition_exist(v$ownname, v$tabname, v$partname) != 1) then raise INCORRECT_PARTITION; end if;

      dbms_stats.gather_table_stats(v$ownname, v$tabname, v$partname, granularity => 'partition', cascade => TRUE );

  else
      util.pkg$log.p$add_log(in$jobname, 'Собираем статистику по таблице <b>'||v$ownname||'.'||v$tabname||'</b>', in$level => 'notice');
      dbms_stats.gather_table_stats(v$ownname, v$tabname, cascade => TRUE);
  end if;

  util.pkg$log.p$add_log(in$jobname, 'Статистика собрана', in$level => 'info');

  exception
     when INCORRECT_FORMAT    then util.pkg$log.p$add_log(in$jobname, 'Неверный формат таблицы '||v$ownname||'.'||v$tabname, in$level => 'warning'); util.pkg$log.p$add_trace(in$jobname);
     when INCORRECT_PARTITION then util.pkg$log.p$add_log(in$jobname, 'Партиция '||v$partname||' не существует' , in$level => 'warning'); util.pkg$log.p$add_trace(in$jobname);
     when INCORRECT_DATE      then util.pkg$log.p$add_log(in$jobname,'Входная дата ('||to_char(in$partname,'dd.mm.yyyy')||')
                                   должна быть последним днём очищаемого месяца ('||to_char(last_day(in$partname),'dd.mm.yyyy')||')', in$level => 'warning'); raise;
     when others then util.pkg$log.p$add_trace(in$jobname);
  end p$stat;

  procedure p$stat(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2)
  is
  begin
    p$stat(in$jobname => in$jobname, in$ownname => in$ownname, in$tabname => in$tabname, in$partname => null);
  exception
     when others then util.pkg$log.p$add_trace(in$jobname);
  end p$stat;


    procedure p$exchange(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2, in$partname date)
    is
    v$ownname         varchar2(30);
    v$tabname         varchar2(40);
    v$partname        varchar2(40);

    v$second_ownname  varchar2(30);
    v$second_tabname  varchar2(40);
    v$second_partname varchar2(40);

    v$buffer_ownname  varchar2(30);
    v$buffer_tabname  varchar2(40);

    v$cnt number(10);

    TARGET_TABLE_UNEXIST exception;
    TARGET_PART_UNEXIST  exception;
    TARGET_GLOBAL_INDEX  exception;

    SOURCE_TABLE_UNEXIST exception;
    SOURCE_PART_UNEXIST  exception;
    SOURCE_GLOBAL_INDEX  exception;

    BUFFER_TABLE_UNEXIST exception;

    INCORRECT_DATE       exception;

    begin

        v$ownname        := upper(in$ownname);
        v$tabname        := upper(in$tabname);
        if (in$partname = c$maxdate) then v$partname := upper(v$tabname||'_XX_XX');
                                     else v$partname := upper(v$tabname||'_'||to_char(in$partname,'yy_mm'));
        end if;

        -- Заменяемая таблица должна быть формата tmp.имя_целевой_таблицы
        v$second_ownname  := 'TMP';
        v$second_tabname  := upper(in$tabname);

       -- Проверяем наличие таблиц, которые будут обмениваться данными
       if (f$table_exist(v$ownname,v$tabname) != 1)               then raise TARGET_TABLE_UNEXIST; end if;
       if (f$table_exist(v$second_ownname,v$second_tabname) != 1) then raise SOURCE_TABLE_UNEXIST; end if;


        if (in$partname is null) then -- Обмен между двумя таблицами, реализуется, как обмен целевой таблицы и времянки с одной партицией
        begin
            -- Особая партиция темповой таблицы должна называться TABNAME$X
            v$second_partname := v$second_tabname||'$X';

            util.pkg$log.p$add_log(in$jobname, 'Обмен партициями: '||v$ownname||'.'||v$tabname||' <—> '||v$second_ownname||'.'||v$second_tabname||'.'||v$second_partname, in$level => 'notice');

            if (f$partition_exist(v$second_ownname,v$second_tabname,v$second_partname) != 1) then raise SOURCE_PART_UNEXIST; end if;
            if (f$global_index_exist(v$second_ownname,v$second_tabname) != 0)                then raise SOURCE_GLOBAL_INDEX;      end if;

            util.pkg$log.p$add_log(in$jobname, 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$ownname||'.'||v$tabname||' including indexes', 'info');
            execute immediate 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$ownname||'.'||v$tabname||' including indexes';
        end;
        else
        begin
            -- Проверяем, что дата введена правильно
            if (in$partname != last_day(in$partname)) then raise INCORRECT_DATE; end if;

            v$second_partname := v$partname;

            -- Узнаём, темповая таблица имеет партици с нужным именем или нет
            if (f$partition_exist(v$second_ownname,v$second_tabname,v$second_partname) = 0) then -- Обмен партиционированной таблицы с темповой
            begin

                util.pkg$log.p$add_log(in$jobname, 'Обмен партициями: '||v$ownname||'.'||v$tabname||'.'||v$partname||' <—> '||v$second_ownname||'.'||v$second_tabname, in$level => 'notice');

                if (f$partition_exist(v$ownname,v$tabname,v$partname) != 1) then raise TARGET_PART_UNEXIST; end if;
                if (f$global_index_exist(v$ownname,v$tabname) != 0)         then raise TARGET_GLOBAL_INDEX;      end if;

                util.pkg$log.p$add_log(in$jobname, 'alter table '||v$ownname||'.'||v$tabname||' exchange partition '||v$partname||' with table '||v$second_ownname||'.'||v$second_tabname||' including indexes', 'info');
                execute immediate 'alter table '||v$ownname||'.'||v$tabname||' exchange partition '||v$partname||' with table '||v$second_ownname||'.'||v$second_tabname||' including indexes';

            end;
            else -- Обмен двух партиционированных таблиц
            begin

               -- Определяем параметры буферной таблицы
               v$buffer_ownname  := 'TMP';
               v$buffer_tabname  := upper(in$tabname)||'$X';

               util.pkg$log.p$add_log(in$jobname, 'Обмен партициями: '||v$ownname||'.'||v$tabname||'.'||v$partname||' <—> '||v$buffer_ownname||'.'||v$buffer_tabname||' <—> '||v$second_ownname||'.'||v$second_tabname||'.'||v$second_partname, in$level => 'notice');

               if (f$partition_exist(v$ownname,v$tabname,v$partname) != 1)                      then raise TARGET_PART_UNEXIST;  end if;
               if (f$global_index_exist(v$ownname,v$tabname) != 0)                              then raise TARGET_GLOBAL_INDEX;  end if;
               if (f$partition_exist(v$second_ownname,v$second_tabname,v$second_partname) != 1) then raise SOURCE_PART_UNEXIST;  end if;
               if (f$global_index_exist(v$second_ownname,v$second_tabname) != 0)                then raise SOURCE_GLOBAL_INDEX;  end if;
               if (f$table_exist(v$buffer_ownname,v$buffer_tabname) != 1)                       then raise BUFFER_TABLE_UNEXIST; end if;

               util.pkg$log.p$add_log(in$jobname, 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes', 'info');
               execute immediate 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes';

               util.pkg$log.p$add_log(in$jobname, 'alter table '||v$ownname||'.'||v$tabname||' exchange partition '||v$partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes', 'info');
               execute immediate 'alter table '||v$ownname||'.'||v$tabname||' exchange partition '||v$partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes';

               util.pkg$log.p$add_log(in$jobname, 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes', 'info');
               execute immediate 'alter table '||v$second_ownname||'.'||v$second_tabname||' exchange partition '||v$second_partname||' with table '||v$buffer_ownname||'.'||v$buffer_tabname||' including indexes';


            end;
            end if;

        end;
        end if;

    util.pkg$log.p$add_log(in$jobname, 'Обмен партициями успешно завершен', in$level => 'info');

    exception
    when TARGET_TABLE_UNEXIST then util.pkg$log.p$add_log(in$jobname, 'Неверный формат таблицы-приёмника '||v$ownname||'.'||v$tabname, in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when TARGET_PART_UNEXIST  then util.pkg$log.p$add_log(in$jobname, 'Партиция '||v$partname||' в таблице-приёмнике '||v$ownname||'.'||v$tabname||' не существует' , in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when TARGET_GLOBAL_INDEX  then util.pkg$log.p$add_log(in$jobname, 'На партиционированной таблице-приёмнике '||v$ownname||'.'||v$tabname||' существуют глобальные индексы!' , in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when SOURCE_TABLE_UNEXIST then util.pkg$log.p$add_log(in$jobname, 'Неверный формат таблицы-источника '||v$second_ownname||'.'||v$second_tabname, in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when SOURCE_PART_UNEXIST  then util.pkg$log.p$add_log(in$jobname, 'Партиция '||v$second_partname||' в таблице-источнике '||v$second_ownname||'.'||v$second_tabname||' не существует' , in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when SOURCE_GLOBAL_INDEX  then util.pkg$log.p$add_log(in$jobname, 'На партиционированной таблице-источнике '||v$second_ownname||'.'||v$second_tabname||' существуют глобальные индексы!' , in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when BUFFER_TABLE_UNEXIST then util.pkg$log.p$add_log(in$jobname, 'Буферная таблица не существует '||v$buffer_ownname||'.'||v$buffer_tabname, in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when INCORRECT_DATE       then util.pkg$log.p$add_log(in$jobname, 'Дата отбора партиции ('||to_char(in$partname,'dd.mm.yyyy')||')
                                                                       должна быть последним днём месяца ('||to_char(last_day(in$partname),'dd.mm.yyyy')||')', in$level => 'warning');
                                   util.pkg$log.p$add_trace(in$jobname); raise;

    when others               then util.pkg$log.p$add_trace(in$jobname); raise;
    end p$exchange;

    procedure p$exchange(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2)
    is
    begin
    p$exchange(in$jobname, in$ownname, in$tabname, null);
    exception
    when others then util.pkg$log.p$add_trace(in$jobname); raise;
    end p$exchange;


  procedure p$compress(in$jobname varchar2, in$ownname varchar2, in$tabname varchar2, in$partname date)
  is
  v$ownname      varchar2(30);
  v$tabname      varchar2(40);
  v$partname     varchar2(40);
  v$cnt number(10);

  INCORRECT_DATE      exception;
  INCORRECT_FORMAT    exception;
  INCORRECT_PARTITION exception;
  UNEXIST_PARTITION   exception;
  BITMAP_INDEX        exception;

  begin

  if (in$partname is null) then raise UNEXIST_PARTITION; end if;

  v$ownname := upper(in$ownname);
  v$tabname := upper(in$tabname);

  if (in$partname = c$maxdate) then v$partname := upper(v$tabname||'_XX_XX');
                               else v$partname := upper(v$tabname||'_'||to_char(in$partname,'yy_mm'));
  end if;

  util.pkg$log.p$add_log(in$jobname, 'Ужимаем таблицу <b>'||in$ownname||'.'||v$tabname||'</b>, партицию: <b>'||v$partname||'</b>', in$level => 'notice');

  --Проверяем, что такая таблица есть
  if (f$table_exist(v$ownname, v$tabname) != 1) then raise INCORRECT_FORMAT; end if;

  -- Проверяем, что дата введена правильно
  if (in$partname != last_day(in$partname)) then raise INCORRECT_DATE; end if;

  -- Проверяем существование выбраной партиции
  if (f$partition_exist(v$ownname, v$tabname, v$partname) != 1) then raise INCORRECT_PARTITION; end if;

  -- Проверяем наличие битмап идекса на таблице без единой сжатой партиции. Такие автоматикой сжать не удастся.
  select count(*) into v$cnt
  from sys.all_indexes
  where table_owner = v$ownname
    and index_type  = 'BITMAP'
    and table_name  = v$tabname
    and not exists
    (
       select * from sys.all_tab_partitions
       where table_owner = v$ownname
         and table_name  = v$tabname
         and compression = 'ENABLED'
    );
  if v$cnt != 0 then raise BITMAP_INDEX; end if;


  util.pkg$log.p$add_log(in$jobname, 'ALTER TABLE '||v$ownname||'.'||v$tabname||' move partition '||v$partname||' compress update global indexes', in$level => 'notice');
                   execute immediate 'ALTER TABLE '||v$ownname||'.'||v$tabname||' move partition '||v$partname||' compress update global indexes' ;

  util.pkg$log.p$add_log(in$jobname, 'ALTER TABLE '||v$ownname||'.'||v$tabname||' modify partition '||v$partname||' rebuild unusable local indexes', in$level => 'notice');
                   execute immediate 'ALTER TABLE '||v$ownname||'.'||v$tabname||' modify partition '||v$partname||' rebuild unusable local indexes' ;

  util.pkg$log.p$add_log(in$jobname, 'Партиция сжата', in$level => 'info');

  exception
     when BITMAP_INDEX        then util.pkg$log.p$add_log(in$jobname, 'На сжимаемой таблице существует битмап индекс не в режиме компрессии<br>
                                                                       Для переведения индекса в режим компрессии, необходимо сжать хотя бы одну партицию таблицы,
                                                                       а затем перестроить битмап индекс полностью.', in$level => 'warning');                  util.pkg$log.p$add_trace(in$jobname);
     when UNEXIST_PARTITION   then util.pkg$log.p$add_log(in$jobname, 'Партиция должна быть указана', in$level => 'warning');                                  util.pkg$log.p$add_trace(in$jobname);
     when INCORRECT_FORMAT    then util.pkg$log.p$add_log(in$jobname, 'Неверный формат таблицы '||v$ownname||'.'||v$tabname, in$level => 'warning');           util.pkg$log.p$add_trace(in$jobname);
     when INCORRECT_PARTITION then util.pkg$log.p$add_log(in$jobname, 'Партиция не существует' , in$level => 'warning');                                       util.pkg$log.p$add_trace(in$jobname);
     when INCORRECT_DATE      then util.pkg$log.p$add_log(in$jobname,'Входная дата ('||to_char(in$partname,'dd.mm.yyyy')||')
                                   должна быть последним днём очищаемого месяца ('||to_char(last_day(in$partname),'dd.mm.yyyy')||')', in$level => 'warning');  util.pkg$log.p$add_trace(in$jobname);
     when others then util.pkg$log.p$add_trace(in$jobname); raise;
  end p$compress;


  procedure p$defragmentation (in$program_action varchar2, in$ownname varchar2, in$tabname varchar2)
is

    v$cnt number;
    v$ownname varchar2(2000);
    v$tabname varchar2(2000);

    UNEXIST_TABLE exception;

begin

    --util.pkg$log.p$test_mode('UTIL.COMMON_LOGS', 'ar-evseev@rsb.ru');

    v$ownname := upper(trim(in$ownname));
    v$tabname := upper(trim(in$tabname));

    util.pkg$log.p$add_log(in$program_action, '<span class = blue_btext>Дефрагментация таблицы '||v$ownname||'.'||v$tabname||' и связанных с ней индексов.</span>');

    --Проверяем, что такая таблица есть peo.pkg$ddl)
    if (f$table_exist(v$ownname, v$tabname) != 1) then raise UNEXIST_TABLE; end if;

    -- Смотрим, где лежат реальные данные, всего возможно 3 варианта
    -- 1. Данные лежат в таблице
    -- 2. Таблица является контейнером для набора партиций, которые содержат данные
    -- 3. Таблица является контейнером для набора партиций, каждая из которых — контейнер, содержащий набор субпартиций, которые содержат данные


    -- Проверяем субпартиции:
    select least(count(*),1) into v$cnt
    from sys.all_tab_subpartitions
    where table_owner = v$ownname
      and table_name  = v$tabname;

    if (v$cnt > 0) then

        for i in
        (
            select s.subpartition_name
            from sys.all_tab_subpartitions s
            where table_owner = v$ownname
              and table_name  = v$tabname
            order by s.subpartition_name
        )
        loop
            util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter table '||v$ownname||'.'||v$tabname||' move subpartition '||i.subpartition_name||'''', 'info');
            execute immediate 'alter table '||v$ownname||'.'||v$tabname||' move subpartition '||i.subpartition_name;
        end loop;
    else

        -- Проверяем партиции:
        select least(count(*),1) into v$cnt
        from sys.all_tab_partitions
        where table_owner = v$ownname
          and table_name  = v$tabname;

        if (v$cnt > 0) then

            for i in
            (
                select s.partition_name
                from sys.all_tab_partitions s
                where table_owner = v$ownname
                  and table_name  = v$tabname
                order by s.partition_name
            )
            loop
                util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter table '||v$ownname||'.'||v$tabname||' move partition '||i.partition_name||'''', 'info');
                execute immediate 'alter table '||v$ownname||'.'||v$tabname||' move partition '||i.partition_name;
            end loop;

        else

           -- Если это не субпартиции и не партиции, значит таблица
           util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter table '||v$ownname||'.'||v$tabname||' move''', 'info');
           execute immediate 'alter table '||v$ownname||'.'||v$tabname||' move';

        end if;

    end if;

    -- Перестраиваем индексы
    for i in
    (
       select i.owner, isp.subpartition_name, ip.partition_name, i.index_name
       from sys.all_indexes i
       left join sys.all_ind_partitions    ip  on i.index_name = ip.index_name and i.owner = ip.index_owner
       left join sys.all_ind_subpartitions isp on ip.partition_name = isp.partition_name and i.owner = isp.index_owner
       where i.table_owner = v$ownname
         and i.table_name = v$tabname
    )
    loop
        if (i.subpartition_name is not null) then
            util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter index '||i.owner||'.'||i.index_name||' rebuild subpartition '||i.subpartition_name||'''', 'info');
            execute immediate 'alter index '||i.owner||'.'||i.index_name||' rebuild subpartition '||i.subpartition_name;
        elsif (i.partition_name is not null) then
            util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter index '||i.owner||'.'||i.index_name||' rebuild partition '||i.partition_name||'''', 'info');
            execute immediate 'alter index '||i.owner||'.'||i.index_name||' rebuild partition '||i.partition_name;
        else
            util.pkg$log.p$add_log(in$program_action, 'execute immediate ''alter index '||i.owner||'.'||i.index_name||' rebuild', 'info');
            execute immediate 'alter index '||i.owner||'.'||i.index_name||' rebuild';
        end if;
    end loop;

    p$stat(in$program_action, v$ownname, v$tabname);


    exception
    when UNEXIST_TABLE then util.pkg$log.p$add_log(in$program_action, 'Указанная таблица '||v$ownname||'.'||v$tabname||' не существует', in$level => 'warning'); util.pkg$log.p$add_trace(in$program_action);
    when others then util.pkg$log.p$add_trace(in$program_action); raise;

end p$defragmentation;

end pkg$ddl;
/

﻿CREATE OR REPLACE package UTIL.pkg$log as

function f$get_param(
    in$param_name in varchar2                 -- наименование параметра
)
return varchar2;

procedure p$start_log(
    in$program_action in varchar2              -- Наименование вызывающей процедуры
  , in$executor in varchar2                    -- Ответственный за процедуру
  , in$coexecutor in varchar2                  -- Заменяющий ответственного за процедуру
);

procedure p$add_log(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in clob default '<hr>'          -- Опционально : html-сообщение
  , in$level in varchar2 default 'notice'      -- Опционально : Уровень детализации лога (error / warning / notice / info / debug)
  , in$date_flag in varchar2 default 'date'    -- Опционально : Добавлять ли дату в начале строки (nodate - не добавлять, date - добавлять)
  , in$eol_flag in varchar2 default 'eol'      -- Опционально : Добавлять ли окончание строки (EndOfLine = <br>) в конце сообщения (noeol - не добавлять, eol - добавлять)
  , in$tech_action in varchar2 default 'UTIL.PKG$LOG.P$ADD_LOG'    -- Опционально : Название процедуры, сформировавшей сообщение.
                                                                   -- Кроме вызывающей процедуры это могут быть технические пакеты (статусы, DDL, LOG и пр.)
);

procedure p$add_log_dblink(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in varchar2 default '<hr>'      -- Опционально : html-сообщение
  , in$level in varchar2 default 'notice'      -- Опционально : Уровень детализации лога (error / warning / notice / info / debug)
  , in$date_flag in varchar2 default 'date'    -- Опционально : Добавлять ли дату в начале строки (nodate - не добавлять, date - добавлять)
  , in$eol_flag in varchar2 default 'eol'      -- Опционально : Добавлять ли окончание строки (EndOfLine = <br>) в конце сообщения (noeol - не добавлять, eol - добавлять)
  , in$tech_action in varchar2 default 'UTIL.PKG$LOG.P$ADD_LOG_DBLINK' -- Опционально : Название процедуры, сформировавшей сообщение.
                                                                   -- Кроме вызывающей процедуры это могут быть технические пакеты (статусы, DDL, LOG и пр.)
);

procedure p$end_log(
    in$program_action in varchar2              -- Наименование вызывающей процедуры
  , in$periodicity in varchar2 default 'D'     -- Опционально : периодичность запуска: D (daily) / W (weekly) / M (monthly)
  , in$priority in number default 0            -- Опционально : Приоритет письма (0,1)
);

procedure p$add_xplan(
    in$program_action in varchar2              -- Имя вызывающей процедуры
);

procedure p$add_todo(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in varchar2                     -- Сообщение о необходимости доработки
  , in$executor in varchar2 default 'ОИАО'     -- Ответственный за процедуру
);

procedure p$add_table(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$sql_string in varchar2                  -- Запрос типа 'select idcardtype, label from rf.prod_card'
  , in$header in varchar2                      -- Заголовок таблицы (<tr><th>Заголовок1</th><th>Заголовок2</th></tr>)
  , in$caption in varchar2 default null        -- Необязательный параметр: название таблицы
                                               -- <caption>Название таблицы</caption>
  , in$level in varchar2 default 'notice'      -- Уровень лога в случае наличия данных
);

procedure p$add_trace(
    in$program_action in varchar2              -- Имя вызывающей процедуры
);

procedure p$archive_log;

procedure p$test_mode(
    in$program_action in varchar2               -- Имя вызывающей процедуры
  , in$recipients in varchar2                   -- Перечень получателей тестовых логов
);

end;
/


CREATE OR REPLACE package body UTIL.pkg$log as

/*******************************************************************************
* CONST
*******************************************************************************/

-- Дефолтный program_action
c$default_action  varchar2(100) := 'UTIL.COMMON_LOGS';

-- Наименование большой ежемесячной цепочки
c$chain         varchar2(100) := 'BEZ_CHAIN';

/*******************************************************************************
* PRIVATE
*******************************************************************************/

function f$get_param(
    in$param_name in varchar2                 -- наименование параметра
)
return varchar2
is
    var$ret util.log$config.value%type;
begin
    var$ret := '';

    if (in$param_name in ('admin_mail','default_recipients')) then
        for cur in (
            select u.email
            from util.dwuser u
            inner join util.dwuser_division d using(id_dwuser_division)
            where d.division_name = 'Отдел информационно-аналитического обеспечения'
              and u.dt_dismiss is null
        ) loop
            var$ret := var$ret || cur.email || ';';
        end loop;
    else
        select value
        into var$ret
        from util.log$config c
        where c.name = in$param_name;
    end if;

    return var$ret;
exception when others then
    return null;
end;

procedure p$send_admin_log(
    in$message in clob                 -- сообщение к отправке
)
is
    var$mail util.log$config.value%type;
begin
    var$mail := f$get_param('admin_mail');

    util.pkg$mail.p$send(var$mail,'UTIL.PKG$LOG [Error]',in$message,1);
exception when others then
    null;
end;

procedure p$add_message(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in clob default '<hr>'          -- Опционально : html-сообщение
  , in$level in varchar2 default 'notice'      -- Опционально : Уровень детализации лога (error / warning / notice / info / debug)
  , in$date_flag in varchar2 default 'date'    -- Опционально : Добавлять ли дату в начале строки (nodate - не добавлять, date - добавлять)
  , in$eol_flag in varchar2 default 'eol'      -- Опционально : Добавлять ли окончание строки (EndOfLine = <br>) в конце сообщения (noeol - не добавлять, eol - добавлять)
  , in$tech_action in varchar2 default 'UTIL.PKG$LOG.P$ADD_LOG' -- Опционально : Название процедуры, сформировавшей сообщение.
                                                                -- Кроме вызывающей процедуры это могут быть технические пакеты (статусы, DDL, LOG и пр.)
) is
    c$amount            integer := 3999;

    var$cpos            integer := 1;
    var$amount          integer := c$amount;
    var$lag             integer := 0;

    var$level           varchar2(100);
    var$severity        varchar2(20);
    var$send_mail       varchar2(1);
    var$level_num       number(1);

    var$log_id          number;
    var$message         clob;
    var$date_flag       varchar2(100);
    var$eol_flag        varchar2(100);
    var$program_action  util.log$seq.program_action%type;
    var$recipients      util.log$config.value%type;

    var$log_seq         number;

    program_action_seq_not_found exception;
begin
    var$program_action := nvl(in$program_action,c$default_action);

    if (in$message is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Не указан параметр in$message (текущее значение in$message = null)'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$ADD_MESSAGE'
        );

        var$message := '<hr>';
    else
        var$message := in$message;
    end if;

    begin
        select keyword, severity, send_mail, lvl
        into var$level, var$severity, var$send_mail, var$level_num
        from util.log$level
        where keyword = lower(in$level);
    exception when others then
        pkg$log.p$add_message(
            var$program_action
          , 'Параметр in$level не найден в таблице UTIL.log$LEVEL.KEYWORD (текущее значение in$level = '||nvl(in$level,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$ADD_MESSAGE'
        );

        select keyword, severity, send_mail, lvl
        into var$level, var$severity, var$send_mail, var$level_num
        from util.log$level
        where keyword = 'notice';
    end;

    if (lower(in$date_flag) not in ('date','nodate') or in$date_flag is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Параметр in$date_flag может принимать значения только date, nodate (текущее значение in$date_flag = '||nvl(in$date_flag,'null')
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$ADD_MESSAGE'
        );

        var$date_flag := 'date';
    else
        var$date_flag := lower(in$date_flag);
    end if;

    if (in$message like '<h_>%') then
        var$date_flag := 'nodate';
    end if;

    if (lower(in$eol_flag) not in ('eol','noeol') or in$eol_flag is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Параметр in$eol_flag может принимать значения только eol, noeol (текущее значение in$eol_flag = '||nvl(in$eol_flag,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$ADD_MESSAGE'
        );

        var$eol_flag := 'eol';
    else
        var$eol_flag := lower(in$eol_flag);
    end if;

    -- Проверяем, что p$start_log был вызван
    begin
        select log_id
        into var$log_id
        from util.log$seq
        where program_action = upper(var$program_action);
    exception when others then
        select util.log_seq.nextval
        into var$log_seq
        from dual;

        -- Обновляем log_id у процедуры
        merge into util.log$seq l
        using (
            select upper(var$program_action) program_action, var$log_seq log_id
            from dual
        ) s on (s.program_action = l.program_action)
        when matched then
            update set l.log_id = s.log_id
        when not matched then
            insert (l.program_action, l.log_id)
            values (s.program_action, s.log_id);

        select log_id
        into var$log_id
        from util.log$seq
        where program_action = upper(var$program_action);
    end;

    -- Добавляем дату и признак конца строки
    var$message := case when var$date_flag = 'date' and var$message != '<hr>' then '<b>['||to_char(sysdate,'hh24:mi')||']</b> ' end
                || case when var$level = 'error' then '<span class="log_level_error">'
                        when var$level = 'warning' then '<span class="log_level_warning">'
                        --when in$level = 'notice' then '<span class="log_level_notice">'
                        when var$level = 'info' then '<span class="log_level_info">'
                        when var$level = 'debug' then '<span class="log_level_debug">'
                   end
                || in$message
                || case when var$level != 'notice' then '</span>' end
                || case when var$eol_flag = 'eol' and var$message not like '<h_>%' and var$message not like '%</ul>%' and var$message not like '%</li>%' then '<br/>' end;

    -- Режем клоб по 4000 символов
    while var$cpos < dbms_lob.getlength(var$message)
    loop
        var$lag := instr(dbms_lob.substr(var$message, c$amount, var$cpos),'>',-1);

        if (var$lag = 0) then
            var$amount := c$amount;
        else
            var$amount := var$lag;
        end if;

        insert into util.log$messages(
            id, log_id, program_action, tech_action, message_dt, message_level, message_text
        )
        values (
            util.log_message_seq.nextval
          , var$log_id
          , upper(var$program_action)
          , upper(in$tech_action)
          , sysdate
          , var$level_num
          , dbms_lob.substr(var$message, var$amount, var$cpos)
        );

        var$cpos := var$cpos + var$amount;
    end loop;

    commit;
exception
    when program_action_seq_not_found then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$ADD_LOG вызвала ошибку : Процедура '||var$program_action||' не найдена в UTIL.log$SEQ, нужно сначала вызвать P$START_LOG ( in$program_action = '||var$program_action||')');
        rollback;
    when others then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$ADD_LOG вызвала ошибку :'||sqlerrm||' >> '||dbms_utility.format_error_backtrace||'( in$program_action = '||var$program_action||')');
        rollback;
end;

procedure p$send_log(
    in$program_action in varchar2                    -- Имя вызывающей процедуры, лог которой нужно отослать
  , in$subject in varchar2                           -- Тема письма
  , in$level in varchar default 'notice'             -- Опционально : в письмо включаются логи уровня <= in$level
  , in$priority in number default 0                  -- Опционально : Приоритет письма (0,1)
  , in$recipients in varchar2 default null           -- Опционально : Перечень адресатов.
                                                     -- Если параметр указан, то вместо обычных адресатов письмо отправляется только по адресам in$recipients
)
is
    var$program_action          util.log$seq.program_action%type;

    var$subject                 varchar2(1000);
    var$message                 clob;
    var$recipients              util.log$config.value%type;
    var$level                   util.log$level.keyword%type;
    var$priority                number;
    var$max_mail_size           number;

    var$test_mode_flag          number;

    var$warning_count           number;
    var$error_count             number;
    var$warning_count_real      number;
    var$error_count_real        number;

    var$cnt                     number;
begin
    var$program_action :=nvl(in$program_action,c$default_action);

    if (in$subject is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Должен быть указан параметр in$subject (текущее значение in$subject = null)'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$SEND_LOG'
        );

        var$subject := 'Нет темы письма';
    else
        var$subject := in$subject;
    end if;

    begin
        select keyword
        into var$level
        from util.log$level
        where keyword = lower(in$level);
    exception when no_data_found then
        pkg$log.p$add_message(
            var$program_action
          , 'Некорректный параметр in$level (текущее значение in$level = '||nvl(in$level,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$SEND_LOG'
        );

        var$level := 'notice';
    end;

    if (in$priority is null or in$priority not in (0,1)) then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.SEND_LOG вызвала ошибку : Возможные значения параметра in$priority in (0,1) (текущее значение in$priority = '||nvl(in$priority,'null')||')! ( in$program_action = '||var$program_action||')');
        var$priority := 0;
    else
        var$priority := in$priority;
    end if;

    begin
        select recipients, 1
        into var$recipients, var$test_mode_flag
        from util.log$test_mode
        where program_action = upper(var$program_action);

        if not regexp_like(var$recipients, '[a-zA-Z0-9._%-]+@rsb.ru') then
            pkg$log.p$send_admin_log(
                'Неверный формат получателя тестового лога «'||var$recipients||'». Введите адрес в формате «surname@rsb.ru;». Список адресатов тестового лога заменен на получателей по умолчанию. (in$program_action = '||var$program_action||')'
            );
            var$test_mode_flag := 1;
            var$recipients := f$get_param('default_recipients');
        end if;
    exception when others then
        var$test_mode_flag := 0;
    end;

    if (var$test_mode_flag = 0) then
        if (in$recipients is null) then
            begin
                select comments
                into var$recipients
                from dba_scheduler_programs
                where owner = 'OLEG'
                  and program_action = upper(var$program_action);
            exception when no_data_found then
                pkg$log.p$add_message(
                    var$program_action
                  , 'Процедура '||var$program_action||' не найдена в DBA_SCHEDULER_PROGRAMS'
                  , 'warning', 'date','eol'
                  , 'UTIL.PKG$LOG.P$SEND_LOG'
                );

                pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.SEND_LOG вызвала ошибку : Процедура не найдена в DBA_SCHEDULER_PROGRAMS ( in$program_action = '||var$program_action||')');
            end;

            if (var$recipients is null) then
                var$recipients := f$get_param('default_recipients');
            end if;
        else
            var$recipients := in$recipients;
        end if;
    end if;

    var$message := f$get_param('css');

    var$message := var$message || '<body>'||chr(10)||chr(13);

    -- Записываем шапку письма
    for cur in (
        select m.*
        from util.log$messages m
        where log_id = (
            select log_id
            from util.log$seq
            where program_action = upper(var$program_action)
        )
        and message_level <= ( select lvl from util.log$level where keyword = nvl(var$level,'notice') )
        and tech_action = 'UTIL.PKG$LOG.P$START_LOG'
        order by id
    ) loop
        var$message := var$message || cur.message_text;
    end loop;

    -- Количество ошибок - исключаем add_table
    select count(*)
    into var$error_count
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and message_level = (select lvl from util.log$level where keyword = 'error')
      and tech_action != 'UTIL.PKG$LOG.P$ADD_TABLE';

    -- Количество ошибок - реальное
    select count(*)
    into var$error_count_real
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and message_level = (select lvl from util.log$level where keyword = 'error');

    -- Количество предупреждений - исключаем add_table
    select count(*)
    into var$warning_count
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and message_level = (select lvl from util.log$level where keyword = 'warning')
      and tech_action != 'UTIL.PKG$LOG.P$ADD_TABLE';

    -- Количество предупреждений - реальное
    select count(*)
    into var$warning_count_real
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and message_level = (select lvl from util.log$level where keyword = 'warning');

    if (var$test_mode_flag = 1) then
        var$message := var$message ||chr(10)||chr(13)|| '<span class="test_text">Процедура запущена в тестовом режиме</span><br>'||chr(10)||chr(13);
    end if;

    if (var$error_count > 0) then
        var$message := var$message || 'Количество ошибок : '
                    || case when var$error_count = 0 then '<span class="green_btext">' else '<span class="red_btext">' end
                    || var$error_count||'</span><br>'||chr(10)||chr(13);
    end if;

    if (var$warning_count > 0) then
        var$message := var$message || 'Количество предупреждений : '
                    || case when var$warning_count = 0 then '<span class="green_btext">' else '<span class="red_btext">' end
                    || var$warning_count||'</span><br>'||chr(10)||chr(13);
    end if;

    select count(*)
    into var$cnt
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and tech_action in ('UTIL.PKG$STATUS_TECH.P$SET','UTIL.PKG$STATUS_TECH.P$SET$DATE','UTIL.PKG$STATUS_TECH.P$SET$NUMBER');

    if (var$cnt > 0) then
        var$message := var$message || 'Проставлены следующие технические даты:'||chr(10)||chr(13);
        var$message := var$message || '<ul>';
    end if;

    for cur in (
        select *
        from util.log$messages
        where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
          and tech_action in ('UTIL.PKG$STATUS_TECH.P$SET','UTIL.PKG$STATUS_TECH.P$SET$DATE','UTIL.PKG$STATUS_TECH.P$SET$NUMBER')
    )
    loop
        var$message := replace(var$message || '<li>' || cur.message_text || '</li>'||chr(10)||chr(13),'<br/></li>','</li>');
    end loop;

    if (var$cnt > 0) then
        var$message := var$message || '</ul>';
    end if;

    select count(*)
    into var$cnt
    from util.log$messages
    where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
      and tech_action = 'UTIL.PKG$STATUS_BUSINESS.P$SET';

    if (var$cnt > 0) then
        var$message := var$message || 'Проставлены следующие бизнес даты:<br>'||chr(10)||chr(13);
        var$message := var$message || '<ul>';
    end if;

    for cur in (
        select *
        from util.log$messages
        where log_id = ( select log_id from util.log$seq where program_action = upper(var$program_action) )
          and tech_action = 'UTIL.PKG$STATUS_BUSINESS.P$SET'
    )
    loop
        var$message := replace(var$message || '<li>' || cur.message_text || '</li>'||chr(10)||chr(13),'<br/></li>','</li>');
    end loop;

    if (var$cnt > 0) then
        var$message := var$message || '</ul>';
    end if;

    var$message := var$message || '<hr>';

    var$max_mail_size := to_number(f$get_param('max_mail_size'));

    -- Дописываем лог (все что дальше после шапки)
    for cur in (
            select m.*
            from util.log$messages m
            where log_id = (
                select log_id
                from util.log$seq
                where program_action = upper(var$program_action)
            )
            and message_level <= ( select lvl from util.log$level where keyword = nvl(var$level,'notice') )
            and tech_action != 'UTIL.PKG$LOG.P$START_LOG'
            order by id
    ) loop
        var$message := var$message
                    || replace(replace(cur.message_text,'</TR>','</TR>'||chr(10)||chr(13)),'</tr>','</tr>'||chr(10)||chr(13))
                    || chr(10)||chr(13);
        if (dbms_lob.getlength(var$message) > var$max_mail_size) then
            var$message := var$message || '<span class="red_btext">Превышен максимально допустимый размер письма (UTIL.log$config.max_mail_size), лог может быть просмотрен только через web-интерфейс!</span>';
            exit;
        end if;
    end loop;

    var$message := var$message || '</body>';

    if (var$error_count_real > 0 or var$warning_count_real > 0) then
        var$priority := 1;
    end if;

    select count(*)
    into var$cnt
    from util.log$messages
    where (tech_action = 'UTIL.PKG$LOG.P$END_LOG' or message_level < 0)
       and message_dt >= sysdate - 5/24/60 -- 5 минут
       and program_action = var$program_action;

    if (var$cnt < 50) then
        util.pkg$mail.p$send(var$recipients,var$subject,var$message,var$priority);
    end if;

    commit;
exception
    when others then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.SEND_LOG вызвала ошибку :'||sqlerrm||' >> '||dbms_utility.format_error_backtrace||' ( in$program_action = '||var$program_action||')');
        rollback;
end;

/*******************************************************************************
* PUBLIC
*******************************************************************************/

procedure p$start_log(
    in$program_action in varchar2              -- Наименование вызывающей процедуры
  , in$executor in varchar2                    -- Ответственный за процедуру
  , in$coexecutor in varchar2                  -- Заменяющий ответственного за процедуру
)
is
    pragma              autonomous_transaction;

    var$log_seq         util.log$seq.log_id%type;
    var$program_action  util.log$seq.program_action%type;
begin
    var$program_action := nvl(in$program_action,c$default_action);

    if (in$executor is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Не указан обязательный параметр in$executor (текущее значение in$executor = null) !'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$START_LOG'
        );
    end if;

    if (in$coexecutor is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Не указан обязательный параметр in$coexecutor (текущее значение in$coexecutor = null) !'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$START_LOG'
        );
    end if;

    -- Достаем новое значение идентификатора лога
    select util.log_seq.nextval
    into var$log_seq
    from dual;

    -- Обновляем log_id у процедуры
    merge into util.log$seq l
    using (
        select upper(var$program_action) program_action, var$log_seq log_id
        from dual
    ) s on (s.program_action = l.program_action)
    when matched then
        update set l.log_id = s.log_id
    when not matched then
        insert (l.program_action, l.log_id)
        values (s.program_action, s.log_id);

    commit;

    pkg$log.p$add_message(var$program_action,in$tech_action => 'UTIL.PKG$LOG.P$START_LOG');
    pkg$log.p$add_message(
        var$program_action
      , '<b>['||to_char(sysdate,'dd.mm.yyyy hh24:mi:ss')||']</b> Запущена процедура <a href="'
             ||pkg$log.f$get_param('html_show_log')||'?in$program_action='||upper(var$program_action)||'&in$log_level=2&in$log_id='||var$log_seq||'">'
             ||upper(var$program_action)||'</a><br>'||chr(10)||chr(13)
      ||'Ответственный : <a href="mailto:'||in$executor||'">'||in$executor||'</a><br>'||chr(10)||chr(13)
      ||'Заместитель : <a href="mailto:'||in$coexecutor||'">'||in$coexecutor||'</a>'||chr(10)||chr(13)
      , 'notice','nodate','eol','UTIL.PKG$LOG.P$START_LOG'
    );
    pkg$log.p$add_message(var$program_action,in$tech_action => 'UTIL.PKG$LOG.P$START_LOG');

    commit;
exception
    when others then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$START_LOG вызвала ошибку : '||sqlerrm||' >> '||dbms_utility.format_error_backtrace||' ( in$program_action = '||var$program_action||')');
        rollback;
end;



procedure p$add_log(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in clob default '<hr>'          -- Опционально : html-сообщение
  , in$level in varchar2 default 'notice'      -- Опционально : Уровень детализации лога (error / warning / notice / info / debug)
  , in$date_flag in varchar2 default 'date'    -- Опционально : Добавлять ли дату в начале строки (nodate - не добавлять, date - добавлять)
  , in$eol_flag in varchar2 default 'eol'      -- Опционально : Добавлять ли окончание строки (EndOfLine = <br>) в конце сообщения (noeol - не добавлять, eol - добавлять)
  , in$tech_action in varchar2 default 'UTIL.PKG$LOG.P$ADD_LOG' -- Опционально : Название процедуры, сформировавшей сообщение.
                                                                -- Кроме вызывающей процедуры это могут быть технические пакеты (статусы, DDL, LOG и пр.)
)
is
    pragma          autonomous_transaction;

    var$recipients      util.log$config.value%type;
    var$program_action  util.log$seq.program_action%type;
begin
    p$add_message(in$program_action, in$message, in$level, in$date_flag, in$eol_flag, in$tech_action);

    if (lower(in$level) = 'error') then
        var$program_action := nvl(in$program_action,c$default_action);

        pkg$log.p$send_log(
            var$program_action
          , ''||upper(var$program_action)||' [Error]' -- in$subject
          , 'debug' -- in$level
          , 1 -- in$priority
        );
    end if;
end;

procedure p$add_log_dblink(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in varchar2 default '<hr>'      -- Опционально : html-сообщение
  , in$level in varchar2 default 'notice'      -- Опционально : Уровень детализации лога (error / warning / notice / info / debug)
  , in$date_flag in varchar2 default 'date'    -- Опционально : Добавлять ли дату в начале строки (nodate - не добавлять, date - добавлять)
  , in$eol_flag in varchar2 default 'eol'      -- Опционально : Добавлять ли окончание строки (EndOfLine = <br>) в конце сообщения (noeol - не добавлять, eol - добавлять)
  , in$tech_action in varchar2 default 'UTIL.PKG$LOG.P$ADD_LOG_DBLINK' -- Опционально : Название процедуры, сформировавшей сообщение.
                                                                       -- Кроме вызывающей процедуры это могут быть технические пакеты (статусы, DDL, LOG и пр.)
)
is
begin
    util.pkg$log.p$add_log(in$program_action, in$message, in$level, in$date_flag, in$eol_flag, in$tech_action);
end;

procedure p$end_log(
    in$program_action in varchar2              -- Наименование вызывающей процедуры
  , in$periodicity in varchar2 default 'D'     -- Опционально : периодичность запуска: D (daily) / W (weekly) / M (monthly)
  , in$priority in number default 0            -- Опционально : Приоритет письма (0,1)
)
is
    pragma                  autonomous_transaction;

    var$program_action      util.log$seq.program_action%type;
    var$periodicity         varchar2(10);
    var$priority            number;

    var$start_dt            date;

    var$chain_name          varchar2(100);
    var$step_name           varchar2(100);
begin
    var$program_action := nvl(in$program_action,c$default_action);

    if (in$periodicity is null or lower(in$periodicity) not in ('d','w','m','t')) then
        pkg$log.p$add_message(
            var$program_action
          , 'Допустимые значение in$periodicity in (D,W,M,T) (текущее значение in$periodicity = '||nvl(in$periodicity,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$END_LOG'
        );

        var$periodicity := 'UNKNOWN';
    else
        var$periodicity := in$periodicity;
    end if;

    if (in$priority not in (0,1) or in$priority is null) then
        pkg$log.p$add_message(
            var$program_action
          , 'Допустимые значение in$priority in (0,1) (текущее значение in$priority = '||nvl(in$priority,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$END_LOG'
        );

        var$priority := 0;
    else
        var$priority := in$priority;
    end if;

    -- Пишем футер письма
    pkg$log.p$add_message(var$program_action,in$tech_action => 'UTIL.PKG$LOG.P$END_LOG');
    pkg$log.p$add_message(var$program_action,
          '<b>['||to_char(sysdate,'dd.mm.yyyy hh24:mi:ss')||']</b> Завершена '
        ||case when upper(var$periodicity) = 'D' then 'ежедневная процедура '
               when upper(var$periodicity) = 'W' then 'еженедельная процедура '
               when upper(var$periodicity) = 'M' then 'ежемесячная процедура '
               when upper(var$periodicity) = 'T' then 'работа триггера '
               else var$periodicity
          end
        ||'<b>'||upper(var$program_action)||'</b>'
        , 'notice', 'nodate', 'eol', 'UTIL.PKG$LOG.P$END_LOG');
    pkg$log.p$add_message(var$program_action,in$tech_action => 'UTIL.PKG$LOG.P$END_LOG');

    -- Достаем дату начала работы джоба
    select min(message_dt)
    into var$start_dt
    from util.log$messages
    where log_id = (
        select log_id
        from util.log$seq
        where program_action = upper(var$program_action)
    );

    -- Отсылаем лог
    pkg$log.p$send_log(
            var$program_action
          , ''||upper(in$program_action)||' ['
         || case when upper(var$periodicity) = 'D' then 'Daily'
                 when upper(var$periodicity) = 'W' then 'Weekly'
                 when upper(var$periodicity) = 'M' then 'Monthly'
                 when upper(var$periodicity) = 'T' then 'Trigger'
                 else var$periodicity
            end
         ||'] Start : '||to_char(var$start_dt,'dd.mm.yyyy hh24:mi:ss')
          , 'notice' -- in$level
          , var$priority
    );

    delete from util.log$test_mode where program_action = upper(var$program_action);

    begin
        select cs.owner ||'.'|| cs.chain_name
             , cs.step_name
        into var$chain_name
           , var$step_name
        from dba_scheduler_chain_steps cs
        left join dba_scheduler_programs sp on (cs.program_owner = sp.owner and cs.program_name = sp.program_name)
        where sp.program_action = var$program_action
          and cs.chain_name = c$chain;

        dbms_scheduler.alter_chain (var$chain_name,var$step_name,'SKIP',true);
    exception when no_data_found then null;
    end;

    commit;
exception
    when others then
        pkg$log.p$send_admin_log('Процедуры UTIL.PKG$LOG.P$END_LOG вызвала ошибку : '||sqlerrm||' >> '||dbms_utility.format_error_backtrace||' ( in$program_action = '||var$program_action||')');
        rollback;
end;

procedure p$add_xplan(
    in$program_action in varchar2              -- Имя вызывающей процедуры
)
is
    v$sql_id            varchar2(100);
    v$sql_child_number  number;
begin
    begin
        select distinct
               first_value(hist.sql_id) over (order by hist.sample_id desc) sql_id
             , first_value(hist.sql_child_number) over (order by hist.sample_id desc) sql_child_number
        into v$sql_id
           , v$sql_child_number
        from v$active_session_history hist
           , v$sqlarea sqla
           , v$session ss
        where sqla.sql_id = hist.sql_id
          and ss.sid = hist.session_id
          and ss.serial# = hist.session_serial#
          and ss.audsid = sys_context('USERENV', 'SESSIONID')
          and sqla.parsing_schema_name != 'SYS'
          and upper(sqla.sql_text) not like '%V$ACTIVE_SESSION_HISTORY%';

        for cur in (
            select replace(plan_table_output,' ','&nbsp;') plan_table_output
            from table(
                dbms_xplan.display_cursor(v$sql_id,v$sql_child_number,'ALL ALLSTATS LAST')
            )
        ) loop
            pkg$log.p$add_message(in$program_action
                , '<span class="log_xplan">'||cur.plan_table_output||'</span>'
                ,'debug','nodate','eol'
                ,'UTIL.PKG$LOG.P$ADD_XPLAN'
            );
        end loop;
    exception when no_data_found then
        util.pkg$log.p$add_log(in$program_action,'Процедуре UTIL.PKG$LOG.P$ADD_XPLAN не удалось определить sql_id последнего выполненого запроса!','warning');
        return;
    end;
exception when others then
    pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$ADD_XPLAN вызвала исключение :'||sqlerrm||' >> ' || dbms_utility.format_error_backtrace||' ( in$program_action = '||in$program_action||')');
end;

procedure p$add_todo(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$message in varchar2                     -- Сообщение о необходимости доработки
  , in$executor in varchar2 default 'ОИАО'     -- Ответственный за процедуру
)
is
    pragma  autonomous_transaction;
begin
    p$add_message(in$program_action,'<hr>', in$tech_action => 'UTIL.PKG$LOG.P$ADD_TODO');
    p$add_message(in$program_action,'<span class = "log_todo">TODO : '
        ||nvl(in$message,'Не указано сообщение')||'<br>'
        ||'Ответственный : '||nvl(in$executor,'Не указан ответственный')
        ||'</span>'
      , 'notice'
      , 'nodate'
      , 'eol'
      , 'UTIL.PKG$LOG.P$ADD_TODO'
    );
    p$add_message(in$program_action,'<hr>', in$tech_action => 'UTIL.PKG$LOG.P$ADD_TODO');
exception when others then
    pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$ADD_TODO вызвала исключение :'||sqlerrm||' >> ' || dbms_utility.format_error_backtrace||' ( in$program_action = '||in$program_action||')');
end;

procedure p$add_table(
    in$program_action in varchar2              -- Имя вызывающей процедуры
  , in$sql_string in varchar2                  -- Запрос типа 'select idcardtype, label from rf.prod_card'
  , in$header in varchar2                      -- Заголовок таблицы (<tr><th>Заголовок1</th><th>Заголовок2</th></tr>)
  , in$caption in varchar2 default null        -- Необязательный параметр: название таблицы
                                               -- <caption>Название таблицы</caption>
  , in$level in varchar2 default 'notice'      -- Уровень лога в случае наличия данных
)
is
   pragma               autonomous_transaction;

   var$level            varchar2(100);

   cur                  pls_integer := dbms_sql.open_cursor;
   cols                 dbms_sql.desc_tab;
   ncols                pls_integer;
   col_names            varchar2(300);
   response_buffer      clob;
   xmldata              xmltype;
   xsldata              xmltype;
   html                 xmltype;
   html_clob            clob;
   xsl                  clob;
   xsl_th               clob;
   xsl_td               clob;
   sql_str              varchar2(6000);

   var$program_action   varchar2(100);
begin
    var$program_action := nvl(in$program_action,c$default_action);

    begin
        select keyword
        into var$level
        from util.log$level
        where keyword = lower(in$level);
    exception when no_data_found then
        pkg$log.p$add_message(
            var$program_action
          , 'Некорректный параметр in$level (текущее значение in$level = '||nvl(in$level,'null')||')'
          , 'warning', 'date','eol'
          , 'UTIL.PKG$LOG.P$ADD_LOG'
        );

        var$level := 'notice';
    end;

    sql_str := in$sql_string;

    dbms_sql.parse (cur, sql_str, dbms_sql.native);
    dbms_sql.describe_columns (cur, ncols, cols);

    xsl_th := '';
    xsl_td := '';
    for colind in 1 .. ncols
    loop
        if colind = 1 then
            col_names := cols(colind).col_name;
        else
            col_names := col_names || ', ' || cols(colind).col_name;
        end if;
        xsl_th := xsl_th || '<TH>'||cols(colind).col_name||'</TH>';
        xsl_td := xsl_td || '<TD align = "right"> <xsl:value-of select="'||cols(colind).col_name||'"/></TD>';
    end loop;

    dbms_sql.close_cursor (cur);
    if in$header is not null then
        xsl_th := in$header;
    else
        xsl_th := '<TR>' ||xsl_th|| '</TR>';
    end if;

    xsl := '<?xml version="1.0" encoding="Windows-1251"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" encoding="Windows-1251" />
<xsl:template match="/"><TABLE>'
||case when in$caption is not null then ' <caption>'||in$caption||'</caption>' end
||'<THEAD><tr style="background:#DCDCDC; display:none;"><td colspan="'||ncols
||'"></td></tr>'
||case when var$level in ('error','warning')
       then replace(xsl_th,'<th>','<th class="th_red">')
       when var$level in ('info','debug')
       then replace(xsl_th,'<th>','<th class="th_gray">')
       else xsl_th
  end
||'</THEAD>'
||'<TBODY>'||
'<xsl:for-each select="//row">' ||
  '<xsl:choose>'||
     '<xsl:when test="position() mod 2 = 1">'||
       '<TR class="odd">'||xsl_td||'</TR>'||
     '</xsl:when>'||
     '<xsl:otherwise>'||
       '<TR class="even">'||xsl_td||'</TR>'||
     '</xsl:otherwise>'||
  '</xsl:choose>' ||
'</xsl:for-each>'||
'</TBODY>'||
'</TABLE>'||
'</xsl:template>'||
'</xsl:stylesheet>';

    xsldata := xmltype.createxml(xsl);

--Т.к. не всегда работает TO_DATE, то при вызове к дате TO_CHAR она будет изменена в соответствии с текущим форматом даты для сессии
--Именно для этого следующая строка:
    execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = "DD.MM.YYYY"';
    execute immediate 'ALTER SESSION set NLS_NUMERIC_CHARACTERS = ". "'; --Замена запятой на точку

    begin
        execute immediate 'SELECT XMLAGG(XMLELEMENT("row",XMLFOREST('||col_names ||'))) FROM('||sql_str||')' into xmldata;

        html := xmldata.transform(xsldata);

        html_clob := replace(replace(replace(replace(html.getclobval(),'&lt;','<'),'&gt;','>'),'&quot;','"'),'&apos;','''');

        pkg$log.p$add_message (var$program_action,'<br>'||html_clob,var$level,'nodate','eol','UTIL.PKG$LOG.P$ADD_TABLE');
    exception when others then
        if (sqlcode = -30625) then
            pkg$log.p$add_message(
                var$program_action
              , '<br><b class="blue_btext">'||in$caption||' (Нет данных для вывода в таблицу)</b>'
              , case when var$level in ('warning','error') then 'notice' else var$level end
              , 'nodate','eol','UTIL.PKG$LOG.P$ADD_TABLE');
        else
            pkg$log.p$add_message(var$program_action,'UTIL.PKG$LOG.P$ADD_TABLE вызвала ошибку : '||sqlerrm ||' >> ' || dbms_utility.format_error_backtrace,'error','nodate','eol','UTIL.PKG$LOG.P$ADD_TABLE');
        end if;
    end;

exception when others then
    pkg$log.p$add_message(var$program_action,'UTIL.PKG$LOG.P$ADD_TABLE вызвала ошибку : '||sqlerrm ||' >> ' || dbms_utility.format_error_backtrace,'error','nodate','eol','UTIL.PKG$LOG.P$ADD_TABLE');
end;

procedure p$add_trace(
    in$program_action in varchar2              -- Имя вызывающей процедуры
)
is
    pragma              autonomous_transaction;

    var$program_action  util.log$seq.program_action%type;
    var$recipients      util.log$config.value%type;
begin
    var$program_action := nvl(in$program_action,c$default_action);

    pkg$log.p$add_message(var$program_action,sqlerrm,'error');
    pkg$log.p$add_message(var$program_action);
    pkg$log.p$add_message(var$program_action,'BackTrace :<br>'||replace(dbms_utility.format_error_backtrace,chr(10),'<br>'),'error');

    if (instr(dbms_utility.format_error_stack,'ORA-01652') > 0) then -- unable to extend temp segment
        pkg$log.p$add_table(
            var$program_action
          , 'select osuser, username, sid, temp_size_gb, segtype, program, sql_text from util.v$log$temp_usage'
          , '<tr><th>osuser</th><th>username</th><th>sid</th><th>temp_size_gb</th><th>segtype</th><th>program</th><th>sql_text</th></tr>'
          , 'Топ 5 сессий, занимающих TEMP'
        );
    elsif (instr(dbms_utility.format_error_stack,'ORA-01555') > 0) then -- snapshot too old
        pkg$log.p$add_table(
            var$program_action
          , 'select username, osuser, sid, serial, start_time, status from util.v$log$rollstat'
          , '<tr><th>username</th><th>osuser</th><th>sid</th><th>serial</th><th>start_time</th><th>status</th></tr>'
          , 'Список блокировок сегментов отката'
        );
    elsif (instr(dbms_utility.format_error_stack,'ORA-00054') > 0) then --  resource is busy
        pkg$log.p$add_table(
            var$program_action
          , 'select os_user_name, oracle_username, sid, lock_type, lock_held, lock_requested, blocking, owner, object_name, sql_id from util.v$log$locks'
          , '<tr><th>os_user_name</th><th>oracle_username</th><th>sid</th><th>lock_type</th><th>lock_held</th><th>lock_requested</th><th>blocking</th><th>owner</th><th>object_name</th><th>sql_id</th></tr>'
          , 'Список блокировок'
        );
    end if;

    pkg$log.p$add_message(var$program_action);

    var$recipients := f$get_param('default_recipients');

    pkg$log.p$send_log(
        var$program_action
      , ''||upper(var$program_action)||' [Error]' -- in$subject
      , 'debug' -- in$level
      , 1 -- in$priority
      , var$recipients
    );
exception when others then
   pkg$log.p$send_admin_log('Процедуры UTIL.PKG$LOG.P$ADD_TRACE вызвала исключение : '||sqlerrm ||' >> ' || dbms_utility.format_error_backtrace ||' >> program_action ='||var$program_action);
end;

procedure p$archive_log
is
    p_action varchar2(100) := 'UTIL.PKG$LOG.P$ARCHIVE_LOG';
    var$flag number := 0;
begin
    pkg$log.p$start_log(p_action,'Карачев Н.А.','Солдатов Д.В.');

    for cur in (
        select d.partition_name, m.dt_month_last dt
        from dba_tab_partitions d
        inner join rus.month m on d.partition_name = 'LOG$MESSAGES_'||to_char(m.dt_month_last,'YY_MM')
        where d.table_owner = 'UTIL'
          and d.table_name = 'LOG$MESSAGES'
          and d.compression = 'DISABLED'
          and m.dt_month_last < trunc(sysdate,'mm')
        order by partition_name
    ) loop
        peo.pkg$ddl.p$compress(p_action,'UTIL','log$messages',cur.dt);
        var$flag := 1;
    end loop;

    if (var$flag = 0) then
        pkg$log.p$add_log(p_action,'Партиций для сжатия не найдено!','warning');
    end if;

    peo.pkg$ddl.p$stat(p_action,'UTIL','LOG$MESSAGES');

    pkg$log.p$end_log(p_action,'M');
exception when others then
    pkg$log.p$add_trace(p_action);
    raise;
end;

procedure p$test_mode(
    in$program_action in varchar2               -- Имя вызывающей процедуры
  , in$recipients in varchar2                    -- Перечень получателей тестовых логов
)
is
    pragma      autonomous_transaction;
begin
    if (in$recipients is null) then
        pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$TEST_MODE вызвала ошибку : Не указан обязательный параметр in$recipients (текущее значение null) !');
        return;
    end if;

    merge into util.log$test_mode trg
    using (
        select nvl(upper(in$program_action),c$default_action) program_action, in$recipients recipients
        from dual
    ) src on (trg.program_action = src.program_action)
    when matched then
        update set
            trg.recipients = src.recipients
    when not matched then
        insert (trg.program_action, trg.recipients)
        values (src.program_action, src.recipients);

    commit;
exception when others then
    pkg$log.p$send_admin_log('Процедура UTIL.PKG$LOG.P$TEST_MODE вызвала исключение :'||sqlerrm||' >> ' || dbms_utility.format_error_backtrace);
    rollback;
end;

end;
/

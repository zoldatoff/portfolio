﻿CREATE OR REPLACE package UTIL.pkg$mail as

  -- customize the smtp host, port and your domain name below.
  smtp_host   varchar2(256) := 'localhost';
  smtp_port   pls_integer   := 25;
  smtp_domain varchar2(256) := 'rs.ru';

  -- customize the signature that will appear in the email's mime header.
  -- useful for versioning.
  mailer_id   constant varchar2(256) := 'Mailer by Oracle UTL_SMTP';

  -- a unique string that demarcates boundaries of parts in a multi-part email
  -- the string should not appear inside the body of any part of the email.
  -- customize this if needed or generate this randomly dynamically.
  boundary        constant varchar2(256) := '-----7D81B75CCC90D2974F7A1CBD';

  first_boundary  constant varchar2(256) := '--' || boundary || utl_tcp.crlf;
  last_boundary   constant varchar2(256) := '--' || boundary || '--' || utl_tcp.crlf;

  -- a mime type that denotes multi-part email (mime) messages.
  multipart_mime_type constant varchar2(256) := 'multipart/mixed; boundary="'|| boundary || '"';
  max_base64_line_width constant pls_integer   := 76 / 4 * 3;

procedure p$send(
    in$recipients   in varchar2                 -- Список e-mail адресов
  , in$subject      in varchar2                 -- Тема письма
  , in$message      in clob                     -- Тело письма
  , in$priority     in pls_integer default null -- Приоритет письма
);

end;
/



CREATE OR REPLACE package body UTIL.pkg$mail as

function f$lookup_unquoted_char(
    in$str  in varchar2
  , in$chrs in varchar2
)
return pls_integer
is
    var$c            varchar2(5);
    var$i            pls_integer;
    var$len          pls_integer;
    var$inside_quote boolean;
begin
    var$inside_quote := false;
    var$i := 1;
    var$len := length(in$str);
    while (var$i <= var$len) loop
        var$c := substr(in$str, var$i, 1);

        if (var$inside_quote) then
            if (var$c = '"') then
                var$inside_quote := false;
            elsif (var$c = '\') then
                var$i := var$i + 1; -- skip the quote character
            end if;
            goto next_char;
        end if;

        if (var$c = '"') then
            var$inside_quote := true;
            goto next_char;
        end if;

        if (instr(in$chrs, var$c) >= 1) then
            return var$i;
        end if;

        <<next_char>>
        var$i := var$i + 1;

    end loop;

    return 0;
end;

function f$get_address(
    in$addr_list in out varchar2
)
return varchar2
is
    var$addr varchar2(256);
    var$i    pls_integer;
begin
    in$addr_list := ltrim(in$addr_list);
    var$i := f$lookup_unquoted_char(in$addr_list, ',;');
    if (var$i >= 1) then
        var$addr      := substr(in$addr_list, 1, var$i - 1);
        in$addr_list  := substr(in$addr_list, var$i + 1);
    else
        var$addr := in$addr_list;
        in$addr_list := '';
    end if;

    var$i := f$lookup_unquoted_char(var$addr, '<');
    if (var$i >= 1) then
        var$addr := substr(var$addr, var$i + 1);
        var$i := instr(var$addr, '>');
        if (var$i >= 1) then
            var$addr := substr(var$addr, 1, var$i - 1);
        end if;
    end if;

    return var$addr;
end;

procedure p$write_mime_header(
    in$conn  in out nocopy utl_smtp.connection
  , in$name  in varchar2
  , in$value in varchar2
)
is
begin
    utl_smtp.write_raw_data(in$conn, utl_raw.cast_to_raw(in$name || ': ' || in$value || utl_tcp.crlf));
end;

procedure p$write_boundary(
    in$conn  in out nocopy utl_smtp.connection
  , in$last  in            boolean default false
) as
begin
    if (in$last) then
        utl_smtp.write_data(in$conn, last_boundary);
    else
        utl_smtp.write_data(in$conn, first_boundary);
    end if;
end;

procedure p$write_text(
    in$conn    in out nocopy utl_smtp.connection
  , in$message in varchar2
)
is
begin
    utl_smtp.write_data(in$conn, in$message);
end;

procedure p$write_mb_text(
    in$conn    in out nocopy utl_smtp.connection
  , in$message in clob
) is
    var$inputCopy clob;

    var$str_buffer  varchar2(16000);
    var$cpos        pls_integer := 1;
    var$amount      integer := 32000;
begin
    while var$cpos < dbms_lob.getlength(in$message) loop
        utl_smtp.write_raw_data(in$conn, utl_raw.cast_to_raw(dbms_lob.substr(in$message, var$amount, var$cpos)));
        var$cpos := var$cpos + var$amount;
    end loop;
    /*var$inputCopy := in$message;
    while length(var$inputCopy) > 0
    loop
        if length(var$inputCopy) > 16000 then
            var$str_buffer:= substr(var$inputCopy,1,16000);
            utl_smtp.write_raw_data(in$conn, utl_raw.cast_to_raw(var$str_buffer));
            var$inputCopy:= substr(var$inputCopy,length(var$str_buffer)+1);
        else
            var$str_buffer := var$inputCopy;
            utl_smtp.write_raw_data(in$conn, utl_raw.cast_to_raw(var$str_buffer));
            var$inputCopy:= '';
            var$str_buffer := '';
        end if;
    end loop;*/
end;

procedure p$write_raw(
    in$conn    in out nocopy utl_smtp.connection
  , in$message in raw
)
is
begin
    utl_smtp.write_raw_data(in$conn, in$message);
end;


procedure p$begin_attachment(
    in$conn         in out nocopy utl_smtp.connection
  , in$mime_type    in varchar2 default 'text/plain'
  , in$inline       in boolean  default true
  , in$filename     in varchar2 default null
  , in$transfer_enc in varchar2 default null
  , in$content_id   in varchar2 default null
)
is
begin
    p$write_boundary(in$conn);
    p$write_mime_header(in$conn, 'Content-Type', in$mime_type);

    if (in$filename is not null) then
        if (in$inline) then
            p$write_mime_header(in$conn, 'Content-Disposition','inline; filename="'||in$filename||'"');
        else
            p$write_mime_header(in$conn, 'Content-Disposition','attachment; filename="'||in$filename||'"');
        end if;
    end if;

    if (in$transfer_enc is not null) then
        p$write_mime_header(in$conn, 'Content-Transfer-Encoding', in$transfer_enc);
    end if;

    if (in$content_id is not null) then
        p$write_mime_header(in$conn, 'Content-ID', in$content_id);
    end if;

    utl_smtp.write_data(in$conn, utl_tcp.crlf);
end;

procedure p$end_attachment(
    in$conn in out nocopy utl_smtp.connection
  , in$last in boolean default false
)
is
begin
    utl_smtp.write_data(in$conn, utl_tcp.crlf);
    if (in$last) then
        p$write_boundary(in$conn, in$last);
    end if;
end;

procedure p$attach_text(
    in$conn         in out nocopy utl_smtp.connection
  , in$data         in varchar2
  , in$mime_type    in varchar2 default 'text/plain'
  , in$inline       in boolean  default true
  , in$filename     in varchar2 default null
  , in$last         in boolean  default false
)
is
begin
    p$begin_attachment(in$conn, in$mime_type, in$inline, in$filename);
    p$write_text(in$conn, in$data);
    p$end_attachment(in$conn, in$last);
end;

procedure p$attach_base64(
    in$conn         in out nocopy utl_smtp.connection
  , in$data         in raw
  , in$mime_type    in varchar2 default 'application/octet'
  , in$inline       in boolean  default true
  , in$filename     in varchar2 default null
  , in$last         in boolean  default false
  , in$content_id   in varchar2 default null
)
is
    var$i   pls_integer;
    var$len pls_integer;
begin
    p$begin_attachment(in$conn, in$mime_type, in$inline, in$filename, 'base64', in$content_id);

    var$i   := 1;
    var$len := utl_raw.length(in$data);
    while (var$i < var$len) loop
        if (var$i + max_base64_line_width < var$len) then
            utl_smtp.write_raw_data(in$conn, utl_encode.base64_encode(utl_raw.substr(in$data, var$i, max_base64_line_width)));
        else
            utl_smtp.write_raw_data(in$conn,utl_encode.base64_encode(utl_raw.substr(in$data, var$i)));
        end if;
        utl_smtp.write_data(in$conn, utl_tcp.crlf);
        var$i := var$i + max_base64_line_width;
    end loop;

    p$end_attachment(in$conn, in$last);
end;

procedure p$attach_base64_clob(
    in$conn            in out nocopy utl_smtp.connection
  , in$data            in clob
  , in$mime_type       in varchar2 default 'application/octet'
  , in$inline          in boolean  default true
  , in$filename        in varchar2 default null
  , in$last            in boolean  default false
  , in$content_id      in varchar2 default null
)
is
    var$i       number(20);
    var$len     number(20);
begin
    p$begin_attachment(in$conn, in$mime_type, in$inline, in$filename, 'base64', in$content_id);

    var$i   := 1;
    var$len := length(in$data);
    while (var$i < var$len) loop
        if (var$i + max_base64_line_width < var$len) then
            utl_smtp.write_raw_data(in$conn, utl_encode.base64_encode(utl_raw.cast_to_raw(substr(in$data, var$i, max_base64_line_width))));
        else
            utl_smtp.write_raw_data(in$conn, utl_encode.base64_encode(utl_raw.cast_to_raw(substr(in$data, var$i))));
        end if;
        utl_smtp.write_data(in$conn, utl_tcp.crlf);
        var$i := var$i + max_base64_line_width;
    end loop;

    p$end_attachment(in$conn, in$last);
end;

function f$begin_session
return utl_smtp.connection
is
    var$conn utl_smtp.connection;
begin
    var$conn := utl_smtp.open_connection(smtp_host, smtp_port);
    utl_smtp.helo(var$conn, smtp_domain);
    return var$conn;
end;

procedure p$begin_mail_in_session(
    in$conn       in out nocopy utl_smtp.connection
  , in$sender     in varchar2
  , in$recipients in varchar2
  , in$subject    in varchar2
  , in$mime_type  in varchar2  default 'text/plain'
  , in$priority   in pls_integer default null
)
is
    var$my_recipients varchar2(32767) := in$recipients;
    var$my_sender     varchar2(32767) := in$sender;
begin
    utl_smtp.mail(in$conn, f$get_address(var$my_sender));

    while (var$my_recipients is not null) loop
        utl_smtp.rcpt(in$conn, f$get_address(var$my_recipients));
    end loop;

    utl_smtp.open_data(in$conn);

    p$write_mime_header(in$conn, 'From', in$sender);

    var$my_recipients := in$recipients;

    while (var$my_recipients is not null) loop
        p$write_mime_header(in$conn, 'To', f$get_address(var$my_recipients));
    end loop;

    p$write_mime_header(in$conn, 'Subject', in$subject);

    p$write_mime_header(in$conn, 'Content-Type', in$mime_type);

    p$write_mime_header(in$conn, 'X-Mailer', mailer_id);

    --   high      normal       low
    --   1     2     3     4     5
    if (in$priority is not null) then
        p$write_mime_header(in$conn, 'X-Priority', in$priority);
    end if;

    utl_smtp.write_data(in$conn, utl_tcp.crlf);

    if (in$mime_type like 'multipart/mixed%') then
        p$write_text(in$conn, 'This is a multi-part message in MIME format.' || utl_tcp.crlf);
    end if;
end;

procedure p$end_mail_in_session(
    in$conn in out nocopy utl_smtp.connection
)
is
begin
    utl_smtp.close_data(in$conn);
end;

procedure p$end_session(
    in$conn in out nocopy utl_smtp.connection
) is
begin
    utl_smtp.quit(in$conn);
end;

function f$begin_mail(
    in$sender     in varchar2,
    in$recipients in varchar2,
    in$subject    in varchar2,
    in$mime_type  in varchar2    default 'text/plain',
    in$priority   in pls_integer default null
)
return utl_smtp.connection
is
    var$conn utl_smtp.connection;
begin
    var$conn := f$begin_session;
    p$begin_mail_in_session(var$conn, in$sender, in$recipients, in$subject, in$mime_type, in$priority);
    return var$conn;
end;

procedure p$end_mail(
    in$conn in out nocopy utl_smtp.connection
) is
begin
    p$end_mail_in_session(in$conn);
    p$end_session(in$conn);
end;

procedure p$send(
    in$recipients   in varchar2
  , in$subject      in varchar2
  , in$message      in clob
  , in$priority     in pls_integer default null
)
is
    var$conn utl_smtp.connection;
begin
    var$conn := f$begin_mail('Хранилище данных ФД <fd.dwh@rsb.ru>', in$recipients, in$subject, 'text/html; charset=windows-1251',in$priority);
    p$write_mb_text(var$conn, in$message);
    p$end_mail(var$conn);
end;

end;
/

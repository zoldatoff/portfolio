﻿CREATE OR REPLACE package UTIL.pkg$f as

 function f$dt2id(in$dt date) return number deterministic parallel_enable;
 function f$id2dt(in$id number) return date deterministic parallel_enable;

 function f$nls_chr return varchar2 deterministic parallel_enable;

 function f$months_between(in$d1 in date,
                           in$d2 in date
                          ) return number deterministic parallel_enable;

 function f$to_date(in$dt              in varchar2,
                    in$regexp_template in varchar2 default null      -- Шаблон регулярного выражения
                   ) return date deterministic parallel_enable;

 function f$to_char(in$number          in number,                       -- Преобразуемое число
                    in$decimal_places  in number   default 2,           -- Число знаков после запятой
                    in$if_clause       in varchar2 default null,        -- Условие применения css-стиля
                    in$then_css        in varchar2 default 'red_btext'  -- Применённый css-стиль при выполнении условия in$if_clause
                   ) return varchar2 deterministic parallel_enable;

 function f$to_number(in$string          in varchar2,                   -- Преобразуемая строка
                      in$regexp_template in varchar2 default null,      -- Шаблон регулярного выражения
                      in$debug           in number   default 0          -- Режим отладки
                     )return number deterministic parallel_enable;

 --Функция, определяющая дату изменения по System change number
 function f$scn2date(in$scn_num number) return date deterministic;

end pkg$f;
/


CREATE OR REPLACE package body UTIL.pkg$f as

  function f$nls_chr return varchar2 deterministic parallel_enable
 is
 out$nls_chr varchar(1);
 begin

     select substr(value,1,1) into out$nls_chr from v$nls_parameters where parameter = 'NLS_NUMERIC_CHARACTERS';
     return out$nls_chr;
     exception when others then return 'Ошибка '||sqlerrm ||' >> ' || dbms_utility.format_error_backtrace;

 end f$nls_chr;


 function f$id2dt (in$id number)
 return date deterministic parallel_enable
 is
 out$dt date;
 begin

     select dt into out$dt from rus.dt where id_dt = in$id;
     return out$dt;

     exception when others then return null;

 end f$id2dt;

 function f$dt2id (in$dt date)
 return number deterministic parallel_enable
 is
 out$id number;
 begin

     select id_dt into out$id from rus.dt where dt = in$dt;
     return out$id;

     exception when others then return null;

 end f$dt2id;

 function f$months_between(in$d1 in date, in$d2 in date) return number deterministic parallel_enable
 is
 v$ret number;
 begin

 -- Особый случай для последнего дня февраля.
 -- Дело в том, что период с 31.01 по 28.02 -- 1 месяц, а с 30.01 по 28.02 -- примерно 0.97
  v$ret := case when last_day(in$d1) = in$d1 and to_char(in$d1,'mm') = '02' and to_char(in$d2,'dd') in ('29','30') then trunc(months_between(in$d1,last_day(in$d2)))
                                                                                                                   else trunc(months_between(in$d1,in$d2))
           end;

  return v$ret;

 exception when others then
    return null;
 end;

  function f$to_date(in$dt in varchar2, in$regexp_template in varchar2 default null) return date deterministic parallel_enable
 is
    v$ret date;
 begin
    /*Сразу return без PLSQL — работает быстрее*/
    case when (trim(lower(in$regexp_template)) = 'rus')                    then return trim(regexp_substr(in$dt, '[0-9]{2}[.][0-9]{2}[.][0-9]{2,4}($|\D)'));
         when (trim(lower(in$regexp_template)) = 'dd.mm.yy')               then return to_date(regexp_substr(in$dt, '[0-9]{2}[.][0-9]{2}[.][0-9]{2}'),'dd.mm.yy');
         when (trim(lower(in$regexp_template)) = 'dd.mm.yyyy')             then return to_date(regexp_substr(in$dt, '[0-9]{2}[.][0-9]{2}[.][0-9]{4}'),'dd.mm.yyyy');
         when (trim(lower(in$regexp_template)) = 'dd.mm.yyyy hh24:mi:ss')  then return to_date(regexp_substr(in$dt, '[0-9]{2}[.][0-9]{2}[.][0-9]{4}[ ]{1}[0-9]{2}[:][0-9]{2}[:][0-9]{2}'), 'dd.mm.yyyy hh24:mi:ss');
                                                                           else return to_date(in$dt);
    end case;

 exception when others then
    return null;
 end;

 function f$to_char(in$number         in number,
                    in$decimal_places in number   default 2,
                    in$if_clause      in varchar2 default null,
                    in$then_css       in varchar2 default 'red_btext'
                   ) return varchar2 deterministic parallel_enable

    is

 v$number          number := 0;
 v$1st_compare     number;
 v$2nd_compare     number;
 out$char          varchar2(1000);
 v$mask            varchar2(30) := '999G999G999G999G990';
 v$if_clause       varchar2(1000);
 v$colored_flag    number := 0;
 v$decimal_places  number := least(in$decimal_places, 20);
 v$then_css        varchar2(100) := regexp_substr(in$then_css, '^[a-zA-Z0-9_]*$'); -- Начало строки + буквы латиницей(большие и малые), цифры и знаки подчеркивания + конец строки


begin

 -- Парсим условие раскраски
 if (in$if_clause is not null) then

    v$if_clause := lower(replace(replace(in$if_clause, ' ', ''),'.',','));
    if (v$if_clause like '%|x|%' or v$if_clause like '%abs(x)%' ) then v$number := abs(in$number); else v$number := in$number; end if;

    v$1st_compare := to_number(regexp_substr(v$if_clause, '[[:digit:]\,\-]*'));
    v$2nd_compare := to_number(regexp_substr(v$if_clause, '[[:digit:]\,\-]*$'));

    v$colored_flag := case when (v$if_clause like '%<%x%<%') then
                              case when (v$if_clause like '%<=%x%<=%' and v$1st_compare <= v$number and v$number <= v$2nd_compare) then 1
                                   when (v$if_clause like '%<=%x%<%'  and v$1st_compare <= v$number and v$number <  v$2nd_compare) then 1
                                   when (v$if_clause like '%<%x%<=%'  and v$1st_compare <  v$number and v$number <= v$2nd_compare) then 1
                                   when (v$if_clause like '%<%x%<%'   and v$1st_compare <  v$number and v$number <  v$2nd_compare) then 1
                                                                                                                                   else 0
                              end
                           else
                              case when (v$if_clause like '%x%<=%'                                  and v$number <= v$2nd_compare) then 1
                                   when (v$if_clause like '%x%>=%'                                  and v$number >= v$2nd_compare) then 1
                                   when (v$if_clause like '%x%<%'                                   and v$number <  v$2nd_compare) then 1
                                   when (v$if_clause like '%x%>%'                                   and v$number >  v$2nd_compare) then 1
                                                                                                                                   else 0
                              end
                      end;
 end if;

if (in$decimal_places>0) then
    v$mask := v$mask||'D';
    for i in 1..in$decimal_places
    loop v$mask := v$mask||'9'; end loop;
end if;

if (v$colored_flag = 1) then out$char := '<span class="'||v$then_css||'">'||trim(replace(to_char(in$number, v$mask), '.', ','))||'</span>';
                        else out$char :=trim(replace(to_char(in$number, v$mask), '.', ','));
end if;

 return out$char;

 exception when others then return 'Ошибка '||sqlerrm ||' >> ' || dbms_utility.format_error_backtrace;

    end f$to_char;


 function f$to_number(in$string          in varchar2,
                      in$regexp_template in varchar2 default null,
                      in$debug           in number   default 0
                     )return number deterministic parallel_enable
    is
    out$number        number;
    v$string          varchar2 (1000);
    v$out_str         varchar2 (1000);
    v$regexp_template varchar2 (1000);

    begin

    case when (trim(lower(in$regexp_template)) = 'number')                  then return /*Сразу return без PLSQL — работает быстрее*/ regexp_substr(in$string,'^[[:digit:]]+$');  -- Вся строка состоит строго из цифр
         when (trim(lower(in$regexp_template)) = 'rus$n_dog')               then v$regexp_template := '(^|\D)[0-9]{7,10}($|\D)';
         when (trim(lower(in$regexp_template)) = 'rus$idblank')             then v$regexp_template := '(^|\D)[0-9]{7,10}($|\D)';
         when (trim(lower(in$regexp_template)) = 'rus$policynumber')        then v$regexp_template := '(^|\D)[0-9]{11,12}($|\D)';
         when (trim(lower(in$regexp_template)) = 'rus$wallet_cabinet_user') then v$regexp_template := 'CABINET_USER=[0-9]{10}([\][n]|$)';
         when (trim(lower(in$regexp_template)) = 'rus$wallet_number')       then v$regexp_template := 'NUMBER=[0-9]{10}[\][n]';
         when (trim(lower(in$regexp_template)) = 'rus$loan_insur')          then v$regexp_template := '[№|N][ ]*[[:digit:]]+';
         when (trim(lower(in$regexp_template)) = 'rus$conto')               then v$regexp_template := '^BRS_K[[:digit:]]{5}([\]|[_])';
         when (trim(lower(in$regexp_template)) = 'rus$subconto')            then v$regexp_template := '(_|-)S[[:digit:]]+([\]|$)';
         when (trim(lower(in$regexp_template)) = 'ukr$dealno')              then return /*Сразу return без PLSQL — работает быстрее*/ regexp_substr(in$string,'\d+');             -- Первое что-то похожее на число
         when (trim(lower(in$regexp_template)) = 'ukr$posting_n_dog')       then return regexp_substr
                                                                                        (
                                                                                            regexp_substr
                                                                                            (
                                                                                               replace(in$string,'ШАБЛОН 17_21  '), -- исправляем доработку дита (между № и номером договора теперь шаблон)
                                                                                               '(Дог|дог|тран|Тран|сделки|ДОГ|Кред|КРЕД|КД|кредиту|кредита|договору)+[[:alpha:]|.]*[[:space:]]*[№|N]*[[:space:]]*[[:digit:]]+'
                                                                                            ), '[[:digit:]]+'
                                                                                        );
         else /*Сразу return без PLSQL — работает быстрее*/ return regexp_substr(in$string,'\d+');
    end case;

    if (in$debug = 1) then dbms_output.put_line('Используемый шаблон регулярного выражения: "'||v$regexp_template||'"'); end if;

    --v$string := trim(replace(replace(in$string,'.',f$nls_chr),',',f$nls_chr));
    v$string := trim(in$string);

    if (in$debug = 1) then dbms_output.put_line('Заменяемая строка: "'||v$string||'"'); end if;

     v$out_str :=  regexp_substr(v$string,v$regexp_template);

        if (in$debug = 1) then dbms_output.put_line('Результирующая строка: "'||v$out_str||'"'); end if;

        if (in$debug = 1) then dbms_output.put_line('Проверка второго вхождения в строке: "'||substr(v$string,regexp_instr(v$string,v$regexp_template)+length(v$out_str))||'"'); end if;
        -- Если есть второе вхождение по заданному шаблону возвращаем null
        if regexp_substr(v$string,v$regexp_template,regexp_instr(v$string,v$regexp_template)+length(v$out_str)) is not null then
            v$out_str := null;
            if (in$debug = 1) then dbms_output.put_line('Обнаружено второе вхождение по заданному шаблону! Будет возвращено пустое значение.'); end if;
        end if;

    if (in$regexp_template is null) then out$number :=  to_number(v$out_str);                   return out$number;
                                    else out$number :=  f$to_number(v$out_str, null, in$debug); return out$number;
    end if;

    exception when others then return null;

    end f$to_number;


 function f$scn2date (in$scn_num number)
 return date deterministic
 is
 out$dt date;
 begin

     select scn_date into out$dt from util.dw_scn where in$scn_num between scn_num_min and scn_num_max;
     return out$dt;

     exception when others then return null;

 end f$scn2date;

 end pkg$f;
/

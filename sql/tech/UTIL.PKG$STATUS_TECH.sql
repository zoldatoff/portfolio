﻿CREATE OR REPLACE package UTIL.pkg$status_tech as

c$lag   number := 2/24; -- Сдвиг ожидания техдаты с 00:00 на 22:00

function f$get$date(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2 default null -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return date deterministic;

procedure p$set$date(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date date default sysdate        -- опционально: значение техдаты, значение по-умолчанию - sysdate
);

function f$get$number(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2              -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return number deterministic;

procedure p$set$number(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_number number                    -- значение техномера
);

function f$wait_until(
   in$p_action in varchar2                          -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                             -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                        -- название таблицы-приёмника
 , in$hour_to_wait in integer                       -- час отсечки (integer в диапазоне от 0 до 23)
 , in$t_date_name in varchar2 default null          -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date in date default trunc(sysdate+c$lag,'DD')-c$lag -- опционально: значение техдаты, значение по-умолчанию - sysdate
)
return varchar2;

procedure p$wait_until(
   in$p_action in varchar2                          -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                             -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                        -- название таблицы-приёмника
 , in$hour_to_wait in integer                       -- час отсечки (integer в диапазоне от 0 до 23)
 , in$t_date_name in varchar2 default null          -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date in date default trunc(sysdate+c$lag,'DD')-c$lag -- опционально: значение техдаты, значение по-умолчанию - sysdate
);

function f$get(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2 default null -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return date deterministic;

procedure p$set(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date date default sysdate        -- опционально: значение техдаты, значение по-умолчанию - sysdate
);

end;
/



CREATE OR REPLACE package body UTIL.pkg$status_tech as

--####################### Private ##############################################
function f$get$name(
    in$owner in varchar2
  , in$table_name in varchar2
  , in$t_date_name in varchar2
) return varchar2
is
begin
    return nvl(upper(in$owner),'NULL')||'.'||nvl(upper(in$table_name),'NULL')||case when in$t_date_name not like '@%' then '.' else '' end||upper(in$t_date_name);
exception when others then
    return '';
end;

function f$get$date$dwh(
   in$owner in varchar2             -- схема, где расположена таблица-источник на Хранилище
 , in$table_name in varchar2        -- название таблицы-источника на Хранилище
)
return date
is
    v$maxdate           date;
    v$search_table      util.status_tech$external.search_table%type;
    v$search_string     util.status_tech$external.search_string%type;

    ex$no_method        exception;
    ex$no_date          exception;
begin
    select upper(search_table), search_string
    into  v$search_table, v$search_string
    from util.status_tech$external e
    where upper(e.owner) = upper(in$owner)
      and upper(e.table_name) = upper(in$table_name)
      and upper(database_name) = '@DWH';

    if (v$search_table = 'ORAWH.LOG_AUTO_PROC') then
        select max(date_begin)
        into v$maxdate
        from orawh.log_auto_proc@dwh.rs.ru l
        where ( l.message like v$search_string or l.dsc like v$search_string)
          and l.status_id = 2;

        if (v$maxdate is null) then
            raise ex$no_date;
        end if;

        return v$maxdate;
    elsif (v$search_table = 'DWH.TXT_LOAD_LOG') then
        select max(begin_time)
        into v$maxdate
        from dwh.txt_load_log@dwh.rs.ru
        where upper(txt_code) = upper(in$table_name)
          and session_note like v$search_string
          and l_finish = 1
          and all_str_count > 0;

        if (v$maxdate is null) then
            raise ex$no_date;
        end if;

        return v$maxdate;
    elsif (v$search_table = 'RS_LASTCOMMIT') then
        if (v$search_string = '1') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh1.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '2') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh2.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '3') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh3.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '4') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh4.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '5') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh5.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '6') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh6.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '7') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh7.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '8') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh8.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '9') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh9.rs_lastcommit@dwh.rs.ru where origin > 0;
        elsif (v$search_string = '10') then
            select case when origin_time is null or origin_time < '01.01.2014' then dest_commit_time else origin_time end
            into v$maxdate
            from orawh10.rs_lastcommit@dwh.rs.ru where origin > 0;
        end if;

        if (v$maxdate is null or v$maxdate < '01.01.2014') then
            raise ex$no_date;
        end if;

        return v$maxdate;
    end if;

    raise ex$no_method;
exception
    when ex$no_date then
        util.pkg$log.p$add_log(
            null
          , 'При чтении техдаты <b>'||f$get$name(in$owner,in$table_name,null)||'</b> '
          ||'произошла ошибка в функции UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH : Не найдена дата в одной из таблиц логов DWH.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH'
        );
        return c$notappldate;
    when ex$no_method then
        util.pkg$log.p$add_log(
            null
          , 'При чтении техдаты <b>'||f$get$name(in$owner,in$table_name,null)||'</b> '
          ||'произошла ошибка функции UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH : Не найден обработчик под заданные параметры таблицы UTIL.STATUS_TECH$EXTERNAL.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH'
        );
        return c$notappldate;
    when no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'При чтении техдаты <b>'||f$get$name(in$owner,in$table_name,null)||'</b> '
          ||'произошла ошибка функции UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH : Не найдены параметры в таблице UTIL.STATUS_TECH$EXTERNAL.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH'
        );
        return c$notappldate;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'При чтении техдаты <b>'||f$get$name(in$owner,in$table_name,null)||'</b> произошла ошибка : '
          ||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE$DWH'
        );
        return c$notappldate;
end;

--####################### Public ###############################################

function f$get$date(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2 default null -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return date deterministic
is
    var$ret date;
    ex$no_data_found    exception;
begin
    if (upper(in$t_date_name) = '@DWH') then
        return pkg$status_tech.f$get$date$dwh(in$owner, in$table_name);
    end if;

    select min(nvl(tech_date , c$notappldate))
    into var$ret
    from util.status_tech
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and t_date_name = nvl(upper(in$t_date_name),t_date_name);

    if (var$ret is null) then
        raise ex$no_data_found;
    end if;

    return var$ret;
exception
    when ex$no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'Техдата <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' не найдена в таблице UTIL.STATUS_TECH.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE'
        );
        return c$notappldate;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'При чтение техдаты <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b> '
          ||'произошла ошибка :'||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$DATE'
        );
        return c$notappldate;
end;

procedure p$set$date(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date date default sysdate        -- опционально: значение техдаты, значение по-умолчанию - sysdate
)
is
    pragma autonomous_transaction;
    var$dt date;
    var$desc util.status_tech.description%type;
begin
    var$dt := nvl(in$tech_date,sysdate);

    if (var$dt > sysdate) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'При установке техдаты <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' передано недопустимое значение параметра in$tech_date = '||to_char(var$dt,'dd.mm.yyyy hh24:mi:ss')||'.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$DATE'
        );

        return;
    end if;

    if (in$t_date_name like '@%') then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Неудачная попытка установить техдату '
          ||f$get$name(in$owner,in$table_name,in$t_date_name)
          ||'. Техдата для таблиц, размещённых на внешних серверах, доступна только для чтения.'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$DATE'
        );

        return;
    end if;

    update util.status_tech
    set tech_date = var$dt
      , p_action = upper(in$p_action)
      , date_ins_upd = sysdate
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and nvl(t_date_name,'x') = coalesce(upper(in$t_date_name),'x')
    returning description into var$desc;

    if (sql%rowcount > 0) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Установлена техдата <b>'
          ||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||case when var$desc is not null then ' ['||var$desc||']' end
          ||' = '||to_char(var$dt,'dd.mm.yyyy hh24:mi:ss')
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$DATE'
        );
    else
        insert into util.status_tech(
            owner, table_name, t_date_name, tech_date, p_action, description, date_ins_upd
        )
        values (
            upper(in$owner), upper(in$table_name), upper(in$t_date_name), var$dt, upper(in$p_action), null, sysdate
        );

        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Добавлена новая техдата <b>'
          ||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b> = '
          ||to_char(var$dt,'dd.mm.yyyy hh24:mi:ss')
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$DATE'
        );
    end if;

    commit;
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'При установке техдаты <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)
      ||'</b> произошла ошибка : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      , 'error'
      , 'date','eol'
      , 'UTIL.PKG$STATUS_TECH.P$SET$DATE'
    );
    rollback;
end;

function f$get$number(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2              -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return number deterministic
is
    var$ret             util.status_tech.tech_number%type;
    ex$no_data_found    exception;
begin
    select tech_number
    into var$ret
    from util.status_tech
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and t_date_name = upper(in$t_date_name);

    if (var$ret is null) then
        raise ex$no_data_found;
    end if;

    return var$ret;
exception
    when ex$no_data_found then
        util.pkg$log.p$add_log(
            null
          , 'Техномер <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' не указан в таблице UTIL.STATUS_TECH.'
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$NUMBER'
        );
        return null;
    when others then
        util.pkg$log.p$add_log(
            null
          , 'При чтении техномера <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' произошла ошибка : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
          , 'error'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$GET$NUMBER'
        );
        return null;
end;

procedure p$set$number(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_number number                    -- значение техномера
)
is
    pragma autonomous_transaction;

    var$desc util.status_tech.description%type;
begin
    if (in$t_date_name like '@%') then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Неудачная попытка установить техномер '
          ||f$get$name(in$owner,in$table_name,in$t_date_name)
          ||'. Техномер для внешних серверов не поддерживается.'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$NUMBER'
        );

        return;
    end if;

    update util.status_tech
    set tech_number = in$tech_number
      , p_action = upper(in$p_action)
      , tech_date = sysdate
      , date_ins_upd = sysdate
    where owner = upper(in$owner)
      and table_name = upper(in$table_name)
      and nvl(t_date_name,'x') = coalesce(upper(in$t_date_name),'x')
    returning description into var$desc;

    if (sql%rowcount > 0) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Установлен техномер <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||case when var$desc is not null then ' ['||var$desc||']' end
          ||' = '||to_char(in$tech_number)
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$NUMBER'
        );
    else
        insert into util.status_tech(
            owner, table_name, t_date_name, tech_date, tech_number, p_action, description, date_ins_upd
        )
        values (
            upper(in$owner), upper(in$table_name), upper(in$t_date_name), sysdate, in$tech_number, upper(in$p_action), null, sysdate
        );

        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Добавлен новый техномер <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||to_char(in$tech_number)
          , 'notice'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.P$SET$NUMBER'
        );
    end if;

    commit;
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'При установке техномера <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' произошла ошибка : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      , 'error'
      , 'date','eol'
      , 'UTIL.PKG$STATUS_TECH.P$SET$NUMBER'
    );
    rollback;
end;

function f$wait_until(
   in$p_action in varchar2                          -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                             -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                        -- название таблицы-приёмника
 , in$hour_to_wait in integer                       -- час отсечки (integer в диапазоне от 0 до 23)
 , in$t_date_name in varchar2 default null          -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date in date default trunc(sysdate+c$lag,'DD')-c$lag -- опционально: значение техдаты, значение по-умолчанию - sysdate
)
return varchar2
is
    var$dt date;
    var$desc util.status_tech.description%type;
begin
    begin
        select description
        into var$desc
        from util.status_tech
        where owner = upper(in$owner)
          and table_name = upper(in$table_name)
          and t_date_name = upper(in$t_date_name);
    exception when others then
        var$desc := null;
    end;

    if (in$hour_to_wait is null or in$hour_to_wait not between 0 and 23) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Час отсечки должен быть в диапазоне от 0 до 23 часов'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
        );
        return 'ERROR';
    end if;

    if (pkg$status_tech.f$get(in$owner,in$table_name,in$t_date_name) = c$notappldate) then
        util.pkg$log.p$add_log(
            upper(in$p_action)
          , 'Техдата <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
          ||' не найденна в таблице UTIL.STATUS_TECH'
          , 'warning'
          , 'date','eol'
          , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
        );
        return 'ERROR';
    end if;

    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'Ждем появления техдаты <b>'
      ||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
      ||case when var$desc is not null then ' ['||var$desc||']' end
      ||' до '||in$hour_to_wait||':00'
      , 'notice','date','eol'
      , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
    );

    loop
        if (nvl(in$tech_date,trunc(sysdate+c$lag,'DD')-c$lag) <= pkg$status_tech.f$get(in$owner,in$table_name,in$t_date_name)) then
            util.pkg$log.p$add_log(
                upper(in$p_action)
              , 'Дождались техдаты <b>'
              ||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
              ||case when var$desc is not null then ' ['||var$desc||']' end
              ||' = '
              ||to_char(pkg$status_tech.f$get(in$owner,in$table_name,in$t_date_name),'dd.mm.yyyy hh24:mi:ss')
              , 'notice','date','eol'
              , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
            );
            return 'TECH_DATE';
        --elsif (to_number(to_char(sysdate,'hh24')) >= in$hour_to_wait) then
        elsif(case when to_char(sysdate+c$lag,'hh24') < in$hour_to_wait then 0
                   when to_char(sysdate,'hh24') < in$hour_to_wait then 0
                   else 1
              end = 1) then
            util.pkg$log.p$add_log(
                upper(in$p_action)
              , 'Истекло время ожидания техдаты <b>'
              ||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
              ||case when var$desc is not null then ' ['||var$desc||']' end
              ||' до '||in$hour_to_wait||':00'
              , 'warning','date','eol'
              , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
            );
            return 'TIME_OF_DAY';
        end if;

        peo.pkg$ddl.p$sleep_for(null,1);
    end loop;
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'При ожидании техдаты <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
      ||' произошла ошибка : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      , 'warning','date','eol'
      , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
    );
    return 'ERROR';
end;

procedure p$wait_until(
   in$p_action in varchar2                          -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                             -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                        -- название таблицы-приёмника
 , in$hour_to_wait in integer                       -- час отсечки (integer в диапазоне от 0 до 23)
 , in$t_date_name in varchar2 default null          -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date in date default trunc(sysdate+c$lag,'DD')-c$lag -- опционально: значение техдаты, значение по-умолчанию - sysdate
)
is
    var$dt date;
    var$result varchar2(100);
begin
    var$result := f$wait_until(in$p_action, in$owner, in$table_name, in$hour_to_wait, in$t_date_name, in$tech_date);
exception when others then
    util.pkg$log.p$add_log(
        upper(in$p_action)
      , 'При ожидании техдаты <b>'||f$get$name(in$owner,in$table_name,in$t_date_name)||'</b>'
      ||' произошла ошибка : '||SQLERRM||' >> '||dbms_utility.format_error_backtrace
      , 'warning','date','eol'
      , 'UTIL.PKG$STATUS_TECH.F$WAIT_UNTIL'
    );
end;

function f$get(
    in$owner in varchar2                    -- схема, где расположена таблица-приёмник
  , in$table_name in varchar2               -- название таблицы-приёмника
  , in$t_date_name in varchar2 default null -- опционально: название техдаты (например, название поля в таблице-приёмнике или название потока данных)
) return date deterministic
is
begin
    return pkg$status_tech.f$get$date(in$owner, in$table_name, in$t_date_name);
end;

procedure p$set(
   in$p_action  in varchar2                 -- наименование лога, куда процедура пишет сообщения
 , in$owner in varchar2                     -- схема, где расположена таблица-приёмник
 , in$table_name in varchar2                -- название таблицы-приёмника
 , in$t_date_name in varchar2               -- название техдаты (например, название поля в таблице-приёмнике или название потока данных)
 , in$tech_date date default sysdate        -- опционально: значение техдаты, значение по-умолчанию - sysdate
)
is
begin
    pkg$status_tech.p$set$date(in$p_action, in$owner, in$table_name, in$t_date_name, in$tech_date);
end;

end;
/

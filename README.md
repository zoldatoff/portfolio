# Dmitry Soldatov’s Programming Portfolio

Информацию обо мне вы можете посмотреть в моём [резюме](https://bitbucket.org/zoldatoff/portfolio/downloads/cv.pdf)

Подробнее о моих работах написано в [wiki](https://bitbucket.org/zoldatoff/portfolio/wiki/Home)
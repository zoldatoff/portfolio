\newpage
\section{Word Association Mining and Analysis}

\subsection{Basic Word Relations}

These two basic and complementary relations can be generalized to describe relations of any items in a language:
\begin{itemize}
  \item \textbf{Paradigmatic}: A \& B have paradigmatic relation if they can be substituted for each other (i.e., A \& B are in the same class). E.g., <<cat>> and <<dog>>; <<Monday>> and <<Tuesday>>.\begin{itemize}
  \item Represent each word by its context
  \item Compute context similarity
  \item Words with high context similarity likely have paradigmatic relation
\end{itemize}

  \item \textbf{Syntagmatic}: A \& B have syntagmatic relation if they can becombined with each other (i.e., A \& B are related semantically). E.g., <<cat>> and <<sit>>; <<car>> and <<drive>>.
\begin{itemize}
  \item Count how many times two words occur together in a context (e.g.,sentence or paragraph)
  \item Compare their co-occurrences with their individual occurrences
  \item Words with high co-occurrences but relatively low individual occurrences likely have syntagmatic relation
\end{itemize}
\end{itemize}
Paradigmatically related words tend to have syntagmatic relation with the same word $\to$ joint discovery of the two relations.




\subsection{Paradigmatic Relation Discovery}

\subsubsection{Word Context as <<Pseudo Document>>}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{word_context.png}
\end{figure}


\subsubsection{Vector Space Model}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{vector_space_model.png}
\end{figure}


\subsubsection{Expected Overlap of Words in Context (EOWC)}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{EOWC.png}
\end{figure}

Disadvantages of EOWC:
\begin{itemize}
  \item favors matching one frequent term very well over matchingmore distinct terms.
  \item treats every word equally (overlap on <<the>> isn’t as so meaningful as overlap on <<eats>>).
\end{itemize}


\subsubsection{Term Frequency: BM25 Transformation}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{TF.png}
\end{figure}


\subsubsection{IDF Weighting: Penalizing Popular Terms}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{IDF.png}
\end{figure}


\subsubsection{Adapting BM25 Retrieval Model for Paradigmatic Relation Mining}

\begin{equation*}
	\mathrm{Sim}(d_1, d_2) = \sum_{i=1}^N \mathrm{IDF}(w_i) \cdot x_i \cdot y_i,
\end{equation*}
where $d_1 = (x_1 \dots x_N), d_2 = (y_1 \dots y_N)$ are contexts of $1^{st}$ and $2^{nd}$ word

\begin{equation*}
	x_i = \frac{\mathrm{BM25}(w_i, d_1)}{\sum\limits_{j=1}^N \mathrm{BM25}(w_j, d_1)}
\end{equation*}

\begin{equation*}
	\mathrm{BM25}(w_i, d_1) = \frac{(k+1) c(w_i, d_1)}{c(w_i, d_1) + k(1-b+b*|d_1|/avdl)}, b \in [0, 1], k \in [0, +\infty)
\end{equation*}


\subsubsection{BM25 and Syntagmatic Relations}
IDF-weighted $d_1 = (x_1 \cdot \mathrm{IDF}(w_1), \dots x_N \cdot \mathrm{IDF}(w_N))$\\

The highly weighted terms in the context vector of word $w$ are likely syntagmatically related to $w$.


\subsubsection{Summary}

Main idea for discovering paradigmatic relations:\begin{itemize}
  \item Collecting the context of a candidate word to form a pseudo document (bag of words)
  \item Computing similarity of the corresponding context documents of two candidate words
  \item Highly similar word pairs can be assumed to have paradigmatic relations\\
\end{itemize}

Text retrieval models can be easily adapted for computing similarity of two context documents:\begin{itemize}
  \item BM25 + IDF weighting represents the state of the art
  \item Syntagmatic relations can also be discovered as a <<by product>>
\end{itemize}




\subsection{Syntagmatic Relation Discovery}

\subsubsection{Entropy}

Binary random variable $X_w$:
\begin{eqnarray*}
	X_w = 
	\begin{cases}
		1 & \mbox{ if word }w\mbox{ is present}\\
		0 & \mbox{ if word }w\mbox{ is absent}
	\end{cases} \\
	p(X_w=1) + p(X_w=0) = 1
\end{eqnarray*}

The more random $X_w$ is, the more difficult the prediction would be. \textbf{Entropy} $H(X)$ measures randomness of $X$ (assume $0 \cdot \log 0 = 0$):
\begin{equation*}
	H(X_w) = \sum_{v \in \{0, 1\}} \bigg(-p(X_w=v)\log_2p(X_w=v)\bigg)
\end{equation*}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{entropy.png}
\end{figure}


\subsubsection{Conditional Entropy}

If $H(Y \,\big|\, X=x)$ is the entropy of the variable $Y$ conditioned on the variable $X$ taking a certain value $x$, then $H(Y \,\big|\, X)$ is the result of averaging $H(Y \,\big|\, X=x)$ over all possible values $x$ that $X$ may take. Given discrete random variables $X$ with domain $\mathcal X$ and $Y$ with domain $\mathcal Y$, the conditional entropy of $Y$ given $X$ is defined as:

\begin{align*}
H(Y \,\big|\, X)\ &\equiv \sum_{x\in\mathcal X}\,p(x)\,H(Y \,\big|\, X=x)\\
& =-\sum_{x\in\mathcal X} p(x)\sum_{y\in\mathcal Y}\,p(y \big| x)\,\log_2\, p(y \big| x)\\
& =-\sum_{x\in\mathcal X}\sum_{y\in\mathcal Y}\,p(x,y)\,\log_2\,p(y \big| x)\\
& =-\sum_{x\in\mathcal X, y\in\mathcal Y}p(x,y)\log_2\,p(y \big| x)\\
& =-\sum_{x\in\mathcal X, y\in\mathcal Y}p(x,y)\log_2 \frac {p(x,y)} {p(x)}. \\
& = \sum_{x\in\mathcal X, y\in\mathcal Y}p(x,y)\log_2 \frac {p(x)} {p(x,y)}. \\
\end{align*}

Conditional entropy for mining syntagmatic relations:
\begin{itemize}
  \item For each word $W1$\begin{itemize}
  \item For every other word $W2$, compute conditional entropy $H(X_{W1} \big| X_{W2})$
  \item Sort all the candidate words in ascending order of $H(X_{W1} \big| X_{W2})$
  \item Take the top-ranked candidate words as words that have potential syntagmatic relations with $W1$
  \item Need to use a threshold for each $W1$
\end{itemize}
  \item However, while $H(X_{W1} \big| X_{W2})$ and $H(X_{W1} \big| X_{W3})$ are comparable, $H(X_{W1} \big| X_{W2})$ and $H(X_{W3} \big| X_{W2})$ aren’t!
\end{itemize}



\subsubsection{Mutual Information}
How much reduction in the entropy of $X$ can we obtain by knowing $Y$? \textbf{Mutual information} is a measure of the variables' mutual dependence:

\begin{equation*}
 I(X;Y) = \sum_{y \in Y} \sum_{x \in X} 
     p(x,y) \log{ \left(\frac{p(x,y)}{p(x)\,p(y)}\right) }
\end{equation*}

MI measures the divergence of the actual joint distribution $p(x,y)$ from the expected distribution under the independence assumption $p(x)\,p(y)$. The larger the divergence is, the higher the MI would be.\\

\pagebreak
Mutual information can be equivalently expressed as:

\begin{align*}
I(X;Y) & {} = H(X) - H(X \,\big|\, Y) \\ 
& {} = H(Y) - H(Y \,\big|\, X) \\ 
& {} = H(X) + H(Y) - H(X,Y) \\
& {} = H(X,Y) - H(X \,\big|\, Y) - H(Y \,\big|\, X)
\end{align*}


\begin{figure}[H]
\begin{minipage}[H]{0.45\linewidth}
    \centering
    \includegraphics[width=\linewidth]{mutual_information.png}
\end{minipage}
\hfill    
\begin{minipage}[H]{0.52\linewidth}
    \caption{Venn diagram for various information measures associated with correlated variables $X$ and $Y$. The area contained by both circles is the joint entropy $H(X,Y)$. The circle on the left (red and violet) is the individual entropy $H(X)$, with the red being the conditional entropy $H(X \,\big|\, Y)$. The circle on the right (blue and violet) is $H(Y)$, with the blue being $H(Y \,\big|\, X)$. The violet is the mutual information $I(X;Y)$.}
\end{minipage}    
\end{figure}

Properties:\begin{itemize}
  \item Non-negative: $I(X;Y) \geqslant 0$
  \item Symmetric: $I(X;Y)=I(Y;X)$
  \item $I(X;Y)=0$ if $X$ \& $Y$ are independent
\end{itemize}
When we fix $X$ to rank different $Y$'s, $I(X;Y)$ and $H(X \,\big|\, Y)$ give the same order but $I(X;Y)$ allows us to compare different $(X,Y)$ pairs.\\

If $\mathcal X = \{0, 1\}$ and $\mathcal Y = \{0, 1\}$ then we only need to know $p(X_{W1}=1), p(X_{W2}=1)$ and $p(X_{W1}=1, X_{W2}=1)$ to compute $I(X_{W1}, X_{W2})$:

\begin{minipage}[H]{0.45\linewidth}
\begin{eqnarray*}
	&p(X_{W1} = 1) = \dfrac{\mathrm{count}(W1)}{N} \\
	&p(X_{W2} = 1) = \dfrac{\mathrm{count}(W2)}{N} \\
	&p(X_{W1} = 1, X_{W2} = 1) = \dfrac{\mathrm{count}(W1, W2)}{N}		
\end{eqnarray*}
\end{minipage}
\hfill
\begin{minipage}[H]{0.45\linewidth}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{prob1.png}
\end{figure}
\end{minipage}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{prob2.png}
\end{figure}


\subsubsection{Summary}

\begin{itemize}
  \item Syntagmatic relation can be discovered by measuring correlations between occurrences of two words.  \item Three concepts from Information Theory:\begin{itemize}
  \item Entropy $H(X)$: measures the uncertainty of a random variable $X$
  \item Conditional entropy $H(X \,\big|\, Y)$: entropy of $X$ given we know $Y$
  \item Mutual information $I(X;Y)$: entropy reduction of $X$ due to knowing $Y$
\end{itemize}
  \item Mutual information provides a principled way for discovering syntagmatic relations.
\end{itemize}




\subsection{Summary}
\begin{itemize}
  \item Two basic associations: paradigmatic and syntagmatic\begin{itemize}
  \item Generally applicable to any items in any language (e.g., phrases or entities as units)
\end{itemize}
  \item Pure statistical approaches are available for discovering both (can be combined to perform joint analysis).\begin{itemize}
  \item Generally applicable to any text with no human effort
  \item Different ways to define <<context>> and <<segment>> lead to interesting variations of applications
\end{itemize}
\end{itemize}



\subsection{Recommended reading}
\begin{itemize}
\item Chris Manning and Hinrich Schütze, Foundations of Statistical Natural Language Processing, MIT Press. Cambridge, MA: May 1999. (Chapter 5 on collocations)
\item Chengxiang Zhai, Exploiting context to identify lexical atoms: A statistical view of linguistic context. Proceedings of the International and Interdisciplinary Conference on Modelling and Using Context (CONTEXT-97), Rio de Janeiro, Brzil, Feb. 4-6, 1997. pp. 119-129.
\item Shan Jiang and ChengXiang Zhai, Random walks on adjacency graphs for mining lexical relations from big text data. Proceedings of IEEE BigData Conference 2014, pp. 549-554.
\end{itemize}



\section{Topic Mining and Analysis}


% =============================================================================
% =============================================================================
\subsection{Probabilistic topic models}


\subsubsection{Formal Definition of Topic Mining and Analysis}

Topic $\approx$ main idea discussed in text data

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{topic_mining.png}
\end{figure}

\begin{itemize}
  \item Input:
  \begin{itemize}
  \item A collection of $N$ text documents $C=\{d_1, \dots , d_N\}$
  \item Number of topics: $k$
  \end{itemize}
  
  \item Output:
  \begin{itemize}
  \item $k$ topics: $\{\theta_1, \dots , \theta_k \}$
  \item Coverage of topics in each $d_i$: $\{\pi_{i1}, \dots, \pi_{ik} \}$
  \item $\pi_{ij}$ = probability of $d_i$ covering topic $\theta_j \to \forall i: \sum\limits_{j=1}^k \pi_{ij}=1$
  \end{itemize}  
  
  \item Problem
  \begin{itemize}
  \item How to define $\theta_i$?
  \end{itemize}  
\end{itemize}


% =============================================================================
\subsubsection{Initial Idea: Topic = Term}

\begin{itemize}
  \item Parse text in $C$ to obtain candidate terms (e.g., term = word).  \item Design a scoring function to measure how good each term is as a topic.
\begin{itemize}
  \item Favor a representative term (high frequency is favored)
  \item Avoid words that are too frequent (e.g., “the”, “a”).
  \item TF-IDF weighting from retrieval can be very useful.
  \item Domain-specific heuristics are possible (e.g., favor title words, hashtags in tweets).
\end{itemize}  \item Pick $k$ terms with the highest scores but try to minimize redundancy.
\begin{itemize}
  \item If multiple terms are very similar or closely related, pick only one of them and ignore others.\\
\end{itemize}
\end{itemize}

Topic Coverage:
\begin{equation*}
	\pi_{ij} = \frac{count(\theta_j, d_i)}{\sum\limits_{L=1}^k count(\theta_L, d_i)}\\
\end{equation*}

Problems with <<Term as Topic>>\begin{itemize}
  \item Lack of expressive power: can only represent simple/general topics, but can’t represent complicated topics  \item Incompleteness in vocabulary coverage: can’t capture variations of vocabulary (e.g., related words)  \item Word sense ambiguity: a topical term or related term can be ambiguous (e.g., basketball star vs. star in the sky)
\end{itemize}


% =============================================================================
\subsubsection{Improved Idea: Topic = Word Distribution}

\begin{itemize}
  \item Topic = word distribution: $\forall i \sum\limits_{w \in V}p(w \;\big|\; \theta_i) = 1$, where $V$ is vocabulary set $\{w_1, w_2, \dots\}$
  \item The computation task: 
\begin{itemize}
  \item Input: collection of documents $C$, number of topics $k$, vocabulary set $V$
  \item Output: topics $\{\theta_1, \dots , \theta_k \}$, coverage of topics $\{\pi_{i1}, \dots, \pi_{ik} \}$
\end{itemize}

\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{probabilistic.png}
\end{figure}

% =============================================================================
\subsubsection{Generative Model for Text Mining}

Model data generation with a probabilistic model: $P(Data \;\big|\; Model, \Lambda)$:
\begin{equation*}
	\Lambda = \Big(\{\theta_1, \dots, \theta_k\}, \{\pi_{11}, \dots, \pi_{1k}\}, \dots \{\pi_{N1}, \dots, \pi_{Nk}\}\Big)
\end{equation*}

Parameter estimation: infer the most likely parameter values $\Lambda^*$
\begin{equation*}
	\Lambda^* = \argmax_\Lambda P(Data \;\big|\; Model, \Lambda)
\end{equation*}

Take $\Lambda^*$ as the <<knowledge>> to be mined for the text mining problemand adjust the design of the model to discover different knowledge.


% =============================================================================
\subsubsection{The Simplest Language Model: Unigram LM}

\textbf{Language Model} = probability distribution over text = generative model for text data\\

\textbf{Unigram LM} $p(w \;\big|\; \theta)$ = word distribution
\begin{itemize}
  \item Generate text by generating each word independently: $p(w_1 w_2 \dots w_n) = p(w_1) p(w_2) \dots p(w_n)	$
  \item Text = sample drawn according to this word distribution\\
\end{itemize}

\textbf{Likelihood function}: $p(X \,|\, \theta)$\\

\textbf{Maximum likelihood (ML) estimation}:\\<<best>> = <<data likelihood reaches maximum>>:
\begin{equation*}
	\hat{\theta} = \argmax_\theta p(X \;\big|\; \theta)
\end{equation*}


\href{https://en.wikipedia.org/wiki/Bayes_estimator}{\textbf{Bayesian}\footnote{Bayes rule: $p(X \;\big|\; Y) = \dfrac{p(Y \;\big|\; X) p(X)}{p(Y)}$} \textbf{estimation}}:\\<<best>> =\\= <<being consistent with our \textbf{prior knowledge} $p(\theta)$ and explaining data well>> \\ = <<maximize \textbf{a posteriori estimation} $p(\theta \,|\, X) \varpropto p(X \,|\, \theta) p(\theta)$>>:
\begin{equation*}
	\hat{\theta} = \argmax_\theta p(\theta \;\big|\; X) = \argmax_\theta p(X \;\big|\; \theta) p(\theta)
\end{equation*}


Using the mean square error (MSE) as risk, the Bayes estimate of the unknown parameter is simply the mean of the posterior distribution: $\hat{\theta} = \sum\limits_\theta \theta^* p(\theta \,|\, X)$


% =============================================================================
\subsubsection{Mixture Model}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{mixture_model.png}
    \caption{Mixture model: $p(w)=p(\theta_d)p(w \;\big|\; \theta_d) + p(\theta_B)p(w \;\big|\; \theta_B)$}
\end{figure}

Mixture Model parameters:
\begin{equation*}
	\Lambda=\big(\{p(w \;\big|\; \theta_d)\}, \{p(w \;\big|\; \theta_B)\}, p(\theta_B), p(\theta_d)\big)
\end{equation*}
\begin{itemize}
  \item $\theta_d$ - topic of $d$
  \item $\theta_B$ - background topic
  \item $\sum\limits_{i=1}^M p(w_i \;\big|\; \theta_d) = \sum\limits_{i=1}^M p(w_i \;\big|\; \theta_B) = 1$
  \item $p(\theta_d) + p(\theta_B) = 1$\\
\end{itemize}

Likelihood function:
\begin{align*}
	p(d \;\big|\; \Lambda) &= \prod_{i=1}^{|d|}p(x_i \;\big|\; \Lambda) \\
	& = \prod_{i=1}^{|d|} \big[p(\theta_d)p(x_i \;\big|\; \theta_d) + p(\theta_B)p(x_i \;\big|\; \theta_B)\big] \\
	& = \prod_{i=1}^{M} \big[p(\theta_d)p(w_i \;\big|\; \theta_d) + p(\theta_B)p(w_i \;\big|\; \theta_B)\big]^{c(w, d)}\\
\end{align*}


ML estimate:
\begin{equation*}
	\Lambda^* = \argmax_\Lambda p(d \;\big|\; \Lambda)
\end{equation*}


% =============================================================================
\subsubsection{Expectation-Maximization (EM) Algorithm}

\begin{enumerate}
  \item Add hidden variable $z \in \{0, 1\}$:
  \begin{itemize}
  \item $z=0$ means word is from $\theta_d$
  \item $z=1$ means word is from $\theta_B$
  \end{itemize}
  
  \item Initialize $p(w \;\big|\; \theta_d)$ with random values.
  
  \item Iterate through E-step \& M-step:
  \begin{itemize}
  \item E-step: <<augment\footnote{Augment - увеличивать, прибавлять}>> data by predicting values of useful hidden variables
  \begin{equation*}
  	p^{(n)}(z=0 \;\big|\; w) = \frac{p(\theta_d) \; p^{(n)}(w \;\big|\; \theta_d)}{p(\theta_d) \; p^{(n)}(w \;\big|\; \theta_d) + p(\theta_B) \; p(w \;\big|\; \theta_B)}
  \end{equation*}
  \item M-step: exploit the <<augmented data>> to improve estimate of parameters (<<improve>> is guaranteed in terms of likelihood)
  \begin{equation*}
  	p^{(n+1)}(w \;\big|\; \theta_d) = \frac{c(w, d) \; p^{(n)}(z=0 \;\big|\; w)}{\sum\limits_{w' \in V} c(w', d) \; p^{(n)}(z=0 \;\big|\; w')}
  \end{equation*}
  \end{itemize}

  \item Stop when likelihood doesn’t change.
\end{enumerate}



% =============================================================================
\subsubsection{Probabilistic Latent Semantic Analysis (PLSA)}
PLSA = mixture model with $k$ unigram LMs ($k$ topics):
\begin{itemize}
  \item Input: $C, k, V$
  \item Output: $\{\theta_1, \dots, \theta_k \}, \{\pi_{i1}, \dots, \pi_{ik} \}$
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{PLSA.png}
\end{figure}

\begin{gather*}
	p_d(w) = \lambda_B \; p(w \;\big|\; \theta_B) + (1-\lambda_B) \sum_{j=1}^k \pi_{d, j} \; p(w \;\big|\; \theta_j) \\
	\log{p(d)} = \sum_{w \in V} \log\Big[\lambda_B \; p(w \;\big|\; \theta_B) + (1-\lambda_B) \sum_{j=1}^k \pi_{d, j} \; p(w \;\big|\; \theta_j)\Big] \\
	\log p(C \;\big|\; \Lambda) = \sum_{d \in C}\sum_{w \in V} \log\Big[\lambda_B \; p(w \;\big|\; \theta_B) + (1-\lambda_B) \sum_{j=1}^k \pi_{d, j} \; p(w \;\big|\; \theta_j)\Big]
\end{gather*}
\begin{itemize}
  \item $\lambda_b$ - percentage of background words (known)
  \item $p(w \;\big|\; \theta_B)$ - background LM (known)
  \item $\pi_{d, j}$ - coverage of topic $\theta_j$ in document $d$: $\forall d \in C \to \sum_{j=1}^k \pi_{d, j} = 1$
  \item $p(w \;\big|\; \theta_j)$ - probability of word $w$ in topic $\theta_j$: $\forall j \in [1, k] \to \sum_{i=1}^M p(w_i \;\big|\; \theta_j) = 1$
  \item $\Lambda = \big(\{\pi_{d, j}\}, \{\theta_j\}\big), j = 1, \dots, k$ - unknown parameters \\
\end{itemize}

Constrained optimization:
\begin{gather*}
	\Lambda^* = \argmax_\Lambda p(C \;\big|\; \Lambda ) \\
	\sum_{i=1}^M p(w_i \;\big|\; \theta_j) = 1 \quad \forall j \in [1, k] \\
	\sum_{j=1}^k \pi_{d, j} = 1 \quad \forall d \in C
\end{gather*}

EM algorithm for PLSA:
\begin{itemize}
  \item Hidden variable = topic indicator: 
  \begin{equation*}
	z_{d,w} \in \{B, 1, 2, \dots , k\}
  \end{equation*}
  
  \item E-Step:
  \begin{gather*}
  	p( z_{d,w} = j ) = \frac{\pi_{d,j}^{(n)} \; p^{(n)}(w \;\big|\; \theta_j)}{\sum_{j'=1}^k \pi_{d,j'}^{(n)} \; p^{(n)}(w \;\big|\; \theta_{j'})} \\
  	p( z_{d,w} = B ) = \frac{\lambda_B \, p(w \;\big|\; \theta_B)}{\lambda_B \, p(w \;\big|\; \theta_B) + (1 - \lambda_B) \sum_{j'=1}^k \pi_{d,j'}^{(n)} \; p^{(n)}(w \;\big|\; \theta_{j'})}
  \end{gather*} 
  
  \item M-Step:
  \begin{gather*}
  	\pi_{d, j}^{(n+1)} = \frac{\sum_{w \in V} c(w, d) \big(1 - p( z_{d,w} = B )\big) p( z_{d,w} = j )}{\sum_{j'} \sum_{w \in V} c(w, d) \big(1 - p( z_{d,w} = B )\big) p( z_{d,w} = j' )} \\
  	p^{(n+1)} (w \;\big|\; \theta_j) = \frac{\sum_{d \in C} c(w, d) \big(1 - p( z_{d,w} = B )\big) p( z_{d,w} = j )}{\sum_{w' \in V} \sum_{d \in C} c(w', d) \big(1 - p( z_{d,w'} = B )\big) p( z_{d,w'} = j )}
  \end{gather*}

\end{itemize}

Computation of EM algorithm:
\begin{itemize}
  \item Initialize all unknown parameters randomly
  \item Repeat until likelihood converges
  \begin{itemize}
  \item E-step:
  \begin{equation*}
  	\begin{cases}
  		p( z_{d,w} = j ) \varpropto \pi_{d,j}^{(n)} \; p^{(n)}(w \;\big|\; \theta_j) \\
  		p( z_{d,w} = B ) \varpropto \lambda_B \, p(w \;\big|\; \theta_B) \\
  		\sum_{j=1}^k p( z_{d,w} = j ) = 1 \quad \forall d, w
  	\end{cases}
  \end{equation*}
  
  \item M-step:
  \begin{equation*}
  	\begin{cases}
  		\pi_{d, j}^{(n+1)} \varpropto \sum_{w \in V} c(w, d) \big(1 - p( z_{d,w} = B )\big) p( z_{d,w} = j ) \\
  		p^{(n+1)} (w \;\big|\; \theta_j) \varpropto \sum_{d \in C} c(w, d) \big(1 - p( z_{d,w} = B )\big) p( z_{d,w} = j ) \\
  		\sum_{j=1}^k \pi_{d, j} = 1 \quad \forall d \in C \\
  		\sum_{i=1}^M p(w_i \;\big|\; \theta_j) = 1 \quad \forall j \in [1, k]		
  	\end{cases}
  \end{equation*}
  \end{itemize}

\end{itemize}


% =============================================================================
\subsubsection{PLSA with Prior Knowledge}

% PLSA with prior knowledge = User-controlled PLSA

Maximum a Posteriori (MAP) Estimate:
\begin{equation*}
	\Lambda^* = \argmax_\Lambda p(\Lambda) p(Data \;\big|\; \Lambda )
\end{equation*}

We may use $p(\Lambda)$ to encode all kinds of preferences and constraints, e.g.,\begin{itemize}
  \item $p(\Lambda)>0$ if and only if one topic is precisely <<background>>: $p(w \;\big|\; \theta_B)$
  \item $p(\Lambda)>0$ if and only if for a particular doc $d$, $\pi_{d,3}=0$ and $\pi_{d,1}=1/2$
  \item $p(\Lambda)$ favors a $\Lambda$ with topics that assign high probabilities to some particular words \\
\end{itemize}

The MAP estimate (with conjugate prior) can be computed using a similar EM algorithm to the ML estimate with smoothing to reflect prior preferences



% =============================================================================
\subsubsection{Latent Dirichlet Allocation (LDA)}

Make PLSA a generative model by imposing a Dirichlet\footnote{The \href{https://en.wikipedia.org/wiki/Dirichlet_distribution}{Dirichlet distribution} of order $K \geqslant 2$ with parameters $\alpha_1, \dots, \alpha_K > 0$ has a probability density function %with respect to Lebesgue measure on the Euclidean space $\mathbf{R}^{K−1}$ given by
\begin{equation*}
f \left(x_1,\dots, x_{K-1}; \alpha_1,\dots, \alpha_K \right) = \frac{1}{\mathrm{B}(\alpha)} \prod_{i=1}^K x_i^{\alpha_i - 1},
\end{equation*}
%on the open $(K − 1)$-dimensional simplex defined by:
%\begin{gather*} 
%x_1, \dots, x_{K-1} > 0 \\
%x_1 + \dots + x_{K-1} < 1 \\ 
%x_K = 1 - x_1 - \dots - x_{K-1} 
%\end{gather*}
%and zero elsewhere.
} prior on the model parameters:
\begin{itemize}
  \item $p(\vec{\pi}_d) = Dirichlet(\vec{\alpha})$
  \item $p(\vec{\theta}_i) = Dirichlet(\vec{\beta})$
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{LDA.png}
\end{figure}


Likelihood Functions for PLSA vs. LDA:
\begin{itemize}
  \item \textcolor{red}{PLSA}:
  \begin{align*}
  	& p_d(w \;\big|\; \{\theta_j\}, \{\pi_{d,j}\}) = \textcolor{brown}{\sum_{j=1}^k \pi_{d,j} \; p(w \;\big|\; \theta_j)} \\
  	& \log p_d(d \;\big|\; \{\theta_j\}, \{\pi_{d,j}\}) = \textcolor{red}{\sum_{w \in V} c(w, d) \log\left[ \sum_{j=1}^k \pi_{d,j} \; p(w \;\big|\; \theta_j) \right]} \\
  	& \log p_d(C \;\big|\; \{\theta_j\}, \{\pi_{d,j}\}) = \sum_{d \in C} \log p_d(d \;\big|\; \{\theta_j\}, \{\pi_{d,j}\})
  \end{align*}
  \item \textcolor{blue}{LDA}:
  \begin{align*}
  	& p_d(w \;\big|\; \{\theta_j\}, \{\pi_{d,j}\}) = \textcolor{brown}{\sum_{j=1}^k \pi_{d,j} \; p(w \;\big|\; \theta_j)} \\
  	& \log p_d(d \;\big|\; \vec{\alpha}, \{\theta_j\}) = \int \textcolor{red}{\sum_{w \in V} c(w, d) \log\left[ \sum_{j=1}^k \pi_{d,j} \; p(w \;\big|\; \theta_j) \right]} \textcolor{blue}{p(\vec{\pi}_d \;\big|\; \vec{\alpha}) \; d \vec{\pi}_d}\\
  	& \log p_d(C \;\big|\; \vec{\alpha}, \vec{\beta}) = \int \sum_{d \in C} \log p_d(d \;\big|\; \vec{\alpha}, \{\theta_j\}) \textcolor{blue}{\prod_{j=1}^k p(\theta_j \;\big|\; \vec{\beta}) \; d\theta_1 \dots d\theta_k}
  \end{align*}
\end{itemize}

Parameters can be estimated using ML estimator:
\begin{equation*}
	(\vec{\alpha}, \vec{\beta}) = \argmax_{\vec{\alpha}, \vec{\beta}} p(C \;\big|\; \vec{\alpha}, \vec{\beta})
\end{equation*}


% =============================================================================
\subsubsection{Summary}

\begin{itemize}
  \item Probabilistic topic models provide a general principled way of mining and analyzing topics in text with many applications
  \item Basic task setup:
    \begin{itemize}
  \item Input: Text data
  \item Output: $k$ topics + proportions of these topics covered in each document
  \end{itemize}
  \item PLSA is the basic topic model, often adequate for most applications  \item LDA improves over PLSA by imposing priors

  \begin{itemize}
  \item Theoretically more appealing
  \item Practically, LDA and PLSA perform similarly for many tasks
  \end{itemize}

\end{itemize}


% =============================================================================
% =============================================================================
% =============================================================================


\subsection{Text Clustering}

Text clustering is an unsupervised general text mining technique to 
\begin{itemize}
  \item obtain an overall picture of the text content (exploring text data)
  \item discover interesting clustering structures in text data
\end{itemize}


\subsubsection{Generative Probabilistic Model}

\begin{itemize}
  \item Each cluster is represented by a unigram LM $p(w \;\big|\; \theta_i) \to$ term cluster
  \item A document is generated by first choosing a unigram LM and then generating \textbf{ALL words} in the document using this \textbf{single LM}
  \item Estimated model parameters give both a topic characterization of each cluster and a probabilistic assignment of a document into each cluster
  \item <<Hard>> clusters can be obtained by forcing a document into the cluster corresponding to the unigram LM most likely used to generate the document
\end{itemize}


\textbf{Mixture Model for Document Clustering:}

\begin{itemize}
  \item Data: a collection of documents $C=\{d_1, \dots, d_N\}$
  
  \item Model: mixture of $k$ unigram LMs:
  \begin{equation*}
  	\Lambda=(\{\theta_i\}; \{p(\theta_i)\}), i \in [1,k]
  \end{equation*}
  
  \item Likelihood. To generate a document, first choose a $\theta_i$ according to $p(\theta_i)$, and then generate all words in the document using $p(w \;\big|\; \theta_i)$:
  \begin{equation*}
  	p(d \;\big|\; \Lambda) = \sum_{i=1}^k \left[ p(\theta_i) \prod_{j=1}^{|d|} p(x_j \;\big|\; \theta_i) \right] = \sum_{i=1}^k \left[ p(\theta_i) \prod_{w \in V} p(w \;\big|\; \theta_i)^{c(w, d)} \right]
  \end{equation*}
  
  \item Maximum Likelihood estimate
  \begin{equation*}
  	\Lambda^*=\argmax_\Lambda p(d \;\big|\; \Lambda)
  \end{equation*}
\end{itemize}

\textbf{Cluster Allocation After Parameter Estimation:}\begin{itemize}
  \item Parameters of the mixture model: $\Lambda=(\{\theta_i\}; \{p(\theta_i)\}), i \in [1,k]$

  \begin{itemize}
  \item Each $\theta_i$ represents the content of cluster $i$: $p(w \;\big|\; \theta_i)$
  \item $p(\theta_i)$ indicates the size of cluster $i$
  \item Note that unlike in PLSA, $p(\theta_i)$ doesn’t depend on $d$!
  \end{itemize}

  \item Which cluster should document $d$ belong to? $c_d=?$

  \begin{itemize}
  \item \textbf{Likelihood only}: Assign $d$ to the cluster corresponding to the topic $\theta_i$ that most likely has been used to generate $d$: 
  \begin{equation*}
  	c_d = \argmax_i p(\theta_i \;\big|\; d)
  \end{equation*}
  \item \textbf{Likelihood + prior $p(\theta_i)$ (Bayesian)}: favor large clusters
  \begin{equation*}
  	c_d = \argmax_i p(d \;\big|\; \theta_i) \; p(\theta_i)
  \end{equation*}
  \end{itemize}
	
\end{itemize}

\textbf{ML Estimate:}
\begin{itemize}
  \item Likelihood:
  \begin{align*}
  	&p(d \;\big|\; \Lambda) = \sum_{i=1}^k \left[ p(\theta_i) \prod_{w \in V} p(w \;\big|\; \theta_i)^{c(w, d)} \right] \\
  	&p(C \;\big|\; \Lambda) = \prod_{j=1}^{N} p(d_j \;\big|\; \Lambda)
  \end{align*}
  
  \item Maximum Likelihood estimate
  \begin{equation*}
  	\Lambda^*=\argmax_\Lambda p(C \;\big|\; \Lambda)
  \end{equation*}
\end{itemize}

\textbf{EM Algorithm for Document Clustering}:
\begin{itemize}
  \item Initialization: randomly set $\Lambda=(\{\theta_i\}; \{p(\theta_i)\}), i \in [1,k]$
  \item Repeat until likelihood $p(C \;\big|\; \Lambda)$ converges:
  
  \begin{itemize}
  \item E-Step: Infer which distribution has been used to generate document $d$: hidden variable $Z_d \in [1, k]$
  \begin{eqnarray*}
  	\begin{cases}
  		p^{(n)}(Z_d = i \;\big|\; d) \varpropto p^{(n)}(\theta_i) \prod_{w \in V} p^{(n)}(w \;\big|\; \theta_i)^{c(w, d)} \\
  		\sum\limits_{i=1}^k p^{(n)}(Z_d = i \;\big|\; d) = 1
  	\end{cases}
  \end{eqnarray*}
  
  \item M-Step: Re-estimation of all parameters:
  \begin{eqnarray*}
  	\begin{cases}
  		p^{(n+1)}(\theta_j) \varpropto \sum\limits_{j=1}^N p^{(n)}(Z_d = i \;\big|\; d) \\
  		\sum\limits_{i=1}^k p^{(n+1)}(\theta_i) = 1 \\
  		p^{(n+1)}(w \;\big|\; \theta_i) \varpropto \sum\limits_{j=1}^N c(w, d_j) p^{(n)}(Z_d = i \;\big|\; d) \\
  		\sum\limits_{w \in V} p^{(n+1)}(w \;\big|\; \theta_i) = 1 \quad \forall i \in [1, k]
  	\end{cases}
  \end{eqnarray*}
  \end{itemize}

\end{itemize}


% =============================================================================
\subsubsection{Similarity-based Clustering}

\begin{itemize}
  \item Explicitly define a similarity function to measure similarity between two text objects (i.e., providing <<clustering bias>>)
  \item Find an optimal partitioning of data to 
  
  \begin{itemize}
  \item maximize intra-group similarity
  \item minimize inter-group similarity
  \end{itemize}

  \item Two strategies for obtaining optimal clustering  
  \begin{itemize}
  \item Progressively construct a hierarchy of clusters (hierarchical clustering)
  \item Start with an initial tentative clustering and iteratively improve it (<<flat>> clustering, e.g., k-Means)
  \end{itemize}

\end{itemize}


\subsubsection{Recommended reading}
\begin{itemize}
  \item Manning, Chris D., Prabhakar Raghavan, and Hinrich Schütze. Introduction to Information Retrieval. Cambridge: Cambridge University Press, 2007. (Chapter 16)
\end{itemize}



% =============================================================================
% =============================================================================
% =============================================================================


\subsection{Text Categorization}

Categorization (classification) is the problem of identifying to which of a set of categories (sub-populations) a new observation belongs, on the basis of a training set of data containing observations whose category membership is known.

\subsubsection{Categorization Methods}

\textbf{Categorization variants:}
\begin{itemize}
  \item Binary categorization: only two categories. But can potentially support all other categorizations!
\begin{itemize}
  \item Retrieval: {relevant-doc, non-relevant-doc}
  \item Spam filtering: {spam, non-spam} 
  \item Opinion: {positive, negative}
\end{itemize}
  \item K-category categorization: more than two categories 
\begin{itemize}
  \item Topic categorization: {sports, science, travel, business,...}
  \item Email routing: {folder1, folder2, folder3,...}
\end{itemize}
  \item Hierarchical categorization:Categories form a hierarchy  \item Joint categorization: Multiple related categorization tasks done in a joint manner \\
\end{itemize}


\textbf{Machine Learning for Text Categorization:}\begin{itemize}
  \item General setup: Learn a classifier $f: X \to Y$\begin{itemize}
  \item Input: $X$ = all text objects; Output: $Y$ = all categories
  \item Learn a classifier function, $f: X \to Y$, such that $f(x)=y \in Y$ gives the correct category for $x \in X$ (<<correct>> is based on the training data)
\end{itemize}

  \item All methods:\begin{itemize}
  \item Rely on discriminative features of text objects to distinguish categories
  \item Combine multiple features in a weighted manner
  \item Adjust weights on features to minimize errors on the training data
\end{itemize}
  \item Different methods tend to vary in\begin{itemize}
  \item Their way of measuring the errors on the training data (may optimize a different objective/loss/cost function)
  \item Their way of combining features (e.g., linear vs. non-linear)\\
\end{itemize}

\end{itemize}

\textbf{Generative vs. Discriminative Classifiers}
\begin{itemize}
  \item Generative classifiers (learn what the data <<looks>> like in each category)\begin{itemize}
  \item Attempt to model $p(X,Y) = p(Y) \, p(X \mid Y)$ and compute $p(Y \mid X)$ based on $p(X \mid Y)$ and $p(Y)$ by using Bayes Rule
  \item Objective function is likelihood, thus indirectly measuring training errors
  \item E.g., Naïve Bayes
\end{itemize}
  \item Discriminative classifiers (learn what features separate categories)\begin{itemize}
  \item Attempt to model $p(Y \mid X)$ directly
  \item Objective function directly measures errors of categorization on training data
  \item E.g., Logistic Regression, Support Vector Machine (SVM), k-Nearest Neighbors (kNN)
\end{itemize}

\end{itemize}


% =============================================================================
\subsubsection{Generative Probabilistic Models}

Document Clustering Revisited:
\begin{align*}
	cluster(d) &= \argmax_i p(\theta_i \;\big|\; d) \\
	& = \argmax_i p(d \;\big|\; \theta_i) \; p(\theta_i) \\
	& = \argmax_i \prod_{w \in V} p(w \;\big|\; \theta_i)^{c(w, d)} \; p(\theta_i)
\end{align*}

If $\theta_i$ represents category $i$ accurately, then
\begin{align*}
	category(d)
	& = \argmax_i \prod_{w \in V} p(w \;\big|\; \theta_i)^{c(w, d)} \; p(\theta_i) \\
	& = \argmax_i \left[ \log p(\theta_i) + \sum_{w \in V} c(w, d) \; p(w \;\big|\; \theta_i) \right] 
\end{align*}

\pagebreak
\textbf{Naïve Bayes Classifier}
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{train_category.png}
\end{figure}

Learn $p(w \;\big|\; \theta_i)$ and $\theta_i$ from the training data:
\begin{gather*}
	p(\theta_i) = \frac{N_i}{\sum_{j=1}^k N_j} \varpropto |T_i| \\
	p(w \;\big|\; \theta_i) = \frac{\sum_{j=1}^{N_i} c(w, d_{ij})}{\sum_{w \in V}\sum_{j=1}^{N_i} c(w', d_{ij})} \varpropto c(w, T_i) \\
\end{gather*}

\textbf{Smoothing in Naïve Bayes}
\begin{gather*}
	p(\theta_i) = \frac{N_i + \delta}{\sum_{j=1}^k N_j + k\delta} \\
	p(w \;\big|\; \theta_i) = \frac{\sum_{j=1}^{N_i} c(w, d_{ij}) + \mu \; p(w \;\big|\; \theta_B)}{\sum_{w \in V}\sum_{j=1}^{N_i} c(w', d_{ij}) + \mu}\\
\end{gather*}
where $\delta \geqslant 0$, $\mu \geqslant 0$, $p(w \;\big|\; \theta_B)$ - background LM

% =============================================================================
\subsubsection{Discriminative Classifiers}

\textbf{Logistic Regression}
\begin{itemize}
  \item Predictors $X = (x_1, x_2, \dots, x_M)$, $x_i \in \Re$
  \item Binary response variable: $Y \in \{0,1\} \Leftrightarrow category(d) = \theta_1 \vee \theta_2$
\end{itemize}

\begin{gather*}
	\log \frac{p(\theta_1 \;\big|\; d)}{p(\theta_2 \;\big|\; d)} = \frac{p(Y=1 \;\big|\; X)}{1-p(Y=1) p(Y=1 \;\big|\; X)} = \beta_0 + \sum_{i=1}^M x_i \beta_i \\
	\Rightarrow p(Y=1 \;\big|\; X) = \frac{e^{\beta_0 + \sum_{i=1}^M x_i \beta_i}}{e^{\beta_0 + \sum_{i=1}^M x_i \beta_i}+1}, \quad p(Y=0 \;\big|\; X) = \frac{1}{e^{\beta_0 + \sum_{i=1}^M x_i \beta_i}+1}
\end{gather*}

Estimation of parameters:
\begin{itemize}
  \item Training Data: $T=\{(X_i, Y_i)\}, i=1,2, \dots , |T|$
  \item Parameters: $\vec{\beta} = (\beta_0, \beta_1, \dots, \beta_M)$
  \item Conditional likehood: $p(T \;\big|\; \vec{\beta}) = \prod_{i=1}^{|T|} p(Y=Y_i \;\big|\; X=X_i, \;\vec{\beta})$
  \item Maximum likelihood estimate: $\vec{\beta}^* = \argmax_{\vec{\beta}} p(T \;\big|\; \vec{\beta})$\\
\end{itemize}


\textbf{K-Nearest Neighbors (K-NN)}
\begin{itemize}
  \item Find k examples in the training set that are most similar to the text object to be classified (<<neighbor>> documents)
  \item Assign the category that is most common in these neighbor text objects (neighbors vote for the category)
  \item Can be improved by considering the distance of a neighbor (a closer neighbor has more influence)
  \item Can be regarded as a way to directly estimate the conditional probability of label given data instance, i.e., $p(Y \mid X)$
  \item Need a similarity function to measure similarity of two text objects\\
\end{itemize}


\textbf{Support Vector Machine (SVM)}

\begin{itemize}
  \item Consider two categories: $\{\theta_1, \theta_2\}$
  
  \item Use a linear separator $f(X)$:
  \begin{itemize}
  \item $f(X) \geqslant 0 \Rightarrow X$ is in category $\theta_1$
  \item $f(X) < 0 \Rightarrow X$ is in category $\theta_2$
  \end{itemize}

  \item Best separator = maximize the margin
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{SVM.png}
\end{figure}

\begin{gather*}
	w = \begin{pmatrix}
		w_1 & w_2 & \dots & w_M
	\end{pmatrix}^T\mbox{ - feature weights} \\
	x = \begin{pmatrix}
		x_1 & x_2 & \dots & x_M
	\end{pmatrix}^T\mbox{ - feature vector (e.g. word counts)} \\
\end{gather*}

Classifier $f(\mathbf{x}) = \mathbf{w}^T\mathbf{x}+b$:
\begin{itemize}
  \item $\mathbf{w}^T\mathbf{x}+b \geqslant 1 \Rightarrow y_i = 1 \Leftrightarrow \mathbf{x}$ is in category $\theta_1$
  \item $\mathbf{w}^T\mathbf{x}+b \leqslant -1 \Rightarrow y_i = -1 \Leftrightarrow \mathbf{x}$ is in category $\theta_2$
\end{itemize}

\textbf{Linear SVM}: minimize $\Phi(\mathbf{w}) = \mathbf{w}\mathbf{w}^T$ using constraint $y_i (\mathbf{w}^T\mathbf{x}+b) \geqslant 1 \;\; \forall i$. The optimization problem is quadratic programming with linear constraints.\\

\textbf{Linear SVM with soft margin}: minimize $\Phi(\mathbf{w}) = \mathbf{w}\mathbf{w}^T + C\sum_{i \in [1, |T|]}\xi_i$ using constraint $y_i (\mathbf{w}^T\mathbf{x}+b) \geqslant 1-\xi_i, \; \xi_i \geqslant 0 \;\; \forall i \in [1, |T|]$. The optimization problem is still quadratic programming with linear constraints.

% =============================================================================
\subsubsection{Summary}
\begin{itemize}
  \item Many methods are available, but no clear winner\begin{itemize}
  \item All require effective feature representation (need domain knowledge)
  \item It is useful to compare/combine multiple methods for a particular problem
\end{itemize}
  \item Most techniques rely on supervised machine learning and thus can be applied to any text categorization problem!\begin{itemize}
  \item Humans annotate training data and design features
  \item Computer optimizes the combination of features
  \item Good performance requires 1) effective features and 2) plenty of training data
  \item Performance is generally (much) more affected by the effectiveness of features than by the choice of a specific classifier
\end{itemize}
\end{itemize}

% =============================================================================
\subsubsection{Recommended readings}
\begin{itemize}
  \item Manning, Chris D., Prabhakar Raghavan, and Hinrich Schütze. Introduction to Information Retrieval. Cambridge: Cambridge University Press, 2007. (Chapters 13-15)
\end{itemize}


% =============================================================================
\subsubsection{Evaluation}

\textbf{Classification Accuracy (Percentage of Correct Decisions)}
\begin{align*}
	\mbox{Classification Accuracy} &= \frac{\mbox{Total number of correct decisions}}{\mbox{Total number of decisions made}} \\
	&= \frac{\mbox{count(y(+)) + count(n(-))}}{\mbox{count(y(+)) + count(y(-)) + count(n(+)) + count(n(-))}} \\
	&= \frac{TP + TN}{TP + FP + FN + TP}, 
\end{align*}
\begin{itemize}
  \item +/- is a human answer
  \item y/n is system result
\end{itemize}

Problems with Classification Accuracy:
\begin{itemize}
  \item It may be more important to get the decisions right on some documents than others
  \item It may be more important to get the decisions right on some categories than others
  \item E.g., spam filtering: missing a legitimate email costs more than letting a spam go \\
\end{itemize}


\textbf{Per-Document \& Per-Category Evaluation + Micro-Averaging}
\begin{gather*}
	\mbox{Precision} = P = \frac{TP}{TP + FP} \\
	\mbox{Recall} = R =\frac{TP}{TP + FN} \\
\end{gather*}


\textbf{Combine Precision and Recall: F-Measure}
\begin{gather*}
	F_\beta = \frac{1}{\dfrac{\beta^2}{\beta^2+1}\dfrac{1}{R} + \dfrac{1}{\beta^2+1}\dfrac{1}{P}} = \frac{(\beta^2+1)\cdot P \cdot R}{\beta^2 P + R} \\
	F_1 = \frac{2\cdot P \cdot R}{P+R} \\
\end{gather*}


\textbf{(Macro) Average Over All the Categories}
\begin{enumerate}
	\item Calculate precision, recall, F-score for every category
	\item Aggregate precision, recall, F-score
\end{enumerate}


\textbf{(Macro) Average Over All the Documents}
\begin{enumerate}
	\item Calculate precision, recall, F-score for every document
	\item Aggregate precision, recall, F-score
\end{enumerate}

\textbf{Suggested Reading}
\begin{itemize}
  \item Manning, Chris D., Prabhakar Raghavan, and Hinrich Schütze. Introduction to Information Retrieval. Cambridge: Cambridge University Press, 2007. (Chapters 13-15)
  \item Yang, Yiming. 1999. An Evaluation of Statistical Approaches to Text Categorization. Inf. Retr. 1, 1-2 (May 1999), 69-90. DOI=10.1023/A:1009982220290
\end{itemize}




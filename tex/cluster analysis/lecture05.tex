\newpage
\section{Density-Based and Grid-Based Clustering Methods} 

%-------
\subsection{Basic Concepts of Density-Based Clustering}

Major features:
\begin{itemize}
  \item Discover clusters of arbitrary shape
  \item Handle noise
  \item One scan (only examine the local region to justify density) 
  \item Need density parameters as termination condition
\end{itemize}
Several interesting studies:\begin{itemize}
  \item \textbf{DBSCAN}: Ester, et al. (KDD'96)
  \item \textbf{OPTICS}: Ankerst, et al (SIGMOD'99)
  \item \textbf{DENCLUE}: Hinneburg \& D. Keim (KDD'98)
  \item \textbf{CLIQUE}: Agrawal, et al. (SIGMOD'98) (also, grid-based)
\end{itemize}


%-------
\subsection{DBSCAN: A Density-Based Clustering Algorithm}

\textbf{DBSCAN} (M. Ester, H.-P. Kriegel, J. Sander, and X. Xu, KDD'96) - Density-Based Spatial Clustering of Applications with Noise \\

A \textbf{cluster} is defined as a maximal set of density-connected points based on two parameters:
\begin{itemize}
  \item $\varepsilon$ - maximum radius of the neighborhood
  \item $MinPts$ - minimum number of points in the $\varepsilon$-neighborhood of a point:
\begin{equation*}
	N_{\varepsilon}(q): \{p\mbox{ belongs to }D \:\big|\: dist(p, q) \leqslant \varepsilon\}
\end{equation*}
\end{itemize}

\begin{figure}[H]\centering\subfigure{  \includegraphics[width=0.37\linewidth]{DBSCAN_def1.png}
}\quad\subfigure{  \includegraphics[width=0.53\linewidth]{DBSCAN_def2.png}
}\caption{DBSCAN}\end{figure}



\newpage
\begin{wrapfigure}{r}{0.47\textwidth} 
	\centering
	\vspace{-30pt}
    \includegraphics[width=0.47\textwidth]{DBSCAN_def.png}
    \vspace{-40pt}
\end{wrapfigure}

\subsubsection{Definitions}

A point $p$ is \textbf{directly density-reachable} from a point $q$ w.r.t. $\varepsilon, MinPts$ if\begin{itemize}
  \item $p$ belongs to $N_{\varepsilon}(q)$
  \item core point condition: $|N_{\varepsilon}(q)| \geqslant MinPts$\\
\end{itemize}

A point $p$ is \textbf{density-reachable} from a point $q$ w.r.t. $\varepsilon, MinPts$ if there is a chain of points $p_1, \dots, p_n: p_1 = q, p_n = p$ such that $p_i+1$ is directly density-reachable from $p_i$\\

A point $p$ is \textbf{density-connected} to a point $q$ w.r.t. $\varepsilon, MinPts$ if there is a point $o$ such that both $p$ and $q$ are density-reachable from $o$ w.r.t. $\varepsilon, MinPts$

\subsubsection{The Algorithm}
\begin{itemize}
  \item Arbitrarily select a point $p$
  \item Retrieve all points density-reachable from $p$ w.r.t. $\varepsilon, MinPts$
 
\begin{itemize}
  \item If $p$ is a core point, a cluster is formed
  \item If $p$ is a border point, no points are density-reachable from $p$, and DBSCAN visits the next point of the database
\end{itemize}

  \item Continue the process until all of the points have been processed
\end{itemize}

Computational complexity\begin{itemize}
  \item If a spatial index is used, the computational complexity of DBSCAN is $O(n \log n)$, where $n$ is the number of database objects
  \item Otherwise, the complexity is $O(n^2)$
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{DBSCAN_parameters.png}
    \caption{DBSCAN Is Sensitive to the Setting of Parameters}
\end{figure}


%-------
\subsection{OPTICS: Ordering Points To Identify Clustering Structure}

\textbf{OPTICS} (Ankerst, Breunig, Kriegel, and Sander, SIGMOD'99) is an extension to DBSCAN.\\

Given a $MinPts$, density-based clusters w.r.t. a higher density are completely contained in clusters w.r.t. to a lower density. Idea: higher density points should be processed first—find high-density clusters first. OPTICS stores such a clustering order using two pieces of information: core distance and reachability distance.\\

\textbf{Core distance} of an object $p$: the smallest value $\varepsilon$ such that the $\varepsilon$-neighborhood of $p$ has at least $MinPts$ objects.\\

\textbf{Reachability distance} of object $p$ from core object $q$ is the minimum radius value that makes $p$ density-reachable from $q$.



\subsubsection{The Algorithm}
Loop through all the objects. If object is not processed then \textbf{expand it}:

  \begin{itemize}
  	\item find $\varepsilon$-neighbors
  	\item object is marked as processed
  	\item object reachability-distance is set to UNDEFINED
  	\item object core distance is calculated
  	\item object is written to file
  	\item if object core distance $\neq$ UNDEFINED:
    \begin{itemize}
  		\item iteratively collect directly density-reachable objects with respect to $\varepsilon, MinPts$
  		\item objects are inserted into the seed-list OrderSeeds for further expansion
  		\item the objects from OrderSeeds are sorted by their reachability-distance to the closest core object from which they have been directly density-reachable
  		\item objects from OrderSeeds are marked as processed and are written to file\\
	\end{itemize}
  \end{itemize}
  
\begin{wrapfigure}{r}{0.47\textwidth} 
	\centering
    \includegraphics[width=0.47\textwidth]{OPTICS.png}
    \vspace{-50pt}
\end{wrapfigure}  

OPTICS produces a special cluster-ordering of the data points with respect to its density-based clustering structure:\begin{itemize}
  \item The cluster-ordering contains information equivalent to the density-based clusterings corresponding to a broad range of parameter settings
  \item Good for both automatic and interactive cluster analysis—finding intrinsic, even hierarchically nested clustering structures
\end{itemize}



%-------
\subsection{Grid-Based Clustering Methods}

Grid-Based Clustering: explore multi-resolution grid data structure in clustering
\begin{itemize}
  \item Partition the data space into a finite number of cells to form a grid structure
  \item Find clusters (dense regions) from the cells in the grid structure\\
\end{itemize}

Features and challenges of a typical grid-based algorithm\begin{itemize}
  \item Efficiency and scalability: number of cells $\ll$ number of data points
  \item Uniformity: Uniform, hard to handle highly irregular data distributions
  \item Locality: Limited by predefined cell sizes, borders, and the density threshold
  \item Curse of dimensionality: Hard to cluster high-dimensional data
\end{itemize}


%-------
\subsection{STING: A Statistical Information Grid Approach}

\textbf{STING} (Statistical Information Grid) (Wang, Yang and Muntz, VLDB'97)

\begin{itemize}
  \item The spatial area is divided into rectangular cells at different levels of resolution, and these cells form a tree structure
  \item A cell at a high level contains a number of smaller cells of the next lower level
  \item Statistical information of each cell is calculated and stored beforehand and is used to answer queries
  \item Parameters of higher level cells can be easily calculated from that of lower level cell, including\begin{itemize}
  \item count, mean, $\sigma$ (standard deviation), min, max
  \item type of distribution: normal, uniform, etc.
\end{itemize}
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{STING.png}
\end{figure}

To process a region query:\begin{itemize}
  \item Start at the root and proceed to the next lower level, using the STING index
  \item Calculate the likelihood that a cell is relevant to the query at some confidence level using the statistical information of the cell
  \item Only children of likely relevant cells are recursively explored
  \item Repeat this process until the bottom layer is reached
\end{itemize}


Advantages:
\begin{itemize}
  \item Query-independent, easy to parallelize, incremental update
  \item Efficiency: Complexity is $O(K)$, where $K$ is the number of grid cells at the lowest level, and $K \ll N$ (i.e., the number of data points) 
\end{itemize}


Disadvantages:\begin{itemize}
  \item Its probabilistic nature may imply a loss of accuracy in query processing
\end{itemize}




%-------
\subsection{CLIQUE: Grid-Based Subspace Clustering}

\textbf{CLIQUE} (Clustering In QUEst) (Agrawal, Gehrke, Gunopulos, Raghavan: SIGMOD'98)\\

CLIQUE is a density-based and grid-based subspace clustering algorithm. It automatically identifies subspaces of a high dimensional data space that allow better clustering than original space using the Apriori principle.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{CLIQUE.png}
    \caption{CLIQUE}
\end{figure}

\begin{itemize}
  \item Start at 1-D space and discretize numerical intervals in each axis into grid
  \item Find dense regions (clusters) in each subspace and generate their minimal descriptions
  \item Use the dense regions to find promising candidates in 2-D space based on the Apriori principle
  \item Repeat the above in level-wise manner in higher dimensional subspaces
\end{itemize}


\subsubsection{Major Steps of the CLIQUE Algorithm}

Identify subspaces that contain clusters:\begin{itemize}
  \item Partition the data space and find the number of points that lie inside each cell of the partition
  \item Identify the subspaces that contain clusters using the Apriori principle\\
\end{itemize}

Identify clusters:
\begin{itemize}
  \item Determine dense units in all subspaces of interests
  \item Determine connected dense units in all subspaces of interests \\
\end{itemize}

Generate minimal descriptions for the clusters:\begin{itemize}
  \item Determine maximal regions that cover a cluster of connected dense units for each cluster
  \item Determine minimal cover for each cluster
\end{itemize}


\subsubsection{Additional Comments on CLIQUE}

Strengths:\begin{itemize}
  \item Automatically finds subspaces of the highest dimensionality as long as high density clusters exist in those subspaces
  \item Insensitive to the order of records in input and does not presume some canonical data distribution
  \item Scales linearly with the size of input and has good scalability as the number of dimensions in the data increases
\end{itemize}


Weaknesses:\begin{itemize}
  \item As in all grid-based clustering approaches, the quality of the results crucially depends on the appropriate choice of the number and width of the partitions and grid cells
\end{itemize}



%-------
\subsection{Recommended Readings}
\begin{itemize}
  \item M. Ester, H.-P. Kriegel, J. Sander, and X. Xu. A Density-Based Algorithm for Discovering Clusters in Large Spatial Databases. KDD'96
  \item W. Wang, J. Yang, R. Muntz, STING: A Statistical Information Grid Approach to Spatial Data Mining, VLDB'97
  \item R. Agrawal, J. Gehrke, D. Gunopulos, and P. Raghavan. Automatic Subspace Clustering of High Dimensional Data for Data Mining Applications. SIGMOD'98
  \item A. Hinneburg and D. A. Keim. An Efficient Approach to Clustering in Large Multimedia Databases with Noise. KDD’98
  \item M. Ankerst, M. M. Breunig, H.-P. Kriegel, and J. Sander. Optics: Ordering Points to Identify the Clustering Structure. SIGMOD'99
  \item M. Ester. Density-Based Clustering. In (Chapter 5) Aggarwal and Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press. 2014
  \item W. Cheng, W. Wang, and S. Batista. Grid-based Clustering. In (Chapter 6) Aggarwal and Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press. 2014
\end{itemize}

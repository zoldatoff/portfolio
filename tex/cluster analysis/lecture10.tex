\newpage
\section{Clustering Graphs and Networked Data}

%-----------
\subsection{Graphs, Networks, and Their Representations}

A \textbf{graph} is an ordered pair $G = (\mathcal{V}, \mathcal{E})$ comprising a set $\mathcal{V}$ of vertices or nodes together with a set $\mathcal{E}$ of edges or links, which are 2-element subsets of $\mathcal{V}$ (i.e., an edge is related with two vertices, and the relation is represented as an unordered pair of the vertices with respect to the particular edge). To avoid ambiguity, this type of graph may be described precisely as undirected and simple.\\

The \textbf{order} of a graph is $|\mathcal{V}|$ (the number of vertices). A graph's \textbf{size} is $|\mathcal{E}|$, the number of edges. The \textbf{degree} of a vertex is the number of edges that connect to it, where an edge that connects to the vertex at both ends (a loop) is counted twice.\\

A \textbf{simple} graph is an undirected graph that has no loops (edges connected at both ends to the same vertex) and no more than one edge between any two different vertices. \textbf{Multiple edges} (also called parallel edges or a multi-edge), are two or more edges that are incident to the same two vertices.\\

\textbf{Adjacency matrix}: $A_{ij} = 1$ if there is an edge between vertices $i$ and $j$; $0$ otherwise. \\

\textbf{Directed graph} (digraph) if each edge has a direction (tail $\to$ head): $A_{ij} = 1$ if there is an edge from $j$ to $i$; $0$ otherwise. In directed graph \textit{in-degree} of a vertex $d_{in}(v_i)$ is a number of edges pointing to $v_i$; \textit{out-degree} of a vertex is a number of edges pointing from $v_i$.\\

\textbf{Weighted graph}: If a weight $w_{ij}$ is associated with each edge $e_{ij}$


%-----------
\subsection{Typical Evaluation Measures}

We are given a graph $G = (\mathcal{V}, \mathcal{E}, A)$, which is made up of a set of vertices $\mathcal{V}$ and a set of edges $\mathcal{E}$ such that an edge between two vertices represents their similarity. The adjacency matrix $A$ is $|\mathcal{V}| \times |\mathcal{V}|$ whose nonzero entries equal the edge weights.\\

Let us denote $links(\mathcal{A}, \mathcal{B})$ to be the sum of the edge weights between nodes in $\mathcal{A}$ and $\mathcal{B}$:
\begin{equation*}
	links(\mathcal{A}, \mathcal{B}) = \sum_{i \in \mathcal{A}, j \in \mathcal{B}} A_{ij}
\end{equation*}

The degree of $\mathcal{A}$ is the links of nodes in $\mathcal{A}$ to all the vertices: 
\begin{equation*}
	degree(\mathcal{A}) = links(\mathcal{A}, \mathcal{V})
\end{equation*}


The graph clustering problem seeks to partition the graph into $k$ disjoint partitions or clusters $\mathcal{V}_l \dots \mathcal{V}_k$ such that their union is $\mathcal{V}$.


\subsubsection{Minimum cut}
A \textbf{minimum cut} of a graph is a cut (a partition of the vertices of a graph into two disjoint subsets that are joined by at least one edge) whose cut set has the smallest number of edges (unweighted case) or smallest sum of weights possible.\\

\begin{equation*}
	Cut(G) = \min_{\mathcal{V}_l \dots \mathcal{V}_k} \sum_{c=1}^k links(\mathcal{V}_c, \mathcal{V} \backslash \mathcal{V}_c),
\end{equation*}
where $links(\mathcal{V}_c, \mathcal{V} \backslash \mathcal{V}_c) = degree(\mathcal{V}_c) - links(\mathcal{V}_c, \mathcal{V}_c)$.\\

Mincut can be a poor cut (e.g., cutting one node from the remaining of the graph).



\subsubsection{Ratio cut}
\begin{equation*}
	RCut(G) = \min_{\mathcal{V}_l \dots \mathcal{V}_k} \sum_{c=1}^k \frac{links(\mathcal{V}_c, \mathcal{V} \backslash \mathcal{V}_c)}{|\mathcal{V}_c|}
\end{equation*} 


\subsubsection{Normalized cut}
\begin{equation*}
	NCut(G) = \min_{\mathcal{V}_l \dots \mathcal{V}_k} \sum_{c=1}^k \frac{links(\mathcal{V}_c, \mathcal{V} \backslash \mathcal{V}_c)}{degree(\mathcal{V}_c)}
\end{equation*} 


\subsubsection{Conductance}

The conductance of the whole graph $G$ is the minimum conductance over all the possible cuts $\mathcal{V}_c$:

\begin{equation*}
\varphi(G) = \min \frac{links(\mathcal{V}_c, \mathcal{V} \backslash \mathcal{V}_c)}{\min\{degree(\mathcal{V}_c), degree(\mathcal{V} \backslash \mathcal{V}_c)\}}
\end{equation*} 


\subsubsection{Modularity}
The modularity of a clustering of a graph is the difference between the fraction of all edges that fall into individual clusters and the fraction that would do so if the graph vertices were randomly connected. 

\begin{equation*}
	Q(G) = \max_{\mathcal{V}_l \dots \mathcal{V}_k} \sum_{c=1}^k \left[ \frac{links(\mathcal{V}_c, \mathcal{V}_c)}{|\mathcal{E}|} - \left(\frac{degree(\mathcal{V}_c)}{2 |\mathcal{E}|}\right)^2\right]
\end{equation*} 


\subsection{SimRank: Similarity Based on Random Walk and Structural Context}

\textbf{Walk} in a graph $G$ between nodes $X$ and $Y$ is an ordered sequence of vertices, starting at $X$ and ending at $Y$, such that there is an edge between every pair of consecutive vertices.\\

\textbf{Neighborhood} in a directed graph $G = (\mathcal{V}, \mathcal{E})$:\begin{itemize}
  \item Individual in-neighborhood of $v$: $I(v) = \{u | (u, v) \in \mathcal{E}\}$
  \item Individual out-neighborhood of $v$: $O(v) = \{w | (v, w) \in \mathcal{E}\}$
\end{itemize}

\textbf{Similarity} defined by \textbf{SimRank}:
\begin{equation*}
	s(a, b) = 
\begin{cases}
\mbox{if }a=b\mbox{ then }1	\\
\mbox{if }a \neq b\mbox{ then }
\dfrac{C}{|I(a)| |I(b)|}
 \sum\limits_{i=1}^{|I(a)|}\sum\limits_{j=1}^{|I(b)|}
 s(I_i(a), I_j(b))
\end{cases},
\end{equation*}
where $C$ is a constant between $0$ and $1$. 


%-----------
\subsection{Spectral Clustering}

Process of spectral clustering:\begin{itemize}
  \item Construct a similarity graph (e.g., KNN graph) for all the data points
  \item Embed data points in a low-dimensional space (spectral embedding), in which the clusters are more obvious, with the use of the eigenvectors of the graph Laplacian
  \item A classical clustering algorithm (e.g., k-means) is applied to partition the embedding
\end{itemize}


\subsubsection{Matrix Representations of a Graph}

Adjacency Matrix:
\begin{equation*}
	A_{ij}=
	\begin{cases}
		w_{ij} & \mbox{ weight of edge }(i,j) \\
		0      & \mbox{ if no edge between }i, j
	\end{cases}
\end{equation*}

Degree matrix:
\begin{equation*}
	d_{i} = \sum_{\{j|(i,j) \in E\}} w_{ij}
\end{equation*}

The Laplacian:

\begin{equation*}
	L = D - A, \qquad L_{ij} = 
		\begin{cases}
		d_i	   & \mbox{ if }i=j	\\
		w_{ij} & \mbox{ if }(i,j)\mbox{ is an edge} \\
		0      & \mbox{ if no edge between }i, j
	\end{cases}
\end{equation*}

\begin{figure}[H]\centering\subfigure{  \includegraphics[width=0.3\linewidth]{spectral1.png}
}\quad\subfigure{  \includegraphics[width=0.3\linewidth]{spectral2.png}
}
\quad\subfigure{  \includegraphics[width=0.3\linewidth]{spectral3.png}
}\caption{Matrix Representations of a Graph}\end{figure}


\subsubsection{Eigenvalue and Eigenvector of Graph Laplacian}

The \textit{eigenvector} of a matrix $M$ is the the vector $\mathbf{\nu}$ such that
\begin{equation*}
	M\mathbf{\nu} = \lambda \mathbf{\nu},
\end{equation*}
where $\lambda$ is a scalar. Then $\lambda$ is the corresponding \textit{eigenvalue}. We usually restrict that $|\mathbf{\nu}| = 1$.\\

The graph Laplacian of $G$, $L_G$, also has\begin{itemize}
  \item eigenvalues = spectrum of the Laplacian $\{\lambda_1, \lambda_2, \dots , \lambda_n\}$ where $0 = \lambda_1 \leqslant \lambda_2 \leqslant \dots \leqslant \lambda_n$
  \item eigenvectors $\{\mathbf{\nu}_1, \mathbf{\nu}_2, \dots, \mathbf{\nu}_n\}$\\
\end{itemize}

Eigenvalues reveal global graph properties not apparent from edge structure:
\begin{itemize}
  \item If $0$ is the eigenvalue of $L$ with $k$ different eigenvectors, i.e., $0 = \lambda_1 = \lambda_2 = \dots = \lambda_k$, then $G$ has $k$ connected components
  \item If the graph is connected, $\lambda_2 > 0$ and $\lambda_2$ is the algebraic connectivity of $G$
  \item The greater $\lambda_2$, the more connected $G$ is
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{connectivity.png}
    \caption{Graph connectivity}
\end{figure}



\subsubsection{Bi-Partitioning via Spectral Methods}

\begin{itemize}
  \item To find the bipartition, we take the second eigenvector of the Laplacian, $\mathbf{\nu}_2$, corresponding to $\lambda_2$, the algebraic connectivity of $G$. The smaller $\lambda_2$, the better quality of the partitioning
  \item For each node $i$ in $G$, assign it the value $\mathbf{\nu}_2(i)$
  \item To find clusters $C_1$ and $C_2$, assign nodes with $\mathbf{\nu}_2(i) > 0$ to $C_1$ and $\mathbf{\nu}_2(i) < 0$ to $C_2$.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.3\linewidth]{partitioning1.png}
    %\caption{Graph connectivity}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{partitioning2.png}
    %\caption{Graph connectivity}
\end{figure}



\subsubsection{Extension to k partitions}

\textbf{The Ng-Jordan-Weiss (NJW) Algorithm} (2002)

Normalized Laplacian:
\begin{equation*}
	L_{norm} = D^{-1/2} L D^{1/2}
\end{equation*}

\begin{itemize}
  \item Compute the first $k$ eigenvectors $\mathbf{\nu}_1, \mathbf{\nu}_2, \dots, \mathbf{\nu}_k$ of $L_{norm}$, the normalized Laplacian
  \item Let $U \in R_{n\times k}$ be the matrix containing the vectors $\mathbf{\nu}_1, \mathbf{\nu}_2, \dots, \mathbf{\nu}_k$ as columns
  \item For $i = 1, \dots, n$, take the $i$-th row of $U$ as its feature vector after normalizing to norm $1$ 
  \item Cluster the points with $k$-means into $k$ clusters $C_1, \dots, C_k$
  \item Commonly used as a dimension reduction technique for clustering
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\linewidth]{NJW.png}
\end{figure}



%-----------
\subsection{SCAN: Density-Based Clustering of Networks}

\begin{itemize}
  \item Individuals in a tight social group, or \textbf{clique}, know many of the same people, regardless of the size of the group
  \item Individuals who are \textbf{hubs} know many people in different groups but belong to no single group. Politicians, for example bridge multiple groups
  \item Individuals who are \textbf{outliers} reside at the margins of society. Hermits, for example, know few people and belong to no group
\end{itemize}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{SCAN.png}
    \caption{Cliques, hubs, and outliers}
\end{figure}

The \textbf{neighborhood} of a vertex $v$: $\Gamma(v)$ is defined as $v$ and the immediate neighborhood of $v$.\\

\textbf{Similarity} between two vertices $v$ and $w$:
\begin{equation*}
	\sigma(v, w) = \frac{|\Gamma(v) \cap \Gamma(w)|}{\sqrt{|\Gamma(v) | \, |\Gamma(w)|}}
\end{equation*}

The desired features tend to be captured by a measure called \textbf{Structural Similarity}. Structural similarity is large for members of a clique and small for hubs and outliers.


\subsubsection{Structural Connectivity}

\textbf{$\mathbf{\varepsilon}$-neighborhood}: $N_\varepsilon(v) = \{w \in \Gamma(v) \;\big|\; \sigma(v, w) > \varepsilon \}$\\

\textbf{Core}: $\mathrm{CORE}_{\varepsilon, \mu}(v) \Leftrightarrow |N_\varepsilon(v)| \geqslant \mu$\\

\textbf{Direct structure reachable}: $\mathrm{DirReach}_{\varepsilon, \mu}(v, w)$
\begin{equation*}		
\mathrm{CORE}_{\varepsilon, \mu}(v) \wedge w \in N_\varepsilon(v)
\end{equation*}

\textbf{Structure reachable}: $\mathrm{REACH}_{\varepsilon, \mu}(u, v)$
\begin{equation*}	
\exists \; p_1...p_k: \mathrm{DirReach}_{\varepsilon, \mu} (u,p_1) \wedge \mathrm{DirReach}_{\varepsilon, \mu} (p_1,p_2) \wedge \dots \wedge \mathrm{DirReach}_{\varepsilon, \mu} (p_k,v)
\end{equation*}

\textbf{Structure connected}: $\mathrm{CONNECT}_{\varepsilon, \mu}(v, w)$
\begin{equation*}	
\exists \; u \in V: \mathrm{REACH}_{\varepsilon, \mu} (u,v) \wedge \mathrm{REACH}_{\varepsilon, \mu} (u, w)
\end{equation*}



\subsubsection{Structure-Connected Clusters}

\textbf{Structure-connected cluster} $C$:
\begin{itemize}
  \item Connectivity: $\forall v,w \in C: \mathrm{CONNECT}_{\varepsilon, \mu}(v, w)$
  \item Maximality: $\forall v,w \in V: v \in C \wedge \mathrm{REACH}_{\varepsilon, \mu}(v, w) \Rightarrow w \in C$\\
\end{itemize}

Hubs:
\begin{itemize}
  \item Do not belong to any cluster
  \item Bridge to many clusters\\
\end{itemize}


Outliers:\begin{itemize}
  \item Do not belong to any cluster 
  \item Connect to fewer clusters
\end{itemize}




%-----------
\subsection{Recommended Readings}

\begin{itemize}
  \item S. Arora, S. Rao, and U. Vazirani. Expander Flows, Geometric Embeddings and Graph Partitioning. J. ACM, 56:5:1–5:37, 2009
  \item G. Jeh and J. Widom. SimRank: A Measure of Structural-Context Similarity. KDD’02
  \item U. Luxburg. A Tutorial on Spectral Clustering. Statistics and Computing, 17, 2007
  \item A. Y. Ng, M. I. Jordan, and Y. Weiss. On Spectral Clustering: Analysis and an Algorithm. NIPS’01
  \item S. E. Schaeffer. Graph Clustering. Computer Science Review, 1:27–64, 2007
  \item X. Xu, N. Yuruk, Z. Feng, and T. A. J. Schweiger. SCAN: A Structural Clustering Algorithm for Networks. KDD’07
  \item M. J. Zaki and W. Meira, Jr.. Data Mining and Analysis: Fundamental Concepts and Algorithms. Cambridge University Press, 2014
  \item S. Parthasarathy and S. M. Faisal. Network Clustering, in C. Aggarwal and C. K. Reddy (eds.), Data Clustering: Algorithms and Applications (Chapter 17) . CRC Press, 2014
\end{itemize}

\newpage
\section{Cluster Analysis in Heterogeneous Networks}

%-----------
\subsection{Heterogeneous Information Networks}

\textbf{Information network}
\begin{itemize}
  \item A network where each node represents an entity (e.g., actor in a social network) and each link - a relationship between entities
  \item Each node/link may have attributes, labels, and weights
  \item Link may carry rich semantic information
  \item \textbf{Homogeneous} networks contain single object type and single link type
  \item \textbf{Heterogeneous}, multi-typed networks contain multiple object and link types
\end{itemize}


%-----------
\subsection{RankClus: Integrated Clustering and Ranking in Heterogeneous Networks}

Integrate ranking with clustering/classification in the same process:\begin{itemize}
  \item Ranking, as the feature, is conditional (i.e., relative) to a specific cluster
  \item Ranking and clustering may mutually enhance each other
  \item Ranking-based clustering: RankClus [EDBT'09], NetClus [KDD'09]
\end{itemize}



\subsubsection{A Bi-Typed Network Model and Simple Ranking}

\textbf{Bi-type Information Network}. Given two types of object sets $X$ and $Y$, where $X = \{x_1, x_2, \dots, x_m\}$, and $Y = \{y_1, y_2, \dots ,y_n\}$, graph $G = ⟨V,E⟩$ is called a bi-type information network on types $X$ and $Y$ , if $V (G) = X \cup Y$ and $E(G)=\{⟨o_i,o_j⟩\}$, where $o_i, o_j \in X \cup Y$.\\

Adjacency matrix can be written as:
\begin{equation*}
	W = W_{(m+n)\times(m+n)} = 
	\begin{pmatrix}
		W_{XX} & W_{XY} \\	
		W_{YX} & W_{YY}		
	\end{pmatrix}
\end{equation*}

The simplest ranking of conferences (X) and authors (Y) is based on the number of publications, which is proportional to the numbers of papers accepted by a conference or published by an author. Given the information network $G = ⟨\{X \cup Y\}, W ⟩$, simple ranking generates the ranking score of type $X$ and type $Y$ as follows:
\begin{equation*}
	\begin{cases}
		\vec{r}_X(x) = \frac{\sum\limits_{j=1}^n W_{XY}(x, j)}{\sum\limits_{i=1}^m \sum\limits_{j=1}^n W_{XY}(i, j)} \\
		\vec{r}_Y(y) = \frac{\sum\limits_{j=1}^n W_{XY}(i, y)}{\sum\limits_{i=1}^m \sum\limits_{j=1}^n W_{XY}(i, j)} \\
	\end{cases}
\end{equation*}

In this ranking, authors publishing more papers will have higher ranking score, even these papers are all in junk conferences. In fact, simple ranking evaluate importance of each object according to their immediate neighborhoods.


\subsubsection{Authority Ranking}

\begin{itemize}
  \item Rule 1: Highly ranked authors publish many papers in highly ranked conferences.
\begin{equation*}
	\vec{r}_Y(i) = \sum_{i=1}^m W_{YX}(j,i) \vec{r}_X(i)
\end{equation*}
  \item Rule 2: Highly ranked conferences attract many papers from many highly ranked authors.
\begin{equation*}
	\vec{r}_X(i) = \sum_{j=1}^n W_{XY}(i,j) \vec{r}_Y(j)
\end{equation*}
  \item Rule 3: The rank of an author is enhanced if he or she co- authors with many authors or many highly ranked authors.
\begin{equation*}
	\vec{r}_Y(i) = \alpha \sum_{j=1}^m W_{YX}(i,j) \vec{r}_X(j) + (1-\alpha) \sum_{j=1}^n W_{YY}(i,j) \vec{r}_Y(j)
\end{equation*}
\end{itemize}

$\vec{r}_Y$ should be the primary eigenvector of $\alpha W_{YX} W_{XY} + (1−\alpha) W_{YY}$, and $\vec{r}_X$ should be the primary eigenvector of $\alpha W_{XY} (I − (1 − \alpha) W_{YY})^{−1} W_{YX}$.


\subsubsection{The RankClus algorithm}

General idea: the higher conditional rank of an object in a cluster, the higher possibility that this object will belong to that cluster.

\begin{itemize}
  \item Step 0: Initialization. \\ In the initialization step, generate initial clusters for target objects, i.e., assign each target object with a cluster label from 1 to K randomly.
  \item Step 1: Ranking for each cluster. \\ Based on current clusters, calculate conditional rank for type $Y$ and $X$ and within-cluster rank for type $X$.  When some cluster is empty, the algorithm needs to restart in order to generate K clusters.
  \item Step 2: Estimation of the mixture model component coefficients. \\ Estimate the parameter $\Theta = \{\pi_{i,k}\}$ ($\pi_{i,k}$ is the probability that $x_i$ is from cluster $k$) in the mixture model, get new representations for each target object and centers for each target cluster.
  \item Step 3: Cluster adjustment.\\ Calculate the distance from each object to each cluster center and assign it to the nearest cluster.
  \item Repeat Steps 1, 2 and 3 until clusters changes only by a very small ratio $\varepsilon$ or the iteration number is bigger than a predefined number $iterNum$.
\end{itemize}


%-----------
\subsection{NetClus: Ranking-Based Clustering with Star Network Schema}

\begin{itemize}
  \item Generate initial partitions for target objects and induce initial net-clusters from the original network
  \item An E-M Framework: Repeat
\begin{itemize}
  \item Build ranking-based probabilistic generative model for each net-cluster
  \item Calculate the posterior probabilities for each target object
  \item Adjust their cluster assignment according to the new measure defined by the posterior probabilities to each cluster
\end{itemize}

  \item Until the clusters do not change significantly
  \item Calculate the posterior probabilities for each attribute object in each net-cluster
\end{itemize}


%-----------
\subsection{PathSim: Path-Based Similarity Measure for Heterogeneous Networks}

\textbf{Meta-Path} is a meta-level description of a path between two objects. For example: <<Author-Paper-Author>> or <<Author-Paper-Venue-Paper-Author>>.

\subsubsection{Popular Similarity Measures for Networks}

\textbf{Random walk} (RW): the probability of random walk starting at $x$ and ending at $y$, with meta-path $P$:
\begin{equation*}
	s(x, y) = \sum_{p \in P} prob(p)
\end{equation*}

Random walk favors highly visible objects (i.e., objects with large degrees).\\


\textbf{Pairwise random walk} (PRW): tte probability of pairwise random walk starting at $(x, y)$ and ending at a common object (say $z$), following a meta-path $(P1, P2)$:
\begin{equation*}
	s(x, y) = \sum_{(p_1, p_2) \in (P_1, P_2)} prob(p_1) \cdot prob(p_2^{-1})
\end{equation*}

Pairwise random walk favors pure objects (i.e., objects with highly skewed distribution in their in-links or out-links).\\

\textbf{PathSim} favors peers (objects with strong connectivity and similar visibility with a given meta-path):
\begin{equation*}
	s(x, y) = \frac{2 \times |\{p_{x \leadsto y}: p_{x \leadsto y} \in \mathcal{P}\}|}{|\{p_{x \leadsto x}: p_{x \leadsto x} \in \mathcal{P}\}| + |\{p_{y \leadsto y}: p_{y \leadsto y} \in \mathcal{P}\}|}
\end{equation*}



%-----------
\subsection{User Guided Meta-Path Selection for Clustering in Heterogeneous Networks}

Different user preferences (e.g., by seeding desired clusters) lead to the choice of different meta-paths

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{user.png}
\end{figure}

Input:\begin{itemize}
  \item The target type for clustering $T$ 
  \item Number of clusters $k$
  \item Seeds in some clusters: $L_1, \dots , L_k$
  \item Candidate meta-paths: $P_1, \dots , P_M$
\end{itemize}

Output
\begin{itemize}
  \item Weight of each meta-path: $w_1, \dots , w_m$
  \item Clustering results that are consistent with the user guidance
\end{itemize}


%-----------
\subsection{Recommended Readings}

\begin{itemize}
  \item Y. Sun, J. Han, P. Zhao, Z. Yin, H. Cheng, T. Wu, "RankClus: Integrating Clustering with Ranking for Heterogeneous Information Network Analysis", EDBT'09
  \item Y. Sun, Y. Yu, J. Han, “Ranking-Based Clustering of Heterogeneous Information Networks with Star Network Schema", KDD'09
  \item Y. Sun, J. Han, X. Yan, P. S. Yu, and T. Wu, “PathSim: Meta Path-Based Top-K Similarity Search in Heterogeneous Information Networks”, VLDB'11
  \item Y. Sun and J. Han, Mining Heterogeneous Information Networks: Principles and Methodologies, Morgan \& Claypool Publishers, 2012
  \item Y. Sun, B. Norick, J. Han, X. Yan, P. S. Yu, X. Yu. “PathSelClus: Integrating Meta-Path Selection with User-Guided Object Clustering in Heterogeneous Information Networks”, ACM Trans. on Knowledge Discovery from Data (TKDD), 7(3) 2013
  \item C. Aggarwal. Data Mining: The Textbook. Springer, 2015
\end{itemize}

\newpage
\section{Partitioning-Based Clustering Methods} 

%------------------
\subsection{Basic Concepts of Partitioning Algorithms}
\begin{itemize}
\item \textbf{Partitioning method}: Discovering the groupings in the data by optimizing a specific objective function and iteratively improving the quality of partitions\item \textbf{K-partitioning method}: Partitioning a dataset $D$ of $n$ objects into a set of $K$ clusters so that an objective function is optimized (e.g., the sum of squared distances is minimized, where $c_k$ is the centroid or medoid of cluster $C_k$)\item A typical objective function - \textbf{Sum of Squared Errors (SSE)}: 
\begin{equation*}
SSE(C) = \sum_{k=1}^K \sum_{x_i \in C_k} \| x_i - c_k\|^2
\end{equation*}
\end{itemize}


%------------------
\subsection{The K-Means Clustering Method}
Given a set of observations (x1, x2, …, xn), where each observation is a d-dimensional real vector, k-means clustering aims to partition the n observations into k (≤ n) sets S = {S1, S2, …, Sk} so as to minimize the within-cluster sum of squares (WCSS). In other words, its objective is to find:

\begin{equation*}
\underset{\mathbf{S}} {\operatorname{arg\,min}}  \sum_{i=1}^{k} \sum_{\mathbf x \in S_i} \left\| \mathbf x - \boldsymbol\mu_i \right\|^2 
\end{equation*}

where $\mu_i$ is the mean of points in $S_i$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{k_means.png}
    \caption{K-means Algorithm}
\end{figure}

\begin{algorithm}
\caption{The K-means Algorithm}
\begin{algorithmic}
\State Select K points as initial centroids
\Repeat    \State Form K clusters by assigning each point to its closest centroid    \State Re-compute the centroids (i.e., mean point) of each cluster\Until{convergence criterion is satisfied}
\end{algorithmic}
\end{algorithm}

Features of the \href{https://ru.wikipedia.org/wiki/K-means}{K-means} method:
\begin{itemize}
\item K-means clustering often terminates at a local optimal. Thus initialization can be important to find high-quality clusters.
\item You need to specify K, the number of clusters, in advance. In practice, one often runs a range of values and selected the <<best>> K value
\item Sensitive to noisy data and outliers
\item K-means is applicable only to objects in a continuous n-dimensional space
\item K-means is not suitable to discover clusters with non-convex shapes
\end{itemize}


\href{https://ru.wikipedia.org/wiki/K-means%2B%2B}{K-Means++} method for better initialization of k seeds:
\textrussian{
\begin{itemize}
\item Выбрать первый центроид случайным образом (среди всех точек)
\item Для каждой точки найти значение квадрата расстояния до ближайшего центроида (из тех, которые уже выбраны) $dx^2$
\item Выбрать из этих точек следующий центроид так, чтобы вероятность выбора точки была пропорциональна вычисленному для неё квадрату расстояния. \\Это можно сделать следующим образом. На шаге 2 нужно параллельно с расчётом $dx^2$ подсчитывать сумму $sum(dx^2)$. После накопления суммы найти значение $rnd=random(0, 1) \cdot sum$. $Rnd$ случайным образом укажет на число из интервала $[0; sum)$, и нам остаётся только определить, какой точке это соответствует. Для этого нужно снова начать подсчитывать сумму $sum(dx^2)$ до тех пор, пока сумма не превысит $rnd$. Как только это случится, суммирование останавливается, и мы можем взять текущую точку в качестве центроида. \\При выборе каждого следующего центроида специально следить за тем, чтобы он не совпал с одной из уже выбранных в качестве центроидов точек, не нужно, так как вероятность повторного выбора некоторой точки равна 0.
\item Повторять шаги 2 и 3 до тех пор, пока не будут найдены все необходимые центроиды.
\item Далее выполняется основной алгоритм K-Means.
\end{itemize}
}

%------------------
\subsection{The K-Medoids Clustering Method}
The K-Means algorithm is sensitive to outliers.\\
\href{http://en.wikipedia.org/wiki/K-medoids}{K-medoids}: Instead of taking the mean value of the object in a cluster as a reference point, \textbf{medoids} can be used, which is the most centrally located object in a cluster.

The most common realisation of k-medoid clustering is the Partitioning Around Medoids (PAM) algorithm and is as follows:

\begin{algorithm}[H]
\caption{The K-Medoids Algorithm}
\begin{algorithmic}
\State Initialize: randomly select $k$ of the $n$ data points as the medoids
\Repeat    \State Associate each data point to the closest medoid
    \ForAll{medoid $m$}        \ForAll{non-medoid data point $o$}
            \State Swap $m$ and $o$ and compute the total cost of the configuration
        \EndFor
    \EndFor
    \State Select the configuration with the lowest cost.\Until{convergence criterion is satisfied}
\end{algorithmic}
\end{algorithm}

PAM works effectively for small data sets but does not scale well for large data sets, because its computational complexity is $O(K(n − K)^2)$

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{k_medoids.png}
    \caption{K-medoids Algorithm}
\end{figure}

%------------------
\subsection{The K-Medians Clustering Method}
\href{https://ru.wikipedia.org/wiki/K-medians}{K-medians}: Instead of taking the mean value of the object in a cluster as a referencepoint, medians are used ($L_1$-norm as the distance measure). The criterion function for the K-Medians algorithm:
\begin{equation*}
\underset{\mathbf{S}} {\operatorname{arg\,min}} \sum_{i=1}^{k} \sum_{\mathbf{x} \in S_i} | \mathbf{x} - med_{i}|
\end{equation*}

%------------------
\subsection{The K-Modes Clustering Method}
K-Means cannot handle non-numerical (categorical) data. Mapping categorical value to 1/0 cannot generate quality clusters for high-dimensional data.\\

\textbf{K-Modes} is an extension to K-Means by replacing means of clusters with modes. Dissimilarity measure between object $X$ and the center of a cluster $Z_l$:
\begin{equation*}
\Phi(x_j, z_j) = 
\begin{cases}
1 - \dfrac{n_j^r}{n_l}, & \mbox{when }x_j = z_j \\
1, & \mbox{when }x_j \neq z_j
\end{cases}
\end{equation*}
\begin{itemize}
\item $z_j$ is the categorical value of attribute $j$ in the center of $Z_l$
\item $n_l$ is the number of objects in cluster $l$
\item $n_j^r$ is the number of objects whose attribute value is $r$
\end{itemize}

%------------------
\subsection{Kernel K-Means Clustering}
K-Means can only detect clusters that are linearly separable. \textbf{Kernel K-Means} can be used to detect non-convex clusters. Idea: project data onto the high-dimensional kernel space, and then perform K-Means clustering. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\linewidth]{kernel_k_means.png}
    \caption{Kernel K-means Algorithm}
\end{figure}

Given a set of vectors $a_1,a_2,...,a_n$, the k-means algorithm seeks to find clusters $\pi_1,\pi_2,...\pi_k$ that minimize the objective function:

\begin{equation*}
\mathcal{D}(\{\pi_c\}_{c=1}^k) = \sum_{c=1}^k\sum_{a_i \in \pi_c}\| \mathbf{a}_i-\mathbf{m}_c\|^2 \mbox{, where }\mathbf{m}_c = \frac{\sum_{a_i \in \pi_c} \mathbf{a}_i}{|\pi_c|}
\end{equation*}

Note that the c-th cluster is denoted by $\pi_c$, a clustering or partitioning by $\{\pi_c\}_{c=1}^k$, while the centroid or mean of cluster $\pi_c$ is denoted by $\mathbf{m}_c$.

The \href{http://web.cse.ohio-state.edu/~kulis/pubs/spectral_techreport.pdf}{kernel k-means} objective can be written as a minimization of:

\begin{equation*}
\mathcal{D}(\{\pi_c\}_{c=1}^k) 
= \sum_{c=1}^k\sum_{a_i \in \pi_c}\| \phi(\mathbf{a}_i)-\mathbf{m}_c\|^2 
\mbox{, where }\mathbf{m}_c = \frac{\sum_{a_i \in \pi_c} \phi(\mathbf{a}_i)}{|\pi_c|}
\end{equation*}

If we expand the distance computation $\| \phi(\mathbf{a}_i)-\mathbf{m}_c\|^2$ in the objective function, we obtain the following:

\begin{equation*}
\phi(\mathbf{a}_i) \cdot  \phi(\mathbf{a}_i) - \frac{2 \cdot \sum_{a_j \in \pi_c} \phi(\mathbf{a}_i) \cdot \phi(\mathbf{a}_j)}{|\pi_c|} + \frac{\sum_{a_j, a_l \in \pi_c}\phi(\mathbf{a}_j) \cdot \phi(\mathbf{a}_l)}{|\pi_c|^2}
\end{equation*}

Thus only inner products are used in the computation of the Euclidean distance between a point and a centroid. As a result, if we are given a kernel matrix $K$, where $K_{ij} = \phi(\mathbf{a}_i) \cdot \phi(\mathbf{a}_j)$, we can compute distances between points and centroids without knowing explicit representations of $\phi(\mathbf{a}_i)$ and $\phi(\mathbf{a}_j)$.

Typical kernel functions:
\begin{itemize}
\item Polynomial Kernel $K(\mathbf{a}_i, \mathbf{a}_j)=(\mathbf{a}_i \cdot \mathbf{a}_j + c)^d$\item Gaussian Kernel $K(\mathbf{a}_i, \mathbf{a}_j) = \exp(−\|\mathbf{a}_i − \mathbf{a}_j\|^2/2 \alpha^2)$\item Sigmoid Kernel $K(\mathbf{a}_i, \mathbf{a}_j)=\tanh(c(\mathbf{a}_i \cdot \mathbf{a}_j)+\theta)$
\end{itemize}


%------------------

\subsection{Recommended Readings}
\begin{itemize}
\item J. MacQueen. Some Methods for Classification and Analysis of Multivariate Observations. In Proc. of the 5th Berkeley Symp. on Mathematical Statistics and Probability, 1967\item S. Lloyd. Least Squares Quantization in PCM. IEEE Trans. on Information Theory, 28(2), 1982\item A. K. Jain and R. C. Dubes. Algorithms for Clustering Data. Prentice Hall, 1988\item L. Kaufman and P. J. Rousseeuw. Finding Groups in Data: An Introduction to Cluster Analysis. John Wiley \& Sons, 1990\item R. Ng and J. Han. Efficient and Effective Clustering Method for Spatial Data Mining. VLDB'94\item B. Sch{\"o}lkopf, A. Smola, and K. R. Müller. Nonlinear Component Analysis as a Kernel Eigenvalue Problem. Neural computation, 10(5):1299–1319, 1998\item I. S. Dhillon, Y. Guan, and B. Kulis. Kernel K-Means: Spectral Clustering and Normalized Cuts. KDD’04\item D. Arthur and S. Vassilvitskii. K-means++: The Advantages of Careful Seeding. SODA’07\item C. K. Reddy and B. Vinzamuri. A Survey of Partitional and Hierarchical Clustering Algorithms, in (Chap. 4) Aggarwal and Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press, 2014\item M. J. Zaki and W. Meira, Jr.. Data Mining and Analysis: Fundamental Concepts and Algorithms. Cambridge Univ. Press, 2014
\end{itemize}
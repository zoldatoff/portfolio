\newpage
\section{Hierarchical Clustering Methods} 

%---------------
\subsection{Basic Concepts}

\begin{wrapfigure}{r}{0.45\textwidth} 
	\centering
	\vspace{-30pt}
    \includegraphics[width=0.4\textwidth]{dendrogram.png}
    \vspace{-30pt}
\end{wrapfigure}

Hierarchical clustering:\begin{itemize}
  \item Generate a clustering hierarchy (drawn as a dendrogram)
  \item Not required to specify K, the number of clusters
  \item More deterministic
  \item No iterative refinement\\
\end{itemize}


Two categories of algorithms:
\begin{itemize}
  \item \textbf{Agglomerative}: Start with singleton clusters, continuously merge two clusters at a time to build a bottom-up hierarchy of clusters
  \item \textbf{Divisive}: Start with a huge macro-cluster, split it continuously into two groups, generating a top-down hierarchy of clusters
\end{itemize}

\textbf{Dendrogram}: decompose a set of data objects into a tree of clusters by multi-level nested partitioning. A clustering of the data objects is obtained by cutting the dendrogram at the desired level, then each connected component forms a cluster


\subsubsection{Extensions to Hierarchical Clustering}

Major weaknesses of hierarchical clustering methods
\begin{itemize}
  \item Can never undo what was done previously
  \item Do not scale well: time complexity is at least $O(n^2)$, where n is the number of total objects
\end{itemize}

Other hierarchical clustering algorithms\begin{itemize}
  \item \textbf{BIRCH} (1996): Use CF-tree and incrementally adjust the quality of sub-clusters
  \item \textbf{CURE} (1998): Represent a cluster using a set of well-scattered representative points
  \item \textbf{CHAMELEON} (1999): Use graph partitioning methods on the K-nearest neighbor graph of the data
\end{itemize}

%---------------
\subsection{Agglomerative Clustering Algorithms}

\textbf{AGNES (AGglomerative NESting)} (Kaufmann and Rousseeuw, 1990) 
\begin{itemize}
  \item Use the single-link method and the dissimilarity matrix
  \item Continuously merge nodes that have the least dissimilarity
  \item Eventually all nodes belong to the same cluster \\
\end{itemize}

Agglomerative clustering varies on different similarity measures among clusters:
\begin{itemize}
  \item \textbf{Single link (nearest neighbor)}: The similarity between two clusters is the similarity between their most similar (nearest neighbor) members
\begin{itemize}
  \item Local similarity-based: Emphasizing more on close regions, ignoring the overall structure of the cluster
  \item Capable of clustering non-elliptical shaped group of objects
  \item Sensitive to noise and outliers
\end{itemize}

  \item \textbf{Complete link (diameter)}: The similarity between two clusters is the similarity between their most dissimilar members
\begin{itemize}
  \item Merge two clusters to form one with the smallest diameter
  \item Nonlocal in behavior, obtaining compact shaped clusters
  \item Sensitive to outliers
\end{itemize}

  \item \textbf{Average link}: The average distance between an element in one cluster and an element in the other (i.e., all pairs in two clusters)
\begin{itemize}
  \item Expensive to compute
\end{itemize}

  \item \textbf{Centroid link}: The distance between the centroids of two clusters
\begin{itemize}
  \item Expensive to compute
\end{itemize}

  \item \textbf{Group Averaged Agglomerative Clustering (GAAC)}
\begin{itemize}
  \item Let two clusters $C_a$ and $C_b$ be merged into $C_{a \cup b}$. The new centroid is 
	\begin{equation*}
		c_{a \cup b} = \dfrac{N_a c_a + N_b c_b}{N_a + N_b}
	\end{equation*}
  \item The similarity measure for GAAC is the average of their distances
\end{itemize}

  \item \textbf{Ward’s criterion}: The increase in the value of the SSE criterion for the clustering obtained by merging them into $C_a \cup C_b$:
	\begin{equation*}
		W(C_{a \cup b}, c_{a \cup b}) - W(C, c) = \dfrac{N_a N_b}{N_a + N_b} d(c_a, c_b)
	\end{equation*}
\end{itemize}


%---------------
\subsection{Divisive Clustering Algorithms}

\textbf{DIANA (Divisive Analysis)} (Kaufmann and Rousseeuw, 1990): divisive clustering is a top-down approach in inverse order of AGNES.
\begin{itemize}
  \item The process starts at the root with all the points as one cluster
  \item It recursively splits the higher level clusters to build the dendrogram 
  \item Can be considered as a global approach
  \item More efficient when compared with agglomerative clustering\\
\end{itemize}

Algorithm Design for Divisive Clustering:
\begin{itemize}
  \item Choosing which cluster to split: check the sums of squared errors of the clusters and choose the one with the largest value
  \item Splitting criterion: one may use Ward’s criterion to chase for greater reduction in the difference in the SSE criterion as a result of a split; for categorical data, Gini-index can be used 
  \item Handling the noise: use a threshold to determine the termination criterion (do not generate clusters that are too small because they contain mainly noises)
\end{itemize}


%---------------
\subsection{BIRCH: A Micro-Clustering-Based Approach}
\textbf{BIRCH} (Balanced Iterative Reducing and Clustering Using Hierarchies) is a multiphase clustering algorithm (Zhang, Ramakrishnan \& Livny, SIGMOD’96). It incrementally constructs a CF (Clustering Feature) tree, a hierarchical data structure for multiphase clustering:
\begin{itemize}
  \item Phase 1: Scan DB to build an initial in-memory CF tree (a multi-level compression of the data that tries to preserve the inherent clustering structure of the data)
  \item Phase 2: Use an arbitrary clustering algorithm to cluster the leaf nodes of the CF-tree\\
\end{itemize}
Key idea: multi-level clustering\begin{itemize}
  \item Low-level micro-clustering: Reduce complexity and increase scalability
  \item High-level macro-clustering: Leave enough flexibility for high-level clustering\\
\end{itemize}

Algorithm features:\begin{itemize}
  \item Sensitive to insertion order of data points
  \item Due to the fixed size of leaf nodes, clusters may not be so natural
  \item Clusters tend to be spherical given the radius and diameter measures
  \item Scales linearly: finds a good clustering with a single scan and improves the quality with a few additional scans.\\
\end{itemize}

\subsubsection{Clustering feature}
\textbf{Clustering feature} vector in BIRCH is a summary of the statistics for a given sub-cluster --- the 0th, 1st, and 2nd moments from the statistical point of view: $CF = (N, LS, SS)$
\begin{itemize}
  \item N: Number of data points
  \item LS: linear sum of N points: $\sum_{i=1}^N X_i$  
  \item SS: square sum of N points: $\sum_{i=1}^N X_i^2$  
\end{itemize}

Measures of cluster: centroid, radius, diameter:
\begin{equation*}
\vec{x_0} = \sum\limits_i^n \dfrac{\vec{x_i}}{n},
R = \sqrt{\dfrac{\sum\limits_i^n (\vec{x_i}-\vec{x_0})^2}{n}},
D = \sqrt{\dfrac{\sum\limits_i^n \sum\limits_j^n (\vec{x_i}-\vec{x_j})^2}{n(n-1)}}
\end{equation*}


\subsubsection{CF tree}
\textbf{CF tree} is a height-balanced tree that stores the clustering features, the non-leaf nodes store sums of the CF's of their children. A CF tree has two parameters:
\begin{itemize}
  \item Branching factor: Maximum number of children
  \item Maximum diameter of sub-clusters stored at the leaf nodes
\end{itemize}

Algorithm of CF-tree construction: for each point in the input:
\begin{itemize}
  \item Find closest leaf entry
  \item Add point to leaf entry and update CF
  \item If entry diameter is more than maximum diameter then split leaf and possibly parents
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{birch.png}
\end{figure}


%---------------
\subsection{CURE: Clustering Using Well-Scattered Representatives}

\textbf{\href{http://en.wikipedia.org/wiki/CURE_data_clustering_algorithm}{CURE}} (Clustering Using REpresentatives) (S. Guha, R. Rastogi, and K. Shim, 1998) represents a cluster using a set of well-scattered representative points.\\

To avoid the problems with non-uniform sized or shaped clusters, CURE employs a novel hierarchical clustering algorithm that adopts a middle ground between the centroid based and all point extremes. In CURE, a constant number c of well scattered points of a cluster are chosen and they are shrunk towards the centroid of the cluster by a fraction $\alpha$. The scattered points after shrinking are used as representatives of the cluster. The clusters with the closest pair of representatives are the clusters that are merged at each step of CURE's hierarchical clustering algorithm. This enables CURE to correctly identify the clusters and makes it less sensitive to outliers.


%---------------
\subsection{CHAMELEON: Graph Partitioning on the KNN Graph of the Data}

\textbf{CHAMELEON} is a graph partitioning approach (G. Karypis, E. H. Han, and V. Kumar, 1999) that measures the similarity based on a dynamic model. Two clusters are merged only if the interconnectivity and closeness (proximity) between two clusters are high relative to the internal interconnectivity of the clusters and closeness of items within the clusters. CHAMELEON is capable to generate quality clusters at clustering complex objects. \\

Graph-based, two-phase algorithm:\begin{enumerate}
  \item Use a graph-partitioning algorithm: Cluster objects into a large number of relatively small sub-clusters
  \item Use an agglomerative hierarchical clustering algorithm: Find the genuine clusters by repeatedly combining these sub-clusters
\end{enumerate}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{CHAMELEON.png}
\end{figure}

\textbf{K-nearest neighbor (KNN)} graphs from an original data in 2D:
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{KNN.png}
\end{figure}

\textbf{The absolute interconnectivity} between $C_i$ and $C_j$: $EC_{\{C_i, C_j\}}$ is the sum of the weight of the edges that connect vertices in $C_i$ to vertices in $C_j$.\\

\textbf{Internal interconnectivity} of a cluster $C_i$ is the size of its min-cut bisector $EC_{C_i}$ (i.e., the weighted sum of edges that partition the graph into two roughly equal parts).\\

\textbf{Relative Interconnectivity (RI)}:
\begin{equation*}
	RI(C_i, C_j) = 2\cdot \frac{EC_{\{C_i, C_j\}}}{|EC_{C_i}| + |EC_{C_j}|}
\end{equation*}

\textbf{Relative closeness} between a pair of clusters $C_i$ and $C_j$ is the absolute closeness between $C_i$ and $C_j$, normalized w.r.t. the internal closeness of the two clusters $C_i$ and $C_j$:
\begin{equation*}
	RC(C_i, C_j) = \frac{\bar{S}_{EC_{\{C_i, C_j\}}}}{\frac{|C_i|}{|C_i|+|C_j|}\bar{S}_{EC_{C_i}} + \frac{|C_j|}{|C_i|+|C_j|}\bar{S}_{EC_{C_j}}}
\end{equation*}
\begin{itemize}
  \item $\bar{S}_{EC_{C_i}}$ and $\bar{S}_{EC_{C_j}}$ are the average weights of the edges that belong to the min-cut bisector of clusters $C_i$ and $C_j$ respectively
  \item $\bar{S}_{EC_{\{C_i, C_j\}}}$ is the average weight of the edges that connect vertices in $C_i$ to vertices in $C_j$\\
\end{itemize}

Merge Sub-Clusters:\begin{itemize}
  \item Merges only those pairs of clusters whose $RI$ and $RC$ are both above some user-specified thresholds
  \item Merge those maximizing the function that combines $RI$ and $RC$
\end{itemize}


%---------------
\subsection{Probabilistic Hierarchical Clustering}

Algorithmic hierarchical clustering\begin{itemize}
  \item Nontrivial to choose a good distance measure
  \item Hard to handle missing attribute values
  \item Optimization goal not clear: heuristic, local search
\end{itemize}
Probabilistic hierarchical clustering\begin{itemize}
  \item Use probabilistic models to measure distances between clusters
  \item Generative model: Regard the set of data objects to be clustered as a sample of the underlying data generation mechanism to be analyzed
  \item Easy to understand, same efficiency as algorithmic agglomerative clustering method, can handle partially observed data
\end{itemize}


\subsubsection{Generative Model}

Given a set of 1-D points $X = \{x_1, \dots, x_n\}$ for clustering analysis and assuming they are generated by a Gaussian distribution:
\begin{equation*}
	\mathcal{N}(\mu, \sigma^2) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}
\end{equation*}


The probability that a point $x_i \in X$ is generated by the model:
\begin{equation*}
	P(x_i \large| \mu, \sigma^2) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}
\end{equation*}


The likelihood that $X$ is generated by the model:
\begin{equation*}
	L(\mathcal{N}(\mu, \sigma^2): X) = P(X \large| \mu, \sigma^2) = \prod_{i=1}^n \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}
\end{equation*}

The task of learning the generative model is to find the parameters $\mu$ and $\sigma^2$ such that
\begin{equation*}
	\mathcal{N}(\mu_0, \sigma_0^2) = \argmax{\{L(\mathcal{N}(\mu, \sigma^2): X)\}}
\end{equation*}


\subsection{A Probabilistic Hierarchical Clustering Algorithm}

For a set of objects partitioned into $m$ clusters $C_1, \dots , C_m$, the quality can be measured by
\begin{equation*}
	Q(\{C_1, \dots, C_m\}) = \prod_{i=1}^m P(C_i)
\end{equation*}
where $P()$ is the maximum likelihood.\\

If we merge two clusters $C_{j_1}$ and $C_{j_2}$ into a cluster $C_{j_1} \cup C_{j_2}$, the change in quality of the overall clustering is:
\begin{equation*}
	\prod_{i=1}^m P(C_i) \cdot \left(\frac{P(C_{j_1} \cup C_{j_2})}{P(C_{j_1})P(C_{j_2})}-1\right)
\end{equation*}

Distance between clusters $C_1$ and $C_2$:
\begin{equation*}
	dist(C_i, C_j) = - \log{\frac{P(C_1 \cup C_2)}{P(C_1)P(C_2)}}
\end{equation*}

If $dist(C_i, C_j) < 0$, merge $C_i$ and $C_j$.

%------------------

\subsection{Recommended Readings}
\begin{itemize}
\item A. K. Jain and R. C. Dubes. Algorithms for Clustering Data. Prentice Hall, 1988
\item L. Kaufman and P. J. Rousseeuw. Finding Groups in Data: An Introduction to Cluster Analysis. John Wiley \& Sons, 1990
\item T. Zhang, R. Ramakrishnan, and M. Livny. BIRCH: An Efficient Data Clustering Method for Very Large Databases. SIGMOD'96
\item S. Guha, R. Rastogi, and K. Shim. Cure: An Efficient Clustering Algorithm for Large Databases. SIGMOD’98
\item G. Karypis, E.-H. Han, and V. Kumar. CHAMELEON: A Hierarchical Clustering Algorithm Using Dynamic Modeling. COMPUTER, 32(8): 68-75, 1999.
\item Jiawei Han, Micheline Kamber, and Jian Pei. Data Mining: Concepts and Techniques. Morgan Kaufmann, 3rd ed. , 2011 (Chap. 10)
\item C. K. Reddy and B. Vinzamuri. A Survey of Partitional and Hierarchical Clustering Algorithms, in (Chap. 4) Aggarwal and Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press, 2014
\item M. J. Zaki and W. Meira, Jr.. Data Mining and Analysis: Fundamental Concepts and Algorithms. Cambridge Univ. Press, 2014
\end{itemize}
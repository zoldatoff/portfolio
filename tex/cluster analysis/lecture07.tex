\newpage
\section{Methods for Clustering Validation}

\subsection{Clustering Validation and Assessment}

Major issues on clustering validation and assessment:
\begin{itemize}
\item Clustering evaluation (the goodness of the clustering)
\item Clustering stability (the sensitivity of the clustering result to various algorithm parameters, e.g., number of clusters)\item Clustering tendency (the suitability of clustering, i.e., whether the data has any inherent grouping structure)
\end{itemize}

%-------
\subsection{Measuring Clustering Quality}
There is no commonly recognized best suitable measure in practice.\\

Three categorization of measures: 
\begin{itemize}
\item \textbf{External}: Supervised, employ criteria not inherent to the dataset. Compare a clustering against prior or expert-specified knowledge (i.e., the ground truth) using certain clustering quality measure\item \textbf{Internal}: Unsupervised, criteria derived from data itself. Evaluate the goodness of a clustering by considering how well the clusters are separated and how compact the clusters are, e.g., silhouette coefficient\item \textbf{Relative}: Directly compare different clusterings, usually those obtained via different parameter settings for the same algorithm
\end{itemize}

%-------
\subsection{External Measures for Clustering Validation}
Given the ground truth $T$, $Q(C, T)$ is the quality measure for a clustering $C$. $Q(C, T)$ is good if it satisfies the following four essential criteria:
\begin{itemize}
\item Cluster homogeneity: the purer, the better 
\item Cluster completeness: assign objects belonging to the same category in the ground truth to the same cluster\item Rag bag better than alien: putting a heterogeneous object into a pure cluster should be penalized more than putting it into a rag bag (i.e., <<miscellaneous>> or <<other>> category) 
\item Small cluster preservation: splitting a small category into pieces is more harmful than splitting a large category into pieces
\end{itemize}

\subsubsection{Commonly Used External Measures}
\begin{itemize}
\item Matching-based measures: purity, maximum matching, F-measure\item Entropy-Based Measures: conditional entropy, normalized mutual information (NMI), variation of information\item Pairwise measures: 
\begin{itemize}\item Four possibilities: True positive (TP), FN, FP, TN\item Jaccard coefficient, Rand statistic, Fowlkes-Mallow measure
\end{itemize}\item Correlation measures: discretized Huber static, normalized discretized Huber static
\end{itemize}


%-------
\subsection{External Measures}

\subsubsection{Purity}
Purity quantifies the extent that cluster $C_i$ contains points only from one (ground truth) partition.
\begin{equation*}
purity_i = \frac{1}{n_i} \max_{j=1}^k{n_{ij}}
\end{equation*}

Total purity of clustering C:
\begin{equation*}
purity_i = \sum_{i=1}^{r} \frac{n_i}{n}purity_i = \frac{1}{n} \sum_{i=1}^{r} \max_{j=1}^k{n_{ij}},
\end{equation*}
where $r$ is the number of clusters, $k$ is the number of (ground truth) partitions.

\subsubsection{Maximum matching}
Only one cluster can match only one partition:
\begin{equation*}
match = \argmax_M \frac{w(M)}{n},
\end{equation*}
where $w(M) = \sum\limits_{e \in M} w(e) = \sum\limits_{e \in M} n_{ij}$

\subsubsection{F-Measure}

\textbf{Precision}: The fraction of points in $C_i$ from the majority partition $T_{j_i}$ (i.e., the same as purity), where $j_i$ is the partition that contains the maximum number of points from $C_i$:
\begin{equation*}
precision_i = \frac{1}{n_i} \max_{j=1}^k{n_{ij}} =  \frac{n_{ij_i}}{n_i}
\end{equation*}

\textbf{Recall}: The fraction of point in partition $T_{j_i}$ shared in common with cluster $C_i$, where $m_{j_i} = | T_{j_i} |$
\begin{equation*}
recall_i = \frac{n_{ij_i}}{| T_{j_i} |} =  \frac{n_{ij_i}}{m_{j_i}}
\end{equation*}

\textbf{F-measure} for $C_i$ is the harmonic means of precision and recall:
\begin{equation*}
F_i = \frac{2n_{ij_i}}{n_i + m_{j_i}}
\end{equation*}

F-measure for clustering C is average of all clusters:
\begin{equation*}
F = \frac{1}{r}\sum_{i=1}^{r}F_i
\end{equation*}



\subsubsection{Conditional Entropy}
Entropy of clustering $\mathcal{C}$: $H(\mathcal{C}) = -\sum\limits_{i=1}^r p_{C_i} \log{p_{C_i}}$, where $p_{C_i} = \dfrac{n_i}{n}$ is the probability of cluster $C_i$

Entropy of partitioning $\mathcal{T}$: $H(\mathcal{T}) = -\sum\limits_{j=1}^k p_{T_i} \log{p_{T_i}}$

Entropy of $\mathcal{T}$ with respect to cluster $C_i$: $H(\mathcal{T} \mid C_i) = -\sum\limits_{j=1}^k \dfrac{n_{ij}}{n_i} \log{\dfrac{n_{ij}}{n_i}}$

\textbf{Conditional entropy of $\mathcal{T}$ with respect to clustering $\mathcal{C}$}:
\begin{equation*}
H(\mathcal{T} \mid \mathcal{C}) = - \sum_{j=1}^k \frac{n_i}{n} H(\mathcal{T} \mid C_i) = -\sum_{i=1}^r \sum_{j=1}^k p_{ij}\log{\frac{p_{ij}}{p_{C_i}}}
\end{equation*}

The more a cluster’s members are split into different partitions,the higher the conditional entropy. For a perfect clustering, the conditional entropy value is 0, where the worst possible conditional entropy value is $\log k$.

\begin{equation*}
H(\mathcal{T} \mid \mathcal{C}) = -\sum_{i=1}^r \sum_{j=1}^k p_{ij}\log{p_{ij}} + \sum_{i=1}^r p_{C_i} \log{p_{C_i}} = H(\mathcal{C}, \mathcal{T}) - H(\mathcal{C})
\end{equation*}


\subsubsection{Normalized Mutual Information (NMI)}
\textbf{Mutual information} quantifies the amount of shared info between the clustering $\mathcal{C}$ and partitioning $\mathcal{T}$.

\begin{equation*}
I(\mathcal{C}, \mathcal{T}) = \sum_{i=1}^r \sum_{j=1}^k p_{ij} \log{\frac{p_{ij}}{p_{C_i} \cdot p_{T_j}}}
\end{equation*}

Mutual information measures the dependency between the observed joint probability $p_{ij}$ of $\mathcal{C}$ and $\mathcal{T}$, and the expected joint probability $p_{C_i} \cdot p_{T_j}$ under the independence assumption. When $\mathcal{C}$ and $\mathcal{T}$ are independent, $p_{ij} = p_{C_i} \cdot p_{T_j}$ and $I(\mathcal{C}, \mathcal{T}) = 0$. However, there is no upper bound on the mutual information.\\

\textbf{Normalized mutual information (NMI)}:
\begin{equation*}
NMI(\mathcal{C}, \mathcal{T}) = \frac{I(\mathcal{C}, \mathcal{T})}{\sqrt{H(\mathcal{C}) \cdot H(\mathcal{T})}} \in [0, 1]
\end{equation*}
Value close to 1 indicates a good clustering.

\subsubsection{Pairwise Measures}
Assume:
\begin{itemize}
\item $\mathbf{x}_i$ and $\mathbf{x}_j$ are two points
\item $y$ is the partition label
\item $\hat{y}$ is the cluster label
\item $N = \dbinom{n}{2}$
\end{itemize}

There are four possibilities based on the agreement between cluster label and partition label:
\begin{itemize}
\item True positive: $TP = |\{(\mathbf{x}_i, \mathbf{x}_j): y_i = y_j \mbox{ and } \hat{y}_i = \hat{y_j}\}|$
\item False negative: $FN = |\{(\mathbf{x}_i, \mathbf{x}_j): y_i = y_j \mbox{ and } \hat{y}_i \neq \hat{y_j}\}|$
\item False positive: $FP = |\{(\mathbf{x}_i, \mathbf{x}_j): y_i \neq y_j \mbox{ and } \hat{y}_i = \hat{y_j}\}|$
\item True negative: $TN = |\{(\mathbf{x}_i, \mathbf{x}_j): y_i \neq y_j \mbox{ and } \hat{y}_i \neq \hat{y_j}\}|$
\end{itemize}

Define four measures:
\begin{itemize}
\item $TP = \sum\limits_{i=1}^r \sum\limits_{j=1}^k \dbinom{n_{ij}}{2}$
\item $FN = \sum\limits_{j=1}^k \dbinom{m_j}{2} - TP$
\item $FP = \sum\limits_{i=1}^r \dbinom{n_i}{2} - TP$
\item $TN = N - (TP+FN+FP)$
\end{itemize}

\textbf{Jaccard Coefficient}: $Jaccard = \dfrac{TP}{TP + FN + FP}$

\textbf{Rand Statistic}: $Rand = \dfrac{TP + TN}{N}$

\textbf{Fowlkes-Mallow Measure}: $FM = \sqrt{prec \times recall} = \dfrac{TP}{\sqrt{(TP+FN)(TP+FP)}}$


%-------
\subsection{Internal Measures for Clustering Validation}
Assume:
\begin{itemize}
\item Clustering $\mathcal{C} = \{C_1 \dots C_k\}$
\item Cluster $C_i$ contains $n_i = |C_i|$ points
\item $W(S, R)$ is the sum of weights on all edges with one vertex in $S$ and the other in $R$
\item The sum of all the intra-cluster weights over all clusters: $W_{in} = \dfrac{1}{2} \sum\limits_{i=1}^k W(C_i, C_i)$
\item The sum of all the inter-cluster weights: $W_{out} = \dfrac{1}{2} \sum\limits_{i=1}^k W(C_i, \overline{C_i}) = \sum\limits_{i=1}^{k-1}\sum\limits_{j>i}W(C_i, C_i)$
\item The number of distinct intra-cluster edges: $N_{in} = \sum\limits_{i=1}^k \dbinom{n_i}{2}$
\item The number of distinct inter-cluster edges: $N_{out} = \sum\limits_{i=1}^{k-1} \sum\limits_{j>i} n_i n_j$
\end{itemize}


\textbf{BetaCV Measure}: 
\begin{equation*}
BetaCV = \dfrac{W_{in} / N_{in}}{W_{out} / N_{out}}
\end{equation*}

The ratio of the mean intra-cluster distance to the mean inter-cluster distance. The smaller, the better the clustering.\\

\textbf{Normalized Cut}: 
\begin{equation*}
NC = \sum\limits_{i=1}^k \dfrac{ W(C_i, \overline{C_i})}{ W(C_i, V)} = \sum\limits_{i=1}^k \dfrac{ W(C_i, \overline{C_i})}{ W(C_i, C_i) + W(C_i, \overline{C_i})}
\end{equation*}

The higher normalized cut value, the better the clustering.\\

\textbf{Modularity} (for graph clustering):
\begin{equation*}
Q = \sum_{i=1}^k \left( \frac{W(C_i, C_i)}{W(V, V)} - \frac{W(C_i, V)^2}{W(V, V)^2} \right) \mbox{, where } W(V, V) = 2 \cdot (W_{in}+W_{out})
\end{equation*}

Modularity measures the difference between the observed and expected fraction of weights on edges within the clusters. The smaller the value, the better the clustering --- the intra-cluster distances are lower than expected.


%-------
\subsection{Relative Measures}
\textbf{Relative measure}: directly compare different clusterings, usually those obtained via different parameter settings for the same algorithm.

\subsubsection{Silhouette coefficient}
Silhouette coefficient for a point $\mathbf{x}_i$:
\begin{equation*}
s_i = \frac{\mu_{out}^{min}(\mathbf{x}_i) - \mu_{in}(\mathbf{x}_i)}{\max \{\mu_{out}^{min}(\mathbf{x}_i), \mu_{in}(\mathbf{x}_i)\}},
\end{equation*}
\begin{itemize}
\item $\mu_{in}(\mathbf{x}_i)$ is the mean distance from $\mathbf{x}_i$ to points in its own cluster
\item $\mu_{out}^{min}(\mathbf{x}_i)$ is the mean distance from $\mathbf{x}_i$ to points in its closest cluster
\end{itemize}

Silhouette coefficient: $SC = \dfrac{1}{n}\sum\limits_{i=1}^n s_i$. SC close to +1 implies good clustering: points are close to their own clusters but far from other clusters.


%-------
\subsection{Cluster Stability}
A bootstrapping approach to find the best value of $k$ (judged on stability):
\begin{itemize}
\item Generate $t$ samples of size $n$ by sampling from $\mathbf{D}$ with replacement\item For each sample $\mathbf{D}_i$, run the same clustering algorithm with $k$ values from 2 to $k_{max}$\item Compare the distance between all pairs of clusterings $C_k(\mathbf{D}_i)$ and $C_k(\mathbf{D}_j)$ via some distance function: compute the expected pairwise distance for each value of $k$\item The value $k^*$ that exhibits the least deviation between the clusterings obtained from the resampled datasets is the best choice for $k$ since it exhibits the most stability
\end{itemize}


\subsubsection{Other Methods for Finding the Number of Clusters}
\textbf{Empirical method} for a small $n$: $k \approx \sqrt{n/2}$\\

\textbf{Elbow method}: use the turning point in the curve of the sum of within cluster variance with respect to the number of clusters

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{elbow.png}
\end{figure}

\textbf{Cross validation method}:
\begin{itemize}
\item Divide a given data set into $m$ parts\item Use $m – 1$ parts to obtain a clustering model\item Use the remaining part to test the quality of the clustering \\\textit{For example, for each point in the test set, find the closest centroid, and use the sum of squared distance between all points in the test set and the closest centroids to measure how well the model fits the test set.}\item For any $k > 0$, repeat it $m$ times, compare the overall quality measure w.r.t. different $k$’s, and find number of clusters that fits the data the best.
\end{itemize}


%-------
\subsection{Clustering Tendency}
Methods to define clusterability:
\begin{itemize}
\item \textbf{Spatial histogram}: contrast the histogram of the data with that generated from random samples\item  \textbf{Distance distribution}: compare the pairwise point distance from the data withthose from the randomly generated samples\item \textbf{Hopkins Statistic}: a sparse sampling test for spatial randomness
\end{itemize}

\subsubsection{Testing Clustering Tendency: A Spatial Histogram Approach}

\textbf{Spatial histogram} approach: Contrast the d-dimensional histogram of the input dataset \textbf{D} with the histogram generated from random samples. Dataset \textbf{D} is clusterable if the distributions of two histograms are rather different.

\begin{itemize}
\item Divide each dimension into equi-width bins, count how many points lie in each cells, and obtain the empirical joint probability mass function (EPMF).
\item Do the same for the randomly sampled data
\item Compute how much they differ using the \href{http://en.wikipedia.org/wiki/Kullback–Leibler_divergence}{Kullback-Leibler} (KL) divergence value
\end{itemize}

%-------
\subsection{Recommended Readings}
\begin{itemize}
\item M. J. Zaki and W. Meira, Jr. <<Data Mining and Analysis: Fundamental Concepts and Algorithms>>. Cambridge University Press, 2014\item L. Hubert and P. Arabie. <<Comparing Partitions>>. Journal of Classification, 2: 193–218, 1985\item A. K. Jain and R. C. Dubes. <<Algorithms for Clustering Data>>. Printice Hall, 1988\item M. Halkidi, Y. Batistakis, and M. Vazirgiannis. <<On Clustering Validation Techniques>>. Journal of Intelligent Info. Systems, 17(2-3): 107–145, 2001\item J. Han, M. Kamber, and J. Pei. <<Data Mining: Concepts and Techniques>>. Morgan Kaufmann, 3rd ed. , 2011\item H. Xiong and Z. Li. <<Clustering Validation Measures>>. in (Chapter 23) C. Aggarwal and C. K. Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press, 2014\end{itemize}
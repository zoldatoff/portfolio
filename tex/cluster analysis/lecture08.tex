\newpage
\section{Clustering High-Dimensional Data}

%-----------
\subsection{Introduction to High-Dimensional Clustering}
\subsubsection{Challenges of Clustering High-Dimensional Data}

Many clustering algorithms deal with 1-3 dimensions. These methods may not work well when the number of dimensions grows to 20. But many applications, such as text documents or DNA micro-array data, may need to handle tens of thousands of dimensions.

Difficulties of dimensionality:
\begin{itemize}
  \item \textbf{Optimization:} The difficulty of global optimization increases exponentially with an increase in the number of dimensions
  \item \textbf{Distance concentration}: the relative contrast of $L_p$ distances diminishes as dimensionality increases. So, far and close neighbors have similar distances.
  \item \textbf{Irrelevant attributes} can interfere with the performance of clustering for that object. The relevance of certain attributes may differ for different groups of objects.
  \item \textbf{Correlated attributes}: strong correlation among a subset of attributes can be used to reduce dimensionality.
  \item \textbf{Data sparsity}: data volume in high-dimensional space is extremely sparse.
\end{itemize}



\subsubsection{Methods for Clustering High-Dimensional Data}

Methods can be grouped in two categories
\begin{itemize}
  \item \textbf{Subspace-clustering}: Search for clusters existing in subspaces of the given high dimensional data space. Algorithms: CLIQUE, ProClus, and bi-clustering approaches
  \item \textbf{Dimensionality reduction approaches}: Construct a much lower dimensional space and search for clusters there (may construct new dimensions by combining some dimensions in the original data). Algorithms: spectral clustering and various dimensionality reduction methods.
\end{itemize}

Clustering should not only consider dimensions but also attributes (features):
\begin{itemize}
  \item \textbf{Feature selection}: Useful to find a subspace where the data have nice clusters
  \item \textbf{Feature transformation}: Effective if most dimensions are relevant. Algorithms: PCA (Principal Component Analysis) and SVD (Singular Value Decomposition) are useful when features are highly correlated or redundant.
\end{itemize}


\subsubsection{Subspace Clustering Methods}

\begin{itemize}
  \item Axis-parallel vs. arbitrarily oriented subspaces	\begin{itemize}
  		\item Axis-parallel: Subspaces are in parallel with some axes
  		\item Arbitrarily oriented subspaces
	\end{itemize}
  \item Subspace search methods: Search in axis-parallel subspaces to find clusters
	\begin{itemize}
 		\item Bottom-up approaches
  		\item Top-down approaches
  	\end{itemize}  \item Search and clustering in arbitrarily oriented subspaces 
	\begin{itemize}
 		\item Correlation-based clustering methods. E.g., PCA-based approaches 
  	\end{itemize}
  \item Bi-clustering methods
	\begin{itemize}  		\item Optimization-based methods
  		\item Enumeration methods
  	\end{itemize}
\end{itemize}


%-----------
\subsection{Subspace Clustering}

\subsubsection{Subspace Search Methods}
Similarity measure is based on distance or density:
\begin{itemize}
  \item \textbf{Bottom-up approaches}
	\begin{itemize}
		\item Start from low-D subspaces and search higher-D subspaces only when there may be clusters in such subspaces		\item Various pruning techniques to reduce the number of higher-D subspaces to be searched		\item Ex. CLIQUE (Agrawal et al. 1998)
  	\end{itemize}
  \item \textbf{Top-down approaches}
	\begin{itemize}		\item Start from full space and search smaller subspaces recursively		\item Effective only if the locality assumption holds: Restricts that the subspace of a cluster can be determined by the local neighborhood		\item Ex. PROCLUS (Aggarwal et al. 1999): A k-medoid-like method
  	\end{itemize}
\end{itemize}


\subsubsection{Correlation-Based Methods}

Correlation-based methods are based on advanced correlation models:
\begin{itemize}
  \item Hough transform 
  \item Fractal dimensions
  \item PCA (Principal Component Analysis)-based approach
	\begin{itemize}	  \item Find a projection that captures the largest amount of variation in data	  \item Apply PCA to derive a set of new, uncorrelated dimensions (dimensionality reduction)	  \item Then find clusters in the new space or its subspaces
  	\end{itemize}
\end{itemize}

Principal Component Analysis illustration:
\begin{itemize}
  \item Given N data vectors (numeric data only) from n-dimensions, find $k \leqslant n$ orthogonal vectors (principal components) that can be best used to represent data  \item Normalize input data: Each attribute falls within the same range  \item Compute k orthogonal (i.e. linearly uncorrelated) (unit) vectors, i.e., principal components  \item Each input vector is a linear combination of the k principal component vectors  \item The principal components are sorted in order of decreasing <<significance>> or strength 
  \item Eliminate the weak components (i.e., those with low variance). That is, using the strongest principal components, it is possible to reconstruct a good approximation of the original data
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{PCA.png}
    \caption{PCA of a multivariate Gaussian distribution}
\end{figure}


\subsubsection{Bi-Clustering Methods}

\textbf{Bi-clustering}: cluster both objects and attributes simultaneously (treating objects and attributes in a symmetric way).\\
Four requirements:\begin{enumerate}
  \item Only a small set of objects participate in a cluster
  \item A cluster only involves a small number of attributes
  \item An object may participate in multiple clusters or does not participate in any cluster at all
  \item An attribute may be involved in multiple clusters or is not involved in any cluster at all
\end{enumerate}

Types of ideal Bi-Clusters:
\begin{itemize}
  \item Bi-clusters with constant values: $e_{ij} = c$ 
  \item Bi-clusters with constant values in rows: $e_{ij} = c + \alpha_i$. Also, it can be constant values in columns.
  \item Bi-clusters with coherent values (i.e., pattern-based clusters): $e_{ij} = c + \alpha_i + \beta_j$ 
  \item Bi-clusters with coherent evolutions in rows: $(e_{i_1j_1}− e_{i_1j_2})(e_{i_2j_1}− e_{i_2j_2}) \geqslant 0$
\end{itemize}

Methods of Bi-clustering: 
\begin{itemize}
  \item Optimization-based methods
	\begin{itemize}		\item Try to find submatrices one at a time to achieve the best significance as a bi-cluster
		\item Due to the cost in computation, greedy search is employed to find local optimal bi-clusters		\item Ex. $\delta$-bicluster Algorithm (Cheng and Church, ISMB’2000)
  	\end{itemize}  \item Enumeration methods
	\begin{itemize}		\item Use a tolerance threshold to specify the degree of noise allowed in the bi-clusters to be mined		\item Then try to enumerate all submatrices as bi-clusters that satisfy the requirements		\item Ex. $\delta$-pCluster Algorithm (H. Wang et al. SIGMOD’2002, MaPle: Pei et al., ICDM’2003)
  	\end{itemize}
\end{itemize}



\subsubsection{δ-Bi-clustering}

$\delta$-bi-clustering is used for micro-array data analysis
For a submatrix $I \times J$:
\begin{itemize}
  \item The mean of the i-th row: $e_{iJ} = \dfrac{1}{|J|}\sum\limits_{j \in J} e_{ij}$
  \item The mean of the j-th column: $e_{Ij} = \dfrac{1}{|I|}\sum\limits_{i \in I} e_{ij}$ 
  \item The mean of all elements in the submatrix: $e_{IJ} = \dfrac{1}{|I| \cdot |J|}\sum\limits_{i \in I, j \in J} e_{ij}$
  \item The quality of the submatrix as a bi-cluster can be measured by the mean squared residue value
\begin{equation*}
	H(I \times J) = = \dfrac{1}{|I| \cdot |J|}\sum\limits_{i \in I, j \in J} (e_{ij} - e_{iJ} - e_{Ij} + e_{IJ})^2
\end{equation*}
\item A submatrix $I \times J$ is $\delta$-bi-cluster if $H(I \times J) \leqslant \delta$ where $\delta \geqslant 0$ is a threshold. By setting $\delta > 0$, a user can specify the tolerance of average noise per element against a perfect bi-cluster.
\end{itemize}

\textbf{Maximal $\delta$-bi-cluster} --- a $\delta$-bi-cluster $I \times J$ s.t. no other $\delta$-bi-cluster $I' \times J'$ which contains $I \times J$

Two phase computation (finds only one δ-bi-cluster, thus needs to run multiple times):
\begin{itemize}
  \item \textbf{Deletion phase}
	\begin{itemize}
	  \item Start from the whole matrix, iteratively remove rows and columns while the mean squared residue of the matrix is over $\delta$. 
	  \item At each iteration, for each row/column compute the \textbf{mean squared residue} and remove the row or column of the largest mean squared residue:
	  \begin{eqnarray*}
	  	d(i) = \dfrac{1}{|J|}\sum\limits_{j \in J} (e_{ij} - e_{iJ} - e_{Ij} + e_{IJ})^2,\\
	  	d(j) = \dfrac{1}{|I|}\sum\limits_{i \in I} (e_{ij} - e_{iJ} - e_{Ij} + e_{IJ})^2
	  \end{eqnarray*}
	\end{itemize}
  \item \textbf{Addition phase}
	\begin{itemize}
	  \item Expand iteratively the $\delta$-bi-cluster $I \times J$ obtained in the deletion phase as long as the $\delta$-bi-cluster requirement is maintained
	  \item Consider all the rows/columns not involved in the current bi-cluster $I \times J$ by calculating their mean squared residues
	  \item A row/column of the smallest mean squared residue is added into the current $\delta$-bi-cluster
	\end{itemize}
\end{itemize}



\subsubsection{δ-pClustering}
$\delta$-pClusters uses clustering by pattern similarity [H. Wang, et al., SIGMOD’02]\\

For any $2 \times 2$ submatrix of $I \times J$:
\begin{equation*}
	\mbox{\textit{p-score}}
\begin{pmatrix} 
e_{i_1j_1}  & e_{i_1j_2}  \\
e_{i_2j_1}  & e_{i_2j_2} 
\end{pmatrix} = | (e_{i_1j_1} - e_{i_2j_1}) - (e_{i_1j_2} - e_{i_2j_2}) |
\end{equation*}

For scaling patterns, logarithmic approach will lead to the same p-score form: $\dfrac{d_{xa} / d_{ya}}{d_{xb} / d_{yb}}$.\\

A submatrix $I \times J$ is a \textbf{$\delta$-pCluster} (pattern-based cluster) if the p-score of every $2 \times 2$ submatrix of $I \times J$ is at most $\delta$, where $\delta \geqslant 0$ is a threshold specifying a user's tolerance of noise against a perfect bi-cluster. The p-score controls the noise on every element in a bi-cluster, while the mean squared residue captures the average noise. A $\delta$-pCluster is \textbf{maximal} if no more rows or columns can be added to still make it retain as a $\delta$-pCluster.


%-----------
\subsection{Dimensionality Reduction Methods}

\textbf{Dimensionality reduction} --- reduce dimensionality by mathematical transformation

Methods:
\begin{itemize}
  \item \textbf{Nonnegative matrix factorization (NMF)}: one high-dimensional sparse nonnegative matrix factorizes approximately into two low-rank matrices
  \item \textbf{Spectral clustering} uses the spectrum of the similarity matrix of the data to perform dimensionality reduction for clustering in fewer dimensions. Combines feature extraction and clustering. Typical spectral clustering methods: Normalized Cuts (Shi and Malik, CVPR’97 or PAMI’2000) and The Ng-Jordan-Weiss algorithm (NIPS’01).
\end{itemize}


\subsubsection{Clustering by Nonnegative Matrix Factorization (NMF)}
\begin{itemize}
  \item A nonnegative matrix $A_{n \times d}$ (e.g., word frequencies in documents) can be approximately factorized into two nonnegative lower rank matrices $U_{n \times k}$ and $V_{k \times d}$ ($k \ll d, n$): $A_{n \times d} \approx U_{n \times k} V_{k \times d}$
  \item Residue matrix $R$ represents the noise in the underlying data: $R = A − U V$
  \item Constrained optimization: Determine $U$ and $V$ so that the sum of the square of the residuals in $R$ is minimized 
  \item $U$ and $V$ simultaneously provide the clusters on the rows (docs) and columns (words):
	\begin{itemize}
		\item $U_{n \times k}$: the components of each of $n$ objects mapped into each of $k$ newly created dimensions
		\item $V_{k \times d}$: Each of $k$ newly created dimensions in terms of the original $d$ dimensions
	\end{itemize}
\end{itemize}


%-----------
\subsection{Recommended Readings}
\begin{itemize}
\item R. Agrawal, J. Gehrke, D. Gunopulos, and P. Raghavan. Automatic Subspace Clustering of High Dimensional Data for Data Mining Applications. SIGMOD’98\item C. C. Aggarwal, C. Procopiuc, J. Wolf, P. S. Yu, and J.-S. Park. Fast Algorithms for Projected Clustering. SIGMOD’99\item Y. Cheng and G. Church. Biclustering of Expression Data. ISMB’00\item H.-P. Kriegel, P. Kroeger, and A. Zimek. Clustering High Dimensional Data: A Survey on Subspace Clustering, Pattern-Based Clustering, and Correlation Clustering. TKDD’09\item S. C. Madeira and A. L. Oliveira. Bi-clustering Algorithms for Biological Data Analysis: A Survey. IEEE/ACM Trans. Comput. Biol. Bioinformatics, 1, 2004\item L. Parsons, E. Haque, and H. Liu. Subspace Clustering for High Dimensional Data: A Review. ACM SIGKDD Explorations, 6(1):90–105, 2004.\item J. Pei, X. Zhang, M. Cho, H. Wang, and P. S. Yu. Maple: A Fast Algorithm for Maximal Pattern-Based Clustering. ICDM’03\item H. Wang, W. Wang, J. Yang, and P. S. Yu. Clustering by Pattern Similarity in Large Data Sets. SIGMOD’02\item A. Zimek. Clustering High-Dimensional Data (Chapter 9), in C. Aggarwal and C. K. Reddy (eds.), Data Clustering: Algorithms and Applications. CRC Press, 2014\end{itemize}


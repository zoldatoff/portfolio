#!/bin/bash
pandoc --smart --number-sections --bibliography=diploma.bib --latex-engine=xelatex --csl=gost_numeric.csl --epub-cover-image=cover.jpg diploma.tex -o diploma.epub
pandoc --smart --number-sections --bibliography=diploma.bib --latex-engine=xelatex --csl=gost_numeric.csl --reference-docx=template.docx diploma.tex -o diploma.docx
pandoc --smart --number-sections --bibliography=diploma.bib --latex-engine=xelatex --csl=gost_numeric.csl diploma.tex -o diploma.html


\newpage
\section*{Task 6: Hygiene Prediction}
In this task we must predict whether a set of restaurants will pass the public health inspection tests given the corresponding Yelp text reviews along with some additional information such as the locations and cuisines offered in these restaurants.

I consider this task as a classical problem of data categorization. Given a training set of data, we can use several well-known classification methods to make a prediction on a test set of data. What we need to start the process is to generate the features that  will be used for classification.

\subsection*{Feature generation}
\begin{itemize}
  \item We already have several features that can be used for classification without special preparation. These features are: zip code, number of reviews and average rating
  
  \item We also have the list of cuisines offered by each restaurant. We can consider each cuisine name as a separate feature that takes value 0 (restaurant does not offer this cuisine) or 1 (in opposite case)
  
  \item The text of restaurant reviews can be vectorized using TF-IDF method. In this case every restaurant can be represented as an N-dimensional vector, which can be considered as N features for classification. This is  how I did this vectorization: 

  \begin{enumerate}
  \item I have tokenized the text to unigrams and bigrams, removed stopwords, added part-of-speech tags and applied stemming using \href{http://www.nltk.org/api/nltk.stem.html?highlight=porterstemmer#nltk.stem.porter.PorterStemmer}{Porter stemmer algorithm}
  \item I trained \href{http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html}{CountVectorizer} using train data
  \item I transformed all data (train + test) using CountVectorizer and then applied \href{http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html}{TF-IDF transformation} to the resulting vectors.
  \end{enumerate}
  
  \item The text of restaurant review can be vectorized using \href{https://radimrehurek.com/gensim/models/word2vec.html}{Word2Vec} model as it is described in articles~\cite{kaggle2, kaggle3}. First of all we train Word2Vec model using the sentences from the training dataset, then vectorize all the words in the whole corpus (train + test) and then average the sum of vectors for every restaurant.
  
  \item The text of restaurant review can also be vectorized using \href{https://radimrehurek.com/gensim/models/doc2vec.html}{Doc2Vec} model as it is described in article~\cite{doc2vec}.
\end{itemize}

As a result we have obtained several thousand of features:
\begin{itemize}
  \item Several dozens of features from hygiene.dat.additional (every cuisine plus zip code, number of reviews and average rating)
  \item Several thousands of features from TF-IDF vectorizer (depends on the parameters of transformation)
  \item Several hundreds of features from Word2Vec or Doc2Vec vectorizer (depends on the parameters of model)
\end{itemize}

The number of features seems to be too large, so we need to reduce this number and select only useful features for further classification.


\subsection*{Feature selection}
We can experiment with features by applying \href{http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.KFold.html}{K-Fold}\footnote{The main idea of K-Fold approach is very simple: you can divide train data in K parts and then use K-1 of them to train the classifier and the remaining part -- to check the quality of prediction} technique to the train subset of data. These experiments led me to the following conclusions (these conclusions apply only to the classification approach I have described below, so they may change in case of different approach):
\begin{itemize}
  \item zip code and number of reviews do not affect the quality of classification and sometimes worsen the result, so i did not use these features in my analysis
  \item average rating is a good feature that has a positive effect on the result of classification
  \item cuisine features also slightly improve the result of classification
  \item i was not able to improve the quality of classification using Word2Vec or Doc2Vec features, though I have tried several parameters of vectorization, so I did not use these features in further analysis
  \item TF-IDF features seem to be very useful for the quality of classification.
\end{itemize}

TF-IDF features are too numerous and it seems reasonable that only a few of them  can correlate with the results of health inspection tests, so we need to remove the unsuitable features. Article~\cite{features} suggests an interesting approach for selecting good features. In a nutshell - we can train several machine learning methods (i use \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html}{linear regression}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html}{ridge}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html}{lasso} and \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html}{random forest regressor}) that have some inherent internal ranking of features (for example, regression coefficients). The combination of these rankings helps me to choose the most important features. After several experiments I decided to choose top-200 features for classification.



\subsection*{Classification methods}

I have used the following methods for classification:
\begin{itemize}
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.GaussianNB.html}{Gaussian Naive Bayes}
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html}{Random Forest} classifier
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html}{Logistic Regression} classifier
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html}{Decision Tree} classifier
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html}{K-nearest neighbors}
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html}{Linear Support Vector} classifier
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html}{AdaBoost} classifier
  \item \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html}{Gradient Boosting} classifier
  \item Neural network classifier using \href{http://theanets.readthedocs.org/en/stable/}{theanets} library
\end{itemize}

I also used \href{http://scikit-learn.org/stable/modules/generated/sklearn.grid_search.GridSearchCV.html}{GridSearchCV} method to find the best parameters for every classifier as it is described in article~\cite{scikit}. As a criterion of result quality I used F1-score for prediction of label=1, I also took into account precision and recall. Using cross-validation (K-Fold) I have found that the best classifiers are: Random Forest, Logistic Regression, Gaussian Naive Bayes and neural network classifier.


\subsection*{Discussion of results}

My best result is F1=0.5693 (11-th place as for 10-Oct). To obtain such result I have followed these steps:

\begin{enumerate}
  \item I have chosen these features for classification: TF-IDF vectors, cuisines list, average rating
  
  \item The best result for Random Forest classifier is F1=0.5653. To obtain this result I selected top-200 features and used the following parameters of classifier: \\ {\small \texttt{sklearn.ensemble.RandomForestClassifier(n\_estimators=200, criterion='entropy')} }
  
  \item The best result for neural network is F1=0.5623. To obtain this result I took only TF-IDF vectors as features and used the following parameters: \\ {\small \texttt{ exp = theanets.Experiment(theanets.Classifier, layers=[6264, 2]) \\
      exp.train(train, valid, algo='adam', learning\_rate=1e-2) }}
      
  \item Other classifiers gave too low results (below F1=0.55).    
      
  \item I combined my 8 best results with F1-score more than 0.55 as it is described in article~\cite{ensembling}. This action improved my result to F1=0.5693
\end{enumerate}

I see the following opportunities to improve my result:
\begin{itemize}
  \item Experiment with Word2vec and Doc2Vec models. These vectorizers are more sophisticated than TF-IDF, so theoretically they can improve the quality of classification.
  \item Generate more features using LDA topics, clustering, sentiment analysis
  \item Improve the quality of existing features, for example, remove human names from the text before implementing TF-IDF vectorizer
\end{itemize}

I have also made some calculations using \href{http://dataminingcapstone.cs.illinois.edu}{Leaderboard} data and have found that there are just 77 restaurants that did not pass the health inspection tests. My algorithm has labeled 4860 restaurants -- much more than the real count. So -- despite the fact that I took the 11-th place -- I think my prediction is bad because it has chosen too many odd restaurants.





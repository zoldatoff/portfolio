\newpage
\section{Activities}
% A summary of your project activities, which can be simply a compressed combination of all your individual task reports.


\subsection{Task 1: Exploration of Data Set}
The goal of this task is to explore the Yelp dataset which contains about 700k restaurant reviews. These restaurants represent more than 200 cuisines and are located in more than 100 US cities.

In this task I have investigated the following aspects of Yelp dataset:
\begin{enumerate}
  \item \textbf{Cuisines ratings}. We can distinguish three main groups of cuisines with specific rating distributions:
  \begin{itemize}
    \item Common cuisines. Rating distribution for this group has a clear peak at value 3.5. Representative member of this group is Mexican cuisine (red line on figure~\ref{fig:ratings}).
    \item Low-cost restaurants. The maximum of rating distribution curve for this group is also located near value 3.5, but the whole distribution is more smooth and has a heavy tail in low-rating range (1.0--2.5). The example of such cuisine is Fast Food (blue line on figure~\ref{fig:ratings}).
    \item Special cuisines. The most common rating for these restaurants is shifted to value 4.0 and the whole curve has a heavy tail in high-rating region. Example: Vegetarian food (green line on figure~\ref{fig:ratings}).
  \end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{ratings.png}
    \caption{Cuisine rating distribution}
    \label{fig:ratings}
\end{figure}

  \item \textbf{Location and popularity of restaurants}. On figure~\ref{fig:map} I have visualized the statistics about Mexican restaurants. Each circle represents a city: the size of the circle is proportional to number of reviews and the color of the circle illustrates the average review rating. We can see that the best Mexican food you can taste in McFarland and the most popular places for Mexican cuisine are Las Vegas and Phoenix.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{map_mexican.png}
    \caption{Mexican restaurants map}  
    \label{fig:map}  
\end{figure}


  \item \textbf{Topic mining.} The reviews text can be analyzed using topic mining technique. I have trained LDA model with 100 topics on the whole corpus of restaurant reviews and obtained the topic distribution for several cuisines. I have made two experiments for each cuisine: applied topic modeling to all the cuisine reviews and separately to negative (1-2 stars) and positive (4-5 stars) reviews.
  
  The result for Mexican cuisine is shown on figure~\ref{fig:graph_m}: node size is proportional to topic or word weight, node color is the same for the same topics within the graph. Here we can see that some topics can identify special cuisines (e.g., topic \#79 is about Mexican food) and that positive and negative reviews share the same common topics -- so, perhaps, we need to go deeper and investigate less common topics to find those that can identify positive or negative reviews.

\begin{figure}[H]\centering\subfigure{  \includegraphics[width=0.47\linewidth]{Mexican1.png}
}\quad\subfigure{  \includegraphics[width=0.47\linewidth]{Mexican2.png}
}\caption{Mexican cuisine reviews topics}
\label{fig:graph_m}\end{figure}
  
\end{enumerate}



\subsection{Task 2: Cuisine Clustering and Map Construction}
The goal of this task is to mine the data set to construct a cuisine map to visually understand the landscape of different types of cuisines and their similarities.

In this task I have applied several techniques to understand the similarity of popular cuisines (with more than 10k reviews):

\begin{itemize}
  \item \textbf{TF similarity.} The most simple approach using just term frequency.
  \item \textbf{TF-IDF similarity}. This method also uses inverse document frequency, but in this experiment it returns the same result as TF model.
  \item \textbf{LDA similarity}. LDA is more sophisticated method than TF-IDF, but it has not found any new interesting similarities. LDA just has increased some similarities and made them look more clear.
  \item \textbf{Doc2Vec similarity.} This is the most sophisticated approach I used for text comparison. It can reveal the similarities that are beyond the capabilities of other algorithms, for example, Doc2Vec has found similar cuisines for Barbeque, Thai and Vietnamese cuisines.
\end{itemize}

The most interesting results are presented on figure~\ref{fig:similarity}: here you can compare two maps of cuisine similarities calculated using LDA and Doc2Vec methods.

\begin{figure}[H]\centering\subfigure{  \includegraphics[width=0.47\linewidth]{lda.png}
}\quad\subfigure{  \includegraphics[width=0.47\linewidth]{doc2vec.png}
}\caption{LDA and Doc2Vec similarity}
\label{fig:similarity}\end{figure}


In this task we have also investigated cuisine clustering methods. I have used two methods: k-means clustering and agglomerative clustering, but they returned very similar results, so in this paper I present only k-means clustering based on LDA and Doc2Vec data with 8 clusters. Both maps contain representative but slightly different information. Look at Chinese food: it belongs to the cluster of eastern cuisines in case of LDA approach, and to the cluster of fast food in case of Doc2Vec-based data. 

\begin{figure}[H]\centering\subfigure{  \includegraphics[width=0.47\linewidth]{kmeans_8_.png}
}\quad\subfigure{  \includegraphics[width=0.47\linewidth]{doc2vec_8_.png}
}\caption{K-means clustering based on LDA and Doc2Vec similarity}
\label{fig:kmeans_doc2vec}\end{figure}


\subsection{Task 3: Dish Recognition}
The goal of this task is to mine the data set to discover the common/popular dishes of a particular cuisine. I have chosen \textbf{Chinese} cuisine and reached the result of 10 points (best possible result is 12 points). I have taken the following steps to achieve this result:
\begin{enumerate}
  \item I have started with manual tagging of given list of dishes and expanded it using information from Wikipedia.
  \item I ran SegPhrase classifier using the manually tagged list and got about 60k phrases containing low-quality dish names
  \item I ran ToPMine with 5 topics and obtained one topic with about 3k high-quality dish names.
  \item Union of lists generated by ToPMine and SegPhrase algorithms allowed me to gain only 8 points, so I needed to improve my methods to reach a better result
  \item The problem was to select most qualitative 10k dish names from the list of about 63k phrases. I decided to use Word2Vec method to enrich the list of phrases and to improve the quality of selection. I have trained Word2Vec using unigrams, bigrams and 3-grams from restaurant reviews and calculated the similarity between these n-grams and the manually tagged list of dishes. This approach gave me additional metrics that allowed me to build more qualitative list of dishes: incorporation of this metrics improved my result to 10 points.
\end{enumerate}

All three methods (ToPMine, SegPhrase and Word2Vec) were very appropriate for solving the given problem. But only the combination of all these three methods allowed me to beat the first baseline and gain 10 points.



\subsection{Task 4: Popular Dishes}
The general goal of Tasks 4 and 5 is to leverage recognized dish names to further help people making dining decisions. Specifically, Task 4 is to mine popular dishes in a cuisine that are liked by people; this can be very useful for people who would be interested in trying a cuisine that they might not be familiar with.

I decided to use the following metrics to visualize the list of popular dishes of chosen Chinese cuisine:
\begin{enumerate}
  \item \textbf{Popularity} = the number of unique restaurants offering a particular dish
  \item \textbf{Dish quality}. At first I was going to apply sentiment analysis to the sentences containing dish names to distinguish the most delicious dishes. I have experimented with \href{https://textblob.readthedocs.org}{TextBlob} python library and with Stanford Sentiment Analysis tool, but the results of these methods showed a very low correlation with the ratings of the corresponding reviews. This means that simple methods of sentiment analysis are inappropriate in case of restaurants reviews. The main reason of this is that particular sentences containing dish name often do not include the opinion about this dish. And if we want to analyze the whole review (not just one sentence) - then we already know user rating which is a priori more accurate than our algorithmic calculation. So i decided to use the ratings of the reviews to rank the quality of dishes that were mentioned in these reviews.
\end{enumerate}

The resulting visualization is represented on figure~\ref{fig:task4}. Here we can learn that \textbf{short rib} is perhaps the most delicious dish in Chinese cuisine. 
\begin{itemize}
  \item The length of the bars is proportional to dish popularity
  \item The color of the bar shows the quality of the dish: the most delicious dishes are marked with dark red color, the least delicious dishes -- with dark blue color.
\end{itemize}

\begin{figure}[H]  \centering  \includegraphics[width=0.9\linewidth]{task4.png}
  \caption{Popular Chinese dishes}
  \label{fig:task4}
\end{figure}

\subsection{Task 5: Restaurant Recommendation}
The general goal of Tasks 4 and 5 is to leverage recognized dish names to further help people making dining decisions. Task 5 is to recommend restaurants to people who would like to have a particular dish or a certain type of dishes. Let’s take <<Kung Pao Chicken>> for this experiment. 

We will use the following metrics to visualize the results of this task:
\begin{enumerate}
  \item \textbf{Restaurant popularity} = the total number of reviews about the restaurant. I decided not to rank the restaurants using their rating, because when I try to choose a restaurant for myself I always look at the price level together with rating.
  \item \textbf{Dish quality} = the difference between the rating of the review containing the dish name and the restaurant rating. If the reviews about the dish get more stars than the restaurant, then this dish is worth a try!
\end{enumerate}

The visualization on figure~\ref{fig:task5} shows the restaurants that offer Kung Pao Chicken. From this visualization we can learn that KJ Kitchen an Jasmine are the best places to taste this dish.
\begin{itemize}
  \item Bar length is proportional to the restaurant's popularity  
  \item Bar color represents the recommendation to try Kung Pao Chicken in particular restaurant. Deep red color tells that this dish has ratings higher than average restaurant rating, deep blue color shows the opposite case.
\end{itemize}

\begin{figure}[H]  \centering  \includegraphics[width=0.85\linewidth]{task5.png}
  \caption{Restaurant Recommendations for Kung Pao Chicken}
  \label{fig:task5}
\end{figure} 


\subsection{Task 6: Hygiene Prediction}
\label{subsection:task6}

In this task we must predict whether a set of restaurants will pass the public health inspection tests given the corresponding Yelp text reviews along with some additional information such as the locations and cuisines offered in these restaurants.

I consider this task as a classical problem of data categorization. Given a training set of data, we can train several well-known classification methods to make a prediction on a test set of data. What we need to start the process is to generate the features that will be used for classification. I tried to use the following features:

\begin{itemize}
  \item Zip code, number of reviews and average rating. My experiments showed that zip code and number of reviews do not affect the quality of classification and sometimes worsen the result, so I used only average rating in my further analysis
  
  \item List of cuisines offered by each restaurant: we can consider each cuisine name as a separate feature that takes value 0 (restaurant does not offer this cuisine) or 1 (in opposite case)
  
  \item TF-IDF vectors generated from the text of restaurant reviews: every restaurant can be represented as an N-dimensional vector, which can be considered as N features for classification. It seems reasonable that only a few of these N features can correlate with the results of health inspection tests. Article~\cite{features} suggests an interesting approach to select these "good" features. In a nutshell -- we can train several machine learning methods (I have used \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html}{linear regression}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html}{ridge}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html}{lasso} and \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html}{random forest regressor}) that have some inherent internal ranking of features (for example, regression coefficients). The combination of these rankings helped me to choose the most important features. After several experiments I decided to choose top-200 features for classification.
    
  \item Vectors generated using Word2Vec and Doc2Vec model applied to the text of restaurant reviews as it is described in articles~\cite{kaggle2, kaggle3, doc2vec}. I was not able to improve the quality of classification using Word2Vec or Doc2Vec features, though I have tried several parameters of vectorization, so I did not use these features in further analysis
\end{itemize}

After receiving a set of features, we can start classification using the following methods: \href{http://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.GaussianNB.html}{Gaussian Na\"\i ve Bayes}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html}{Random Forest} classifier, \href{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html}{Logistic Regression} classifier, \href{http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html}{Decision Tree} classifier, \href{http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html}{K-nearest neighbors}, \href{http://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html}{Linear Support Vector} classifier, \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html}{AdaBoost} classifier, \href{http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html}{Gradient Boosting} classifier, Neural network classifier using \href{http://theanets.readthedocs.org/en/stable/}{theanets} library. I have used \href{http://scikit-learn.org/stable/modules/generated/sklearn.grid_search.GridSearchCV.html}{GridSearchCV} function to find the best parameters for every classifier as it is described in article~\cite{scikit}. Using cross-validation (K-Fold\footnote{The main idea of K-Fold approach is very simple: you can divide train data in K parts and then use K-1 of them to train the classifier and the remaining part -- to check the quality of prediction}) I have found that the best classifiers are: Random Forest, Logistic Regression, Gaussian Na\"\i ve Bayes and neural network classifier.


My best result is F1=0.5693 (17-th place). To obtain such result I have used the following methods:

\begin{enumerate}  
  \item I have reached F1=0.5623 using neural network classifier. To obtain this result I took only TF-IDF vectors as features and used a simple network without a hidden layer.
  
  \item I have reached F1=0.5653 using Random Forest classifier. To obtain this result I selected top-200 features among these: TF-IDF vectors + cuisines list + average review rating.
      
  \item I combined my 8 best results with F1-score exceeding 0.55 as it is described in article~\cite{ensembling}. This action improved my result up to F1=0.5693
\end{enumerate}

I see the following opportunities to improve my result:
\begin{itemize}
  \item Experiment with Word2vec and Doc2Vec models. These vectorizers are more sophisticated than TF-IDF, so theoretically they can improve the quality of classification.
  \item Generate more features using LDA topics, clustering, sentiment analysis
  \item Improve the quality of existing features, for example, remove human names from the text before applying TF-IDF vectorizer
\end{itemize}
